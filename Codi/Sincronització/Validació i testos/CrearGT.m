close all, clear all
cd('..');
currentpath = cd('..');
parentpath = pwd();
cd(currentpath);

baseDir = [parentpath '\201405161455\'];
li=dir([baseDir '*.png']);

%Inicialitzaci� de par�metres
puntsFrame=10;
margesX = 14; 
margesY = 2;
initFrame = 930;%Avaluarem la 1a rotaci� de 90� graus
sz = 32;
GT = zeros(puntsFrame,4,sz); %Format GT: GT(1,:,4) --> (1a fila)X1 Y1 X2 Y2 del frame 4

for i=initFrame:initFrame+sz
    fprintf('%d/%d\n',i-initFrame+1,sz);
    if i==initFrame
        f1=im2double(imread([baseDir li(i).name]));
        %Ens quedem nom�s amb les linies senars
        f1 = f1(1:2:end,:,:);
        %Els frames contenen marges negres que no interessen.
        f1 = f1(margesY:end, margesX:end-margesX, :); %En el cas de la Y nom�s t� un marge superior, no inferior
    else
        f1 = f2;
    end
    f2=im2double(imread([baseDir li(i+1).name]));
    f2 = f2(1:2:end,:,:);
    f2 = f2(margesY:end, margesX:end-margesX, :); %En el cas de la Y nom�s t� un marge superior, no inferior
    
    figure(1)
    imshow(f1);
    figure(2)
    imshow(f2);
    
    for ind=1:puntsFrame
        figure(1)
        title(sprintf('Frame %d',i-initFrame+1),'FontWeight','Bold');
        [x1, y1]=ginput(1);
        hold on
        plot(x1,y1,'go'); 
        %text(x1+5,y1+5,sprintf('%d',ind));
        figure(2)
        title(sprintf('Frame %d',i-initFrame+2),'FontWeight','Bold');
        [x2, y2]=ginput(1);
        hold on
        plot(x2,y2,'bo'); 
        %text(x2+5,y2+5,sprintf('%d',ind));
        figure(1)
        hold on;
        plot(x1,y1,'bo'); 
        GT(ind,1:4,i-initFrame+1) = [x1 y1 x2 y2];
    end
    %save('GT2','GT');
end