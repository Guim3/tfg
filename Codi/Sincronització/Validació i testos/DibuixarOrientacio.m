function DibuixarOrientacio(path, color)
    format long;
    if ~exist('path','var')
        path = 'trajectoriaFAST.txt';
    end
     if ~exist('color','var')
        color = 'b';
    end
    table = tdfread(path,'\t');
    %No podem fer servir el n� de frames com a eix X, com a unitat de mesura
    %en el temps. Hem de fer servir el temps que marca el nom dels frames.
    frameNames = table.FrameName(:,1:end-5); %Esborrem el .png dels noms
    timeline = zeros(size(frameNames,1),1);
    for i=1:size(frameNames,1)
        %Calculem el temps del frame individualment 
        timeFrame = strsplit(frameNames(i,1:end-5),'_'); %Agafem la informaci� del nom que ens indica el temps
        timeline(i)= ConvertStrToMs(timeFrame{2},timeFrame{3},...
            timeFrame{4},timeFrame{5})/(60*1000); %Ho convertim a minuts
    end
    bias = 1.38;
    close all;
    figure(1); hold on;  
    title('Orientaci� del dron en el temps','FontWeight','Bold');
    xlabel('Temps (min)'); ylabel('Graus');
    plot(timeline-timeline(1)+bias,table.orientacio, color); %Restem el 1r temps perqu� la gr�fica
                                            %comenci per temps = 0s
    load('trajectoriaOrientacioSensor.mat');
    %Eliminem els canvis sobtats de 360� a 0�
    %Ysensor(Ysensor<120) = Ysensor(Ysensor<120)+360;
    for i=1:size(Ysensor,1)-1
       if abs(Ysensor(i)-Ysensor(i+1)) > 180 %En una esfera de 360� la m�xima dist�ncia entre 2 punts s�n 180�
           %Sempre has de modificar i+1 per no tenir problemes 
           if Ysensor(i) < Ysensor(i+1) %Cas 0� a 359�
               Ysensor(i+1) = Ysensor(i+1)-360; %0� a -1�  
           else %Cas 360� a 1�
               Ysensor(i+1) = Ysensor(i+1)+360; %360� a 361�
           end
       end
    end
    %Centrem la rotaci� perqu� comenci al 0 de quan comen�a l'altre
    %trajectoria
    ind=find(Xsensor(:)>bias+Xsensor(1)-0.01&Xsensor(:)<bias+Xsensor(1)+0.01,1);
    Ysensor = -Ysensor+Ysensor(ind(round(size(ind,1))));
    
    plot(Xsensor-Xsensor(1),Ysensor,'r');
    hold off;
   
end