close all;
load('historic');
%Ara ja tenim el nombre de punts detectats. Per mirar l'accuracy comparar
%les traject�ries de rotaci� generades amb la rotaci� del sensor. Es podria
%fer un diagrama de barres amb la difer�ncia entre traject�ries.
fields = fieldnames(nPoints);

histAvMatched = zeros(numel(fields),1);
for i= 1:numel(fields)
  histAvMatched(i,:) =  mean(nMatched.(fields{i}));
end
figure(1);
subplot(1,3,1);
bar(histAvMatched);
colormap summer;
set(gca,'XTickLabel',fields);
title('Mitjana de punts emparellats', 'FontWeight', 'Bold')
ylabel('Punts detectats')
xlabel('Tipus de descriptor')

histAvInliers = zeros(numel(fields),1);
for i= 1:numel(fields)
  histAvInliers(i,:) =  mean(nInliers.(fields{i}));
end
%figure(2);
subplot(1,3,2);
bar(histAvInliers);
colormap summer;
set(gca,'XTickLabel',fields);
title('Mitjana d''inliers emparellats', 'FontWeight', 'Bold');
ylabel('Punts detectats');
xlabel('Tipus de descriptor');

%figure(3);
subplot(1,3,3);
bar(histAvInliers./histAvMatched);
colormap summer;
set(gca,'XTickLabel',fields);
title('Precision', 'FontWeight', 'Bold');
ylabel('Ratio')
xlabel('Tipus de descriptor')