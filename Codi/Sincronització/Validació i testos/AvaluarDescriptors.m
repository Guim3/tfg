function AvaluarDescriptors()
    close all;
    format long;
    currentpath = cd('..');
    cd('..');
    cd('..');
    parentpath = pwd();
    cd(currentpath);
    %MostrarResultats();
    %pause;
    baseDir = [parentpath '\201405161455\'];
    li=dir([baseDir '*.png']);

    %Inicialitzaci� de par�metres
    margesX = 14; 
    margesY = 2;
    initFrame = 930;%Avaluarem la 1a rotaci� de 90� graus
    sz = 32;
    llistaDetectors = {'AnotacioManual','SURF', 'MSER', 'BRISK', 'Eigen', 'FAST', 'Harris'};

    % llindarRotacio = 10*despFrames; %Rotaci� m�xima permesa entre frames (per descartar outliers)
    % llindarEscala = 0.08;%0.10 %Variaci� d'escala m�xima i m�nima permesa entre frames
    % numMaxFramesSorollosos = 10; %Si s'han om�s X frames (tant sigui per falta d'inliers com per ser sorollosos)
    %                             %Aleshores el frame 1 (el frame que es comparava amb tota la resta),
    %                             %avan�ar� al seg�ent frame per intentar evitar entrar en un bucle on no es
    %                             %troben frames correlacionats.

    %Per cada tipus de detector crearem una estructura dins de dades. Aquesta
    %estructura contindr� la difer�ncia amb la traslaci� amb anotaci� manual, i
    %la difer�ncia de la theta amb anotaci� manual.
    GT = [];
    load('GT');
    dades = struct();
    
    %Inicialitzem l'estructura de dades pel tipus de detector
    for tipusDeteccio = llistaDetectors
        tipusDeteccio = tipusDeteccio{1}; %Conversi� de cell a str
        eval(sprintf('dades.%s = struct();',tipusDeteccio)); 
        eval(sprintf('dades.%s.traslacio = [];',tipusDeteccio)); 
        eval(sprintf('dades.%s.theta = [];',tipusDeteccio)); 
        eval(sprintf('dades.%s.scale = [];',tipusDeteccio)); 
    end
tic
    for i=1:sz
        if i==1
            f1=im2double(rgb2gray(imread([baseDir li(i-1+initFrame).name])));
            %Ens quedem nom�s amb les linies senars
            f1 = f1(1:2:end,:,:);
            %Els frames contenen marges negres que no interessen.
            f1 = f1(margesY:end, margesX:end-margesX, :); %En el cas de la Y nom�s t� un marge superior, no inferior
        else
            f1 = f2;
        end
        f2=im2double(rgb2gray(imread([baseDir li(i+initFrame).name])));
        f2 = f2(1:2:end,:,:);
        f2 = f2(margesY:end, margesX:end-margesX, :); %En el cas de la Y nom�s t� un marge superior, no inferior
        for tipusDeteccio = llistaDetectors
            tipusDeteccio = tipusDeteccio{1}; %Conversi� de cell a str
            if strcmp(tipusDeteccio,'AnotacioManual')
               %Ja tenim els punts anotats, no fa falta llegir cap imatge ni trobar
               %els punts corresponents
               %Format GT --> X(im1) Y(im1) X(im2) Y(im2). La 3a dim �s el frame
               matchedf1Points = GT(:,1:2,i);
               matchedf2Points = GT(:,3:4,i);
               matchedf1Points(:,2) = matchedf1Points(:,2)*2;
               matchedf2Points(:,2) = matchedf2Points(:,2)*2;

            else
                [~, f1Points, f1Features] = LlegirFrame([baseDir li(i-1+initFrame).name], margesX, margesY, tipusDeteccio,f1);
                [~, f2Points, f2Features] = LlegirFrame([baseDir li(i+initFrame).name], margesX, margesY, tipusDeteccio,f2);
                pairs = matchFeatures(f1Features, f2Features, 'MaxRatio', 0.6);
                matchedf1Points = f1Points(pairs(:, 1),:);
                matchedf2Points = f2Points(pairs(:, 2),:);
                matchedf1Points.Location(:,2) = matchedf1Points.Location(:,2)*2;
                matchedf2Points.Location(:,2) = matchedf2Points.Location(:,2)*2;
            end

            [tform, inlierf1Points, inlierf2Points, status] = ...
                    estimateGeometricTransform(matchedf1Points, matchedf2Points, 'similarity', ...
                    'MaxNumTrials', 100000000, 'Confidence', 99.99999, 'MaxDistance', 1.5);
            Tinv  = tform.invert.T;
            ss = Tinv(2,1);
            sc = Tinv(1,1);
            scale = sqrt(ss*ss + sc*sc);
            theta = atan2(ss,sc);%*180/pi;
            translation = Tinv(3,1:2);
            theta = theta*180/pi;
            %Nota: potser s'hauria de corregir la rotaci� i l'orientaci�
            eval(sprintf('dades.%s.traslacio(end+1,1) = translation(1);',tipusDeteccio)); 
            eval(sprintf('dades.%s.traslacio(end,2) = translation(2);',tipusDeteccio)); 
            eval(sprintf('dades.%s.theta(end+1) = theta;',tipusDeteccio)); 
            eval(sprintf('dades.%s.scale(end+1) = scale;',tipusDeteccio));
        end
        fprintf('%d/%d\n',i,sz+1);
    end      
    save('avaluaci�Descriptors','dades');
    MostrarResultats();
toc
end

function MostrarResultats()
    Translacio();
    Orientacio();
end

function Translacio()
    colorList = ['m','c','r','g','k','b','y','w'];
    llistaDetectors = {'SURF', 'BRISK', 'Eigen', 'FAST'};
    load('avaluaci�Descriptors');
    %Filtrem l'esstructura 'dades' amb aquells descriptors que farem servir.
    descriptors = filtrarStruct(dades,llistaDetectors);
    nFrames = size(dades.AnotacioManual.traslacio, 1);
    h = figure();
    set(h, 'Position', [500 500 1000 500])
    subplot(2,2,[3 4]);
    title('Error de traslaci� al llarg dels frames','FontWeight','Bold');
    ylabel('Error (dist�ncia euclidea)');
    xlabel('Frames');
    axis([0 nFrames 0 90]);
    hold on;
    
    fields = fieldnames(descriptors);
    errorArray = zeros(numel(descriptors),nFrames);
  
    for i = 1:numel(fields)
        %Cada fila �s un descriptor. La posici� de cada fila (cada columna)
        %representa l'error d'un frame en concret.
        for j=1:nFrames
            errorArray(i,j) = pdist([dades.AnotacioManual.traslacio(j,:); descriptors.(fields{i}).traslacio(j,:)],'euclidean');
        end
        plot(1:nFrames, errorArray(i,:), colorList(i));
    end
    legend(llistaDetectors);
    
    
    hist = zeros(numel(fields),1);
    for i= 1:numel(fields)
            hist(i,:) =  sum(errorArray(i,:));
    end
    %figure(2);
    subplot(2,2,1);
    bar(hist);
    colormap summer;
    set(gca,'XTickLabel',fields);
    ylabel('Error acumulat');
    title('Error de translaci� acumulat','FontWeight','Bold');

    
    hist = zeros(numel(fields),1);
    for i = 1:numel(fields)
            hist(i) = pdist([sum(dades.AnotacioManual.traslacio); sum(descriptors.(fields{i}).traslacio)], 'euclidean');
    end
    %figure(3);
    subplot(2,2,2)
    bar(hist);
    colormap summer;
    set(gca,'XTickLabel',fields);
    ylabel('Error final');
    title('Error de translaci� final (nom�s es considera la posici� final)','FontWeight','Bold');
end


function Orientacio()
    colorList = ['m','c','r','g','k','b','y','w'];
    llistaDetectors = {'SURF', 'MSER', 'BRISK', 'Eigen', 'FAST', 'Harris'};
    load('avaluaci�Descriptors');
    %Filtrem l'esstructura 'dades' amb aquells descriptors que farem servir.
    descriptors = filtrarStruct(dades,llistaDetectors);
    
    nFrames = size(dades.AnotacioManual.traslacio, 1);
    h = figure();
    set(h, 'Position', [500 500 1000 500])
    subplot(2,2,[3 4]);
    title('Error de rotaci� al llarg dels frames','FontWeight','Bold');
    ylabel('Error');
    xlabel('Frames');
    axis([0 nFrames 0 20]);
    hold on;
    
    fields = fieldnames(descriptors);
    errorArray = zeros(numel(descriptors),nFrames);
  
    for i = 1:numel(fields)
        %Cada fila �s un descriptor. La posici� de cada fila (cada columna)
        %representa l'error d'un frame en concret.
        for j=1:nFrames
            errorArray(i,j) = abs(dades.AnotacioManual.theta(j)- descriptors.(fields{i}).theta(j));
        end
        plot(1:nFrames, errorArray(i,:), colorList(i));
    end
    legend(llistaDetectors);
    
    
    hist = zeros(numel(fields),1);
    for i= 1:numel(fields)
            hist(i,:) =  sum(errorArray(i,:));
    end
    %figure(2);
    subplot(2,2,1);
    bar(hist);
    colormap summer;
    set(gca,'XTickLabel',fields);
    ylabel('Error acumulat');
    title('Error de rotaci� acumulat','FontWeight','Bold');

    
    hist = zeros(numel(fields),1);
    for i = 1:numel(fields)
            hist(i) = abs(sum(dades.AnotacioManual.theta) - sum(descriptors.(fields{i}).theta));
    end
    %figure(3);
    subplot(2,2,2)
    bar(hist);
    colormap summer;
    set(gca,'XTickLabel',fields);
    ylabel('Error final');
    title('Error de rotaci� final','FontWeight','Bold');
end

function output = filtrarStruct(dades, llistaElements)
    %Donat l'estructura 'dades' i els elements de 'llistaElements', filtra
    %crea una estructura 'output' amb aquells elements de 'dades' que estan
    %en 'llistaElements'
    output = struct();
    fields = fieldnames(dades);
    for i =1:numel(fields)
        trobat = false;
        j=1;
        while not(trobat) && j<= length(llistaElements)
            if strcmp(fields{i},llistaElements{j})
                eval(sprintf('output.%s = dades.%s;',llistaElements{j},llistaElements{j}));
                trobat = true;
            end
            j=j+1;
        end
    end
end

function [frame, points, features] = LlegirFrame(path, margesX, margesY, tipusDeteccio, frame)
    if ~exist('tipusDeteccio','var')
        tipusDeteccio = 'FAST';
    end
    if ~exist('frame','var')
        frame=im2double(rgb2gray(imread(path)));
    end
    %Ens quedem nom�s amb les linies senars
    frame = frame(1:2:end,:,:);
    %Els frames contenen marges negres que no interessen.
    frame = frame(margesY:end, margesX:end-margesX, :); %En el cas de la Y nom�s t� un marge superior, no inferior
    %Detect feature points
    if strcmpi(tipusDeteccio, 'SURF')                    %4
        points = detectSURFFeatures(frame, 'NumOctaves', 4, 'NumScaleLevels', 4, 'MetricThreshold', 1000);
    elseif strcmpi(tipusDeteccio, 'MSER')
        points = detectMSERFeatures(frame, 'ThresholdDelta', 2, 'RegionAreaRange', [30 14000], 'MaxAreaVariation', 0.25);
    elseif strcmpi(tipusDeteccio, 'BRISK')              
        points = detectBRISKFeatures(frame, 'MinContrast', 0.01, 'MinQuality', 0.001, 'NumOctaves', 4, 'ROI', [1 1 size(frame,1) size(frame,1)]);
    elseif strcmpi(tipusDeteccio, 'Eigen')     
        points = detectMinEigenFeatures(frame,'MinQuality', 0.01, 'FilterSize', 5, 'ROI', [1 1 size(frame,1) size(frame,1)]);
    elseif strcmpi(tipusDeteccio, 'FAST')     
        points = detectFASTFeatures(frame,'MinQuality', 0.01, 'MinContrast', 0.01, 'ROI', [1 1 size(frame,1) size(frame,1)]);
    elseif strcmpi(tipusDeteccio, 'Harris')     
        points = detectHarrisFeatures(frame,'MinQuality', 0.01, 'FilterSize', 5, 'ROI', [1 1 size(frame,1) size(frame,1)]);
    else
       err = MException('TypeError:UnrecognizedType', ...
        'El tipus especificat ha de ser un dels seg�ents: SURF, MSER, BRISK, Eigen, FAST, Harris.'); 
       throw(err);
    end
        
    %Extract feature descriptors
    [features, points] = extractFeatures(frame, points, 'SURFSize', 64, 'Method', 'Auto');
    
end
