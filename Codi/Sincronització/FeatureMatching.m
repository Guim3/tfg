function framesLogPath = FeatureMatching(baseDir, tipusDeteccio, despFrames)
    %baseDir = directori on s'ubiquen els frames captats pel drone
    %tipusDeteccio = tipus de detecci� a utilitzar entre: SURF, MSER,
    %BRISK, Eigen, FAST i Harris. Per defecte �s FAST.
    %despFrames = 'salt' entre un frame i un altre. Per defecte �s 1.
        
    %Detectar features en general MATLAB:
    %       http://www.mathworks.es/es/help/vision/feature-detection-extraction-and-matching.html
    %SURF
    %Enlla� d'inter�s: http://www.mathworks.es/es/help/vision/examples/object-detection-in-a-cluttered-scene-using-point-feature-matching.html
    %"This method of object detection works best for objects that exhibit
    %non-repeating texture patterns, which give rise to unique feature matches."
    % MSER
    % http://www.mathworks.es/es/help/vision/ref/mserregions-class.html
    % http://www.mathworks.es/es/help/vision/ref/detectmserfeatures.html
    close all
    format long;
    
    if ~exist('tipusDeteccio','var')
        tipusDeteccio = 'FAST';
    end
    
    if ~exist('baseDir','var')
        currentpath = cd('..');
        cd('..');
        parentpath = pwd();
        cd(currentpath);

        baseDir = [parentpath '\201405161455\'];
    end
    if ~exist('despFrames','var')
        despFrames = 1;
    end
    li=dir([baseDir '*.png']);

    %Inicialitzaci� de par�metres
    margesX = 14; 
    margesY = 3;
    %initFrame = 1;%825;%1078;%1011;%925; 
    sz = size(li,1);%2300;300;
    llindarRotacio = 10*despFrames; %Rotaci� m�xima permesa entre frames (per descartar outliers)
    llindarEscala = 0.08;%0.10 %Variaci� d'escala m�xima i m�nima permesa entre frames
    numMaxFramesSorollosos = 10; %Si s'han om�s X frames (tant sigui per falta d'inliers com per ser sorollosos)
                                %Aleshores el frame 1 (el frame que es comparava amb tota la resta),
                                %avan�ar� al seg�ent frame per intentar evitar entrar en un bucle on no es
                                %troben frames correlacionats.
    
    %Par�metres per controlar l'aterrament
    llindarEscalaAterrament = 0.0008;
    maxFramesPerAvaluarAterrament = 40;
    llindarPercentatgeAterrament = 0.6;
    faseAterrament = false;
    framesAterrant = 0;
    
    %El video pot contenir un fragment inicial on el drone no es mou o
    %s'est� enlairant, el qual no ens interessa ja que pot afegir soroll.
    %Per aix� hem de mirar a partir de quin frame el video �s estable.
    initFrame = DescartarIniciVideo(tipusDeteccio, margesX, margesY, llindarRotacio, baseDir, li);
    %initFrame = 428;%1280;%428;
    dades = struct();
    dades.FrameName = cell(1,1);
    frameInitName = li(initFrame).name;
    dades.FrameName{1,1} = frameInitName;
    %C�lcul del temps relatiu
    dades.timeMS= zeros(1,1); %Temps relatiu a partir del frameName
    timeFrame = strsplit(frameInitName(1:end-5),'_'); %Agafem la informaci� del nom que ens indica el temps
    biasTemps= ConvertStrToMs(timeFrame{2},timeFrame{3},...
        timeFrame{4},timeFrame{5}); %El 1r frame estar� a temps 0, per aix� hem de restar cada cop el biasTemps (temps del 1r frame) a tots els frames
    %Fi calcul temps relatiu
    
    dades.offsetX = zeros(1,1); dades.offsetY = zeros(1,1); %despla�ament respecte l'anterior frame
    dades.grauRotacio= zeros(1,1);
    dades.orientacio = zeros(1,1);
    dades.escalatge = zeros(1,1);
    noPassarFrame = true;
    [~, f2Points, f2Features] = LlegirFrame([baseDir li(initFrame).name], margesX, margesY, tipusDeteccio);

    sum = [0 0];
    numFramesSorollosos = 0;
    %for i=initFrame:initFrame+sz-1 %1:sz-1   %Per cada frame
     i = initFrame;
    %  load('historic');
    %  eval(sprintf('nPoints.%s = [];',tipusDeteccio'));
    %  eval(sprintf('nMatched.%s = [];',tipusDeteccio'));
    %  eval(sprintf('nInliers.%s = [];',tipusDeteccio'));
     tic
     while i<sz && not(faseAterrament)
        if noPassarFrame
            %No ens hem de saltar cap frame. El f2 anterior passa a ser el nou f1
            %f1 = f2;
            f1Points = f2Points;
            f1Features = f2Features;
        elseif numFramesSorollosos >= numMaxFramesSorollosos 
            i = i - numFramesSorollosos*despFrames + despFrames; %Retrocedeixes al frame seg�ent de l'�ltim frame1.
            numFramesSorollosos = 0;
            [~, f1Points, f1Features] = LlegirFrame([baseDir li(i).name], margesX, margesY, tipusDeteccio);
            fprintf('Masses frames amb soroll acumulats. Es retrocediran %d frames per tornar a trobar coincid�ncies.\n', numMaxFramesSorollosos-1);
        end
        noPassarFrame = true;

        [f2, f2Points, f2Features] = LlegirFrame([baseDir li(i+1).name], margesX, margesY, tipusDeteccio);

        %Match features by using their descriptors
        pairs = matchFeatures(f1Features, f2Features, 'MaxRatio', 0.6);

        matchedf1Points = f1Points(pairs(:, 1),:);
        matchedf2Points = f2Points(pairs(:, 2),:);

        %Escalem la Y per dos, ja que en el frame nom�s hem agafat la meitat de
        %les files
        matchedf1Points.Location(:,2) = matchedf1Points.Location(:,2)*2;
        matchedf2Points.Location(:,2) = matchedf2Points.Location(:,2)*2;

        [tform, inlierf1Points, inlierf2Points, status] = ...
        estimateGeometricTransform(matchedf1Points, matchedf2Points, 'similarity', 'MaxNumTrials', 100000000, 'Confidence', 99.99999, 'MaxDistance', 1.5);

        if status ~= 0 %No hi ha suficients inliers, per tant passarem al seg�ent frame
            noPassarFrame = false;
            fprintf('%d/%d\tNo hi ha prous inliers entre els dos frames. S''agafar� el seg�ent.\n', i, sz);
        end

        if noPassarFrame
%             figure(5); 
%             showMatchedFeatures(f1, f2, inlierf1Points, inlierf2Points);
%             title('Matched Points (Inliers Only)');
%             legend('ptsFrame1','ptsFrame2');
%             hold off;
            %C�lcul de rotaci� mitjan�ant matriu inversa (matriu 3x3 amb escalatge, rotaci� i traslaci�).
            %(http://www.mathworks.es/es/help/vision/examples/find-image-rotation-and-scale-using-automated-feature-matching.html)
            %tform.invert.T --> matriu de transformaci� que mapeja els punts de
            %original a distorted
            %pDistorted = [inlierOriginal(6).Location 1]*tform.invert.T;
            %
            %tform.T --> mapeja punts de distorted a original
            %pOriginal = [inlierDistorted(6).Location 1]*tform.T;
            Tinv  = tform.invert.T;
            ss = Tinv(2,1);
            sc = Tinv(1,1);
            scale = sqrt(ss*ss + sc*sc);
            theta = atan2(ss,sc);%*180/pi;
            translation = Tinv(3,1:2);
    %         %Restringir la matriu de transformaci� geom�trica (opcional)
    %         HsRt = [[scale*[cos(theta) -sin(theta); sin(theta) cos(theta)]; ...
    %         translation], [0 0 1]'];
            theta = theta*180/pi;
            if 1.00-scale > llindarEscalaAterrament
                    if framesAterrant == 0
                       comptadorPerValorarframesAterrant =  maxFramesPerAvaluarAterrament;
                    end
                    framesAterrant = framesAterrant + 1;
                    %fprintf('Frame Aterrant\n'); %Aterra al frame 3169
            end
            if abs(theta) > llindarRotacio || abs(scale-1) > llindarEscala
                noPassarFrame = false;
                fprintf('%d/%d\tFrame soroll�s. S''agafar� el seg�ent.\tEscala: %.2f\tRotaci� entre frames: %.2f�\n', i, sz, scale, theta);
                
            else
                dades.FrameName{end+1,1} = li(i+1).name;
                %Guardem tamb� el temps relatiu
                frameName = li(i+1).name;
                timeFrame = strsplit(frameName(1:end-5),'_'); %Agafem la informaci� del nom que ens indica el temps
                timeFrame= ConvertStrToMs(timeFrame{2},timeFrame{3},...
                    timeFrame{4},timeFrame{5});
                dades.timeMS(end+1,1) = timeFrame - biasTemps;
                %Fi calcul temps relatiu
                dades.grauRotacio(end+1,1) = -theta;
                aux = dades.orientacio(end,1)+theta;
                if aux > 0;
                    graus = 360;
                else
                    graus = -360;
                end
                dades.orientacio(end+1,1) = mod(aux,graus);
                dades.escalatge(end+1,1) = scale;
                aux1 = inlierf1Points.Location;
                aux2 = inlierf2Points.Location;
                %C�lcul de dist�ncia recorreguda
                [f1Location, f2Location] = RectificarPosicions(aux1, aux2, f2, -theta, dades.orientacio(end-1)); %A difer�ncia del NCC, aqu� �s K perqu� la rotaci� es realitza del frame 2 per adaptar-se al frame 1. En la NCC el frame 1 es rota al frame 2.
                distancies = zeros(size(f1Location,1),2);
                distancies(:,1) = f1Location(:,1) - f2Location(:,1); %L'offset �s respecte el drone. Si dos frames estan despla�ats 10 unitats cap a l'esquerre
                                                                     %aix� indica que el drone s'ha mogut 10 unitats cap a la dreta.
                distancies(:,2) = f2Location(:,2) - f1Location(:,2);%En el cas de la Y �s diferent, ja que en una imatge de matlab, la Y s'incrementa cap abaix.
                dades.offsetX(end+1,1) = mean(distancies(:,1));
                dades.offsetY(end+1,1) = mean(distancies(:,2));
                if framesAterrant > 0
                    comptadorPerValorarframesAterrant = comptadorPerValorarframesAterrant - 1;
                end
                fprintf('%d/%d\tEscala: %.6f\tRotaci� entre frames: %.2f�\tAcumulat: %.2f�\n', i, sz, scale, -theta, dades.orientacio(end));
    %                 sum = DibuixarTrajectoria(dades.offsetX, dades.offsetY, sum);
    %              eval(sprintf('nPoints.%s(end+1) = round(mean(size(f1Points,1), size(f2Points,2)));;',tipusDeteccio'));
    %              eval(sprintf('nMatched.%s(end+1) = size(matchedf1Points,1);',tipusDeteccio'));
    %              eval(sprintf('nInliers.%s(end+1) = size(inlierf1Points,1);',tipusDeteccio'));
                %pause;
            end
        end

        if not(noPassarFrame)
            numFramesSorollosos = numFramesSorollosos + 1;
        else
            numFramesSorollosos = 0;
        end
        %pause;
        
        %Avaluem si s'est� aterrant, i com a tal, hem de deixar de
        %processar les imatges
        if framesAterrant > 0 && comptadorPerValorarframesAterrant == 0
            if framesAterrant/maxFramesPerAvaluarAterrament >= llindarPercentatgeAterrament
                 faseAterrament = true;
            end
            framesAterrant = 0;
        end
        i = i + despFrames;
     end
    toc
    %Mirem si s'ha deixat de processar imatges pel drone aterrant
    if faseAterrament == true
       %En cas afirmatiu, esborrem els �ltims frames que hem processar per
       %verificar que s'estava aterrant
       fields = fieldnames(dades);
       for i = 1:numel(fields)
            dades.(fields{i}) = dades.(fields{i})(1:end-maxFramesPerAvaluarAterrament,:);
       end
    end
    % save('historic', 'nPoints', 'nMatched', 'nInliers');

    %Creaci� fitxer
    framesLogPath = ['Fitxers generats\trajectoria' tipusDeteccio '.txt'];
    writetable(struct2table(dades), framesLogPath,'Delimiter','\t');
end

function [frame, points, features] = LlegirFrame(path, margesX, margesY, tipusDeteccio, frame)
    if ~exist('tipusDeteccio','var')
        tipusDeteccio = 'FAST';
    end
    if ~exist('frame','var')
        frame=im2double(rgb2gray(imread(path)));
    end
    %Els frames contenen marges negres que no interessen.
    frame = frame(margesY:end-1, margesX:end-margesX, :); %En el cas de la Y nom�s t� un marge superior, no inferior
    %Ens quedem nom�s amb les linies senars
    frame = frame(1:2:end,:,:);
    %Detect feature points
    if strcmpi(tipusDeteccio, 'SURF')                    %4
        points = detectSURFFeatures(frame, 'NumOctaves', 4, 'NumScaleLevels', 4, 'MetricThreshold', 1000);
    elseif strcmpi(tipusDeteccio, 'MSER')
        points = detectMSERFeatures(frame, 'ThresholdDelta', 2, 'RegionAreaRange', [30 14000], 'MaxAreaVariation', 0.25);
    elseif strcmpi(tipusDeteccio, 'BRISK')              
        points = detectBRISKFeatures(frame, 'MinContrast', 0.01, 'MinQuality', 0.001, 'NumOctaves', 4, 'ROI', [1 1 size(frame,1) size(frame,1)]);
    elseif strcmpi(tipusDeteccio, 'Eigen')     
        points = detectMinEigenFeatures(frame,'MinQuality', 0.01, 'FilterSize', 5, 'ROI', [1 1 size(frame,1) size(frame,1)]);
    elseif strcmpi(tipusDeteccio, 'FAST')     
        points = detectFASTFeatures(frame,'MinQuality', 0.01, 'MinContrast', 0.01, 'ROI', [1 1 size(frame,1) size(frame,1)]);
    elseif strcmpi(tipusDeteccio, 'Harris')     
        points = detectHarrisFeatures(frame,'MinQuality', 0.01, 'FilterSize', 5, 'ROI', [1 1 size(frame,1) size(frame,1)]);
    else
       err = MException('TypeError:UnrecognizedType', ...
        'El tipus especificat ha de ser un dels seg�ents: SURF, MSER, BRISK, Eigen, FAST, Harris.'); 
       throw(err);
    end
        
    %Extract feature descriptors
    [features, points] = extractFeatures(frame, points, 'SURFSize', 64, 'Method', 'Auto');
    
end

function stableFrame = DescartarIniciVideo(tipusDeteccio, margesX, margesY, llindarRotacio, baseDir, li)
    %Aquesta funci� retorna el n� del frame on el video s'estabilitza. El
    %criteri per considerar que el video s'ha estabilitzat �s que es doni
    %un m�nim de frames consecutius (minNFramesEstables) on es compleixi:
    %  - El drone no est� parat/quiet (la translaci� ha de ser m�s gran que
    %       llindarTranslaci�)
    %  - No hi ha frames sorollosos. Aix� tan pot ser perqu� el frame �s
    %  soroll�s, com perqu� l'escala superi el llindarEscala, i per tant el
    %  drone estigui en fase d'enlairar-se
    % Si es troben minNFramesEstables consecutius que compleixin aquestes
    % condicions, el primer frame de tots els consecutius ser� el frame per
    % on es comen�ar� a realitzar el feature matching.
    
    sz = size(li,1);
    llindarTranslacio = 1; %Llindar pel qual direm que el drone est� quiet.
    llindarEscala = 0.001; %Aquest llindar �s m�s restrictiu que el llindar general de l'escala.
    %Es podria donar el cas de que el drone estigu�s quiet, es produ�s
    %soroll, per� no suficient com per detectar-lo, i com a tal es compl�s
    %que hi ha moviment i no soroll. Per evitar aix�, hi ha d'haver un
    %m�nim de frames seguits sense soroll i amb moviment.
    %Nota: per no soroll s'ent�n una escala pr�xima a 1(+-0.08).
    minNFramesEstables = 10;
    [~, f2Points, f2Features] = LlegirFrame([baseDir li(1).name], margesX, margesY, tipusDeteccio);
    i = 1;
    framesOK = 0; %Nombre de frames sense soroll i amb moviment
    %Mentre (no hi hagi moviment OR hi hagi frames sorollosos) AND no s'arribi al final
    while framesOK < minNFramesEstables && i<sz
%         f1 = f2;
        f1Points = f2Points;
        f1Features = f2Features;
        [~, f2Points, f2Features] = LlegirFrame([baseDir li(i+1).name], margesX, margesY, tipusDeteccio);
        %Match features by using their descriptors
        pairs = matchFeatures(f1Features, f2Features, 'MaxRatio', 0.6);

        matchedf1Points = f1Points(pairs(:, 1),:);
        matchedf2Points = f2Points(pairs(:, 2),:);

        %Escalem la Y per dos, ja que en el frame nom�s hem agafat la meitat de
        %les files
        matchedf1Points.Location(:,2) = matchedf1Points.Location(:,2)*2;
        matchedf2Points.Location(:,2) = matchedf2Points.Location(:,2)*2;

        [tform, inlierf1Points, inlierf2Points, status] = ...
        estimateGeometricTransform(matchedf1Points, matchedf2Points, 'similarity', 'MaxNumTrials', 100000000, 'Confidence', 99.99999, 'MaxDistance', 1.5);
        if status ~= 0 %No hi ha suficients inliers
            fprintf('%d/%d\tNo hi ha prous inliers.\n', i, sz);
          
            framesOK = 0;
        else
%             figure(5); 
%             showMatchedFeatures(f1, f2, inlierf1Points, inlierf2Points);
%             title('Matched Points (Inliers Only)');
%             legend('ptsFrame1','ptsFrame2');
%             hold off;
            Tinv  = tform.invert.T;
            ss = Tinv(2,1);
            sc = Tinv(1,1);
            scale = sqrt(ss*ss + sc*sc);
            theta = atan2(ss,sc);%*180/pi;
            translation = Tinv(3,1:2);
            %Mirem que el frame no sigui soroll�s (o que el drone no estigui quiet)
            if abs(theta) > llindarRotacio || abs(scale-1) > llindarEscala
                framesOK = 0;
                fprintf('%d/%d\tFrame soroll�s o drone enlairant-se.\tEscala: %.2f\tRotaci� entre frames: %.2f�\tTranslaci�: %.2f\n', i, sz, scale, theta, sum(abs(translation)));
            elseif sum(abs(translation)) < llindarTranslacio
                framesOK = 0;
                fprintf('%d/%d\tFrame sense moviment.\tEscala: %.2f\tRotaci� entre frames: %.2f�\tTranslaci�: %.2f\n', i, sz, scale, theta, sum(abs(translation)));
            else
                fprintf('%d/%d\tEscala: %.2f\tRotaci� entre frames: %.2f�\tTranslaci�: %.2f\n', i, sz, scale, -theta, sum(abs(translation)));
                framesOK = framesOK +1;
            end
        end
        i = i + 1;
        %pause();
    end
    stableFrame = i-framesOK; %Hauria de donar vora 544
    fprintf('El video s''estabilitza a partir del frame %d, �s a dir, el drone no est� parat ni s''est� enlairant.\n', stableFrame);
end

function [f1Points, f2Points]=  RectificarPosicions(f1Points, f2Points, f2, degree, orientation)
    %%Rectificar rotaci�
    %Primer de tot rotem els punts del frame 2 perqu� tinguin la mateixa
    %correspondencia amb els punts del frame 1.
    %Per mantenir el sistema de refer�ncia, el frame 2 rotar� sobre el seu
    %centre de massa, i no sobre el (0, 0). Per tant es far�:
    %punts(x,y)*T(-sz(1)/2,-sz(2)/2)*R(alpha)*T(sz(1)/2,sz(2)/2)
     %Per exemple, si el robot est� quiet i nom�s rota, hauria de marcar
    %despla�ament 0.
    %1a forma: multiplicaci� de matrius
    alpha=degree*pi/180;
    T1 = eye(3); T1(3,1:2) = [-size(f2,2)/2 -size(f2,1)/2];
    T2 = T1; T2(3,1:2) = [size(f2,2)/2 size(f2,1)/2];
    R = eye(3); R(1:2,1:2) = [cos(alpha) sin(alpha); -sin(alpha) cos(alpha)];
    f2Points = [f2Points ones(size(f2Points,1),1)]*T1*R*T2;
    f2Points = f2Points(:,1:2);
    %2a forma: estalviar-se multiplicacions
    %T(-sz(1)/2,-sz(2)/2)
%     T = repmat([size(f2,2)/2 size(f2,1)/2],size(f2Points,1),1); 
%     f2Points = f2Points-T; 
%     %R(alpha)
%     xp = f2Points(:,1)*cos(alpha)-f2Points(:,2)*sin(alpha); 
%     yp = f2Points(:,1)*sin(alpha)+f2Points(:,2)*cos(alpha);
%     f2Points = [xp yp];
%     %T(sz(1)/2,sz(2)/2)
%     f2Points = f2Points+T;

    %%Dibuixar resultat (opcional)
    %Si f2Points t� algun punt negatiu donar� problemes per representar-ho
    %Borrem aquest punt tant en f1Points com en f2Points
    dibuixar_resultat = false;
    if dibuixar_resultat
        [rows, ~] = find(f2Points<0);
        if ~isempty(rows)
            rows = unique(rows); %Mirem a quines files el nombre �s negatiu
            mask =  true(size(f2Points)); %Creem una m�scara de 1=positiu 0=negatiu
            mask(rows,:) = zeros(length(rows),size(f2Points,2));
            %Passem aquesta m�scara pel f2Points, i li donem format (sin� �s un array)
            auxf2 = reshape(f2Points(mask),size(f2Points,1)-length(rows),size(f2Points,2));
            auxf1 = reshape(f1Points(mask),size(f1Points,1)-length(rows),size(f1Points,2));
        else
           auxf1 = f1Points; 
           auxf2 = f2Points; 
        end
        auxf1(:,2) = auxf1(:,2)/2;
        auxf2(:,2) = auxf2(:,2)/2;
        a = append(SURFPoints,auxf1);
        b = append(SURFPoints,auxf2);

        r = imrotate(f2, -degree, 'crop');
        figure(6); 
        showMatchedFeatures(f1, r, a, b);
        title('Matched Points (Inliers Only)');
        legend('ptsFrame1','ptsFrame2');
        hold off;
%         figure(7); 
%         showMatchedFeatures(f1, r, a, b, 'montage');
%         title('Matched Points (Inliers Only)');
%         legend('ptsFrame1','ptsFrame2');
%         hold off;
    end

    %%Rectificar orientaci� 
    %Apliquem la rotaci� de l'orientaci� acumulada perqu� X i Y sempre
    %siguin sobre el mateix sistema de refer�ncia (mapa), i no depengui de la
    %posici� del dron.
    alpha = -orientation*pi/180; %Orientation ha de ser positiu perqu� es fa el canvi
    %d'orientaci� abans de fer offsetY = y2-y1. I �s que s'ha de remarcar
    %que les imatges s�n matrius, i per tant la Y augmenta cap abaix
    %(canviant aix� el sentit del vector, i per tant la rotaci� s'ha de fer
    %al sentit oposat. Per aix� mateis el imrotate fa -degree)
    xp1=f1Points(:,1)*cos(alpha)-f1Points(:,2)*sin(alpha);
    yp1=f1Points(:,1)*sin(alpha)+f1Points(:,2)*cos(alpha);
    xp2=f2Points(:,1)*cos(alpha)-f2Points(:,2)*sin(alpha);
    yp2=f2Points(:,1)*sin(alpha)+f2Points(:,2)*cos(alpha);
    f1Points = [xp1 yp1];
    f2Points = [xp2 yp2];

end

function [sum] = DibuixarTrajectoria(offsetX, offsetY, sum)

            if size(offsetX,1) == 1
                sum(1) = offsetX(1);
                sum(2) = offsetY(1);
            end
            figure(1); %set(1,'Visible','off');
            hold on; axis equal; %axis([-500 500 -500 500]);
            aux = sum;
            sum(1) = sum(1) + offsetX(end);
            sum(2) = sum(2) + offsetY(end);
            %plot(offsetX(1:i), offsetY(1:i));
            title(sprintf('Frame %d', size(offsetX,1)));
            plot([aux(1) sum(1)], [aux(2) sum(2)]);
            hold off;
end