function [p1, p2, offset] = EmparellarPunts(p1, p2)
%Si hem entrat en aquesta funci� �s perqu� hi ha algun punt que s'ha 
%detectat en un lloc i en l'altre no, i per aix� no podem fer servir RANSAC,
%ja que requereix que els punts estiguin emparellats. S'utilitzar�
%programaci� din�mica, amb una idea semblant al string matching, per
%emparellar tots els punts amb el m�nim cost
    costEsborrar = 1000;
    %%Inicialitzaci� taula M
    global M;
    M = nan(size(p2,1)+1,size(p1,1)+1);
    M(1,1) = 0;
    M(1,2:end) = (1:size(p1,1)).*costEsborrar;
    M(2:end,1) = (1:size(p2,1))'.*costEsborrar;
    %Taula d'offsets
    global O;
    O = cell(size(p2,1)+1,size(p1,1)+1);
    %Taula d'unions
    global U;
    U= cell(size(p2,1)+1,size(p1,1)+1);
    
    %%Primera crida recursiva
    C(size(M,1),size(M,2),p1,p2,costEsborrar);
    
    offset = O{end,end};
    unions = U{end,end};
    
    %%Emparellem els punts
    p1 = p1(unions(:,1),:);
    p2 = p2(unions(:,2),:);
end
%Algo no va b�, el despla�ament �s molt semblant en tots els casos, per� el
%cost augmenta molt
function cost = C(i,j,p1,p2,costEsborrar)
    global M;
    global O;
    global U;
    if not(isnan(M(i,j)))
       cost = M(i,j); 
       return
    end
    llistaCostos = zeros(3,1);
    llistaCostos(1) = C(i-1,j-1,p1,p2,costEsborrar); %Unir jp1 amb ip2
    if not(isempty(O{i-1,j-1})) %En la 1a uni� el cost �s 0
        llistaCostos(1) = llistaCostos(1)+...
            pdist([p2(i-1,:)+O{i-1,j-1}; p1(j-1,:)]);
    end
    llistaCostos(2) = C(i-1,j,p1,p2,costEsborrar)+costEsborrar; %Esborrar ip2
    llistaCostos(3) = C(i,j-1,p1,p2,costEsborrar)+costEsborrar; %Esborrar jp1
    [cost, ind] = min(llistaCostos);
    M(i,j) = cost;

    switch ind
        case 1
            offset = O{i-1,j-1};
            unions = U{i-1,j-1};
        case 2
            offset = O{i-1,j};
            unions = U{i-1,j};
        case 3
            offset = O{i,j-1};
            unions = U{i,j-1};
    end
            
    if ind==1
        %Emmagatzemem la uni�
        unions(end+1,:) = [j-1 i-1]; %p1 p2
    end
    U{i,j} = unions;
    if ind==1 && isempty(offset)
        %Si no existia cap offset pr�viament i s'ha realitzat una uni� (el
        %millor cost �s el d'una uni�), hem d'inicialitzar l'offset
        offset = p1(j-1,:)-p2(i-1,:);
    end
    O{i,j} = offset;
end