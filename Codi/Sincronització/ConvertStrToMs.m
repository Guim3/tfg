function ms = ConvertStrToMs(hour,min,sec,ms)
    hour = str2double(hour);
    min = str2double(min);
    sec = str2double(sec);
    ms = str2double(ms);
    ms = ((hour*60*60*1000)+(min*60*1000)+(sec*1000+ms));

end