%S'han d'eliminar els frames sorollosos. Per fer-ho, s'utilitza la
%normalized cross correlation per tal de buscar la semblan�a entre dos
%frames consecutius. Amb aix� podrem saber quan es produeixen canvis grans
%entre dos frames, que molt probablement seran a causa de soroll.
%S'analitzaran aquests canvis. Si es produeixen dos canvis de forma molt
%propera, podrem dir que es tracta de soroll, ja que normalment el soroll
%�s puntual i durant pocs frames. Per tant podem eliminar els frames de soroll.

close all, clear all

currentpath = cd('..');
parentpath = pwd();
cd(currentpath);
th = 0.75;
margesX = 14; 
margesY = 2; 
baseDir = [parentpath '\provaSoroll\'];
li=dir([baseDir '*.png']);
sz = 100;% size(li,1);
res = zeros(1,sz);
tic
for i=880:880+sz %1:sz-1   
    im=gpuArray(im2double(rgb2hsv(imread([baseDir li(i).name]))));
    %Ens quedem nom�s amb les linies senars
    im = im(1:2:end,:,3);
    im2=gpuArray(im2double(rgb2hsv(imread([baseDir li(i+1).name]))));
    im2 =im2(1:2:end,:,3); 
    im = im(margesY:end, margesX:end-margesX, :); %Els frames contenen marges negres que no interessen.
    im2 = im2(margesY:end, margesX:end-margesX, :); %En el cas de la Y nom�s t� un marge superior, no inferior
    %Fem NCC per tot el frame 2
    aux = gather(normxcorr2(im,im2));
    res(1,i) = max(aux(:));
    
end
toc
figure(1); plot(res);
%Tots els frames per sota el th s�n eliminats
mask = res>th;
li = li(mask);
res = zeros(1,size(li,1));
for i=1:size(li,1)-1   
    im=gpuArray(im2double(rgb2hsv(imread([baseDir li(i).name]))));
    %Ens quedem nom�s amb les linies senars
    im = im(1:2:end,:,3);
    im2=gpuArray(im2double(rgb2hsv(imread([baseDir li(i+1).name]))));
    im2 =im2(1:2:end,:,3); 
    im = im(margesY:end, margesX:end-margesX, :); %Els frames contenen marges negres que no interessen.
    im2 = im2(margesY:end, margesX:end-margesX, :); %En el cas de la Y nom�s t� un marge superior, no inferior
    %Fem NCC per tot el frame 2
    aux = gather(normxcorr2(im,im2));
    res(1,i) = max(aux(:));
    
end
figure(2);plot(res);