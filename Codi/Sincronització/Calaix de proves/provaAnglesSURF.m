%tform.invert.T --> matriu de transformaci� que mapeja els punts de
%original a distorted
%pDistorted = [inlierOriginal(6).Location 1]*tform.invert.T;
%
%tform.T --> mapeja punts de distorted a original
%pOriginal = [inlierDistorted(6).Location 1]*tform.T;

%Imatge traslladada -50 (X) i -100 (Y) i rotada 30�
original = im2double(imread('cameraman.tif'));
theta = 30;
traslacioX = 100;
traslacioY = 50;
distorted = original;
distorted = imrotate(distorted,theta, 'crop'); 
distorted = [ones(size(distorted,1),traslacioX) distorted]; %Traslaci� 100 cap a la dreta
distorted = distorted(:,1:end-traslacioX);
distorted = [ones(traslacioY,size(distorted,2)); distorted];
distorted = distorted(1:end-traslacioY,:);
ptsOriginal  = detectSURFFeatures(original);
ptsDistorted = detectSURFFeatures(distorted);
[featuresOriginal,  validPtsOriginal]  = extractFeatures(original,  ptsOriginal);
[featuresDistorted, validPtsDistorted] = extractFeatures(distorted, ptsDistorted);
indexPairs = matchFeatures(featuresOriginal, featuresDistorted);
matchedOriginal  = validPtsOriginal(indexPairs(:,1));
matchedDistorted = validPtsDistorted(indexPairs(:,2));
figure;
showMatchedFeatures(original,distorted,matchedOriginal,matchedDistorted);
%showMatchedFeatures(original,distorted,SURFPoints,SURFPoints);
[tform, inlierDistorted, inlierOriginal] = estimateGeometricTransform(...
    matchedDistorted, matchedOriginal, 'similarity');

%Imatge nom�s traslladada -50 (X) i -100 (Y)
distorted2 = original(50:end,100:end); 
ptsOriginal  = detectSURFFeatures(original);
ptsDistorted = detectSURFFeatures(distorted2);
[featuresOriginal,  validPtsOriginal]  = extractFeatures(original,  ptsOriginal);
[featuresDistorted, validPtsDistorted] = extractFeatures(distorted2, ptsDistorted);
indexPairs = matchFeatures(featuresOriginal, featuresDistorted);
matchedOriginal  = validPtsOriginal(indexPairs(:,1));
matchedDistorted = validPtsDistorted(indexPairs(:,2));
figure;
showMatchedFeatures(original,distorted2,matchedOriginal,matchedDistorted);
[tform2, inlierDistorted, inlierOriginal] = estimateGeometricTransform(...
    matchedDistorted, matchedOriginal, 'similarity');

disp('Matriu de transformaci� de la imatge traslladada i rotada');
disp(tform.T);

disp('Matriu de transformaci� de la imatge nom�s traslladada');
disp(tform2.T);

disp('La traslaci� �s correcte en la segona imatge (100 i 50), per� amb la rotaci� la traslaci� deixa de ser acurada.');
disp('Qu� caldria fer per tenir la traslaci� despr�s d''invertir la rotaci�?');