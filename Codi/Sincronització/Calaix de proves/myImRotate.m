function imROutIn = myImRotate(im, degree, marginValue, bucles)
    %La interpolaci� �s del ve� m�s proper. MarginValue �s el valor que li
    %vols donar a la zona de la imatge que queda en "negre" despr�s de la
    %rotaci�. Pot ser �til de cara a discriminar aquesta zona i no
    %processar-la.
    if ~exist('marginValue','var')
        marginValue = 0;
    end
    if ~exist('bucles','var')
        bucles = false;
    end
    im = im2double(im);
    alpha=-degree*pi/180;
    %Calculem la mida de la imatge despr�s de la rotaci�
    lim=[1, size(im,2), 1 ,size(im,2); 1, 1, size(im,1) ,size(im,1) ];
    plim=[cos(alpha) -sin(alpha);  sin(alpha) cos(alpha)]*lim;
    xplim=[floor(min(plim(1,:))), ceil(max(plim(1,:)))];
    yplim=[floor(min(plim(2,:))), ceil(max(plim(2,:)))];
    imROutIn=double(zeros(yplim(2)-yplim(1)+1,xplim(2)-xplim(1)+1));
    imROutIn(:) = marginValue;
    beta=-alpha;

    if not(bucles)
        %Rotaci� output->input sense bucles (m�s r�pid, per� m�s mem�ria)
        [j, i]=meshgrid(1:size(imROutIn,2),1:size(imROutIn,1)); % x-> j, y->i
        xp=j+xplim(1);%+1;
        yp=i+yplim(1);%+1;
        x=round(xp*cos(beta)-yp*sin(beta));
        y=round(xp*sin(beta)+yp*cos(beta));
        %El que s'ha de fer �s eliminar totes les y i les x que queden fora del rang de la imatge 'im'
        mask = (x > 0 & x <= size(im,2) & y > 0 & y <= size(im,1));
        imROutIn(sub2ind(size(imROutIn),i(mask),j(mask)))=im(sub2ind(size(im),y(mask),x(mask)));
    else
        %Rotaci� output->input amb bucles
        for i=1:size(imROutIn,1)
            for j=1:size(imROutIn,2)
                xp=j+xplim(1)+1;
                yp=i+yplim(1)+1;
                %Fas la rotaci� inversa (beta = -alpha)
                %Perqu� vas a buscar el p�xel a la imatge original (output -> input)
                x=round(xp*cos(beta)-yp*sin(beta));
                y=round(xp*sin(beta)+yp*cos(beta));
                if(x > 0 && x <= size(im,2) && y > 0 && y <= size(im,1))
                    imROutIn(i,j)=im(y,x);
                end
            end
        end

end