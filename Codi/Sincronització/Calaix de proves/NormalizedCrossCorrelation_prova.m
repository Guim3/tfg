close all, clear all

currentpath = cd('..');
parentpath = pwd();
cd(currentpath);

baseDir = [parentpath '\201405161455\prova\'];
li=dir([baseDir '*.png']);

%Primera idea: realitzo un tall del primer frame, i aquest tall ser� la
%matriu que passar� per tot el 2n frame amb una normalized cross
%correlation. Aix� ho far� amb diferents talls. �s a dir, cada tall del 1r
%frame el passo per tota el 2n frame. Em quedo amb aquell tall que hagi
%aconseguit una semblan�a m�s elevada amb alguna posici� del 2n frame, utilitzant 
%aix� aquest punt de refer�ncia (tant la zona del 1r frame com del 2n.
%D'aquesta forma puc saber com s'ha mogut el 1r frame respecte el 2n.
%Inicialitzaci� par�metres
debug = true;
llindar_valor_maxim = 0.2;
llindar_quantitat_valors_maxims = 15;
midaX = 10; margesX = 1; 
midaY = 10; margesY = 1; 
sz =  size(li,1);
initFrame = 1;
dades = struct();
dades.FrameName = cell(sz,1);
dades.FrameName{1} = [li(1).name];
dades.offsetX = zeros(sz,1); dades.offsetY = zeros(sz,1); %despla�ament respecte l'anterior frame
k = 1;
for i=1:sz-1   
    im=gpuArray(im2double(rgb2hsv(imread([baseDir li(i).name]))));
    im = im(:,:,3);
    im2=gpuArray(im2double(rgb2hsv(imread([baseDir li(i+1).name]))));
    im2 = im2(:,:,3);
    maxim = -inf;
    maximProvisional = -inf;
    trobat = false;
    %Per cada tall del frame 1
    for offset1X=1:midaX:size(im,2)-midaX
        for offset1Y=1:midaY:size(im,1)-midaY
            %Retallem la imatge
            %[xmin ymin width height]
            rect = [offset1X offset1Y midaX-1 midaY-1];
            template = imcrop(im,rect);
            %Si tots els elements del template s�n iguals
            if all(template(:) == template(1))
               %Modifiquem lleugerament un dels elements, ja que sin�
               %no podem fer normxcorr2
               if template(1) < 1
                   template(1) = template(1) + 0.001;
               else
                   template(1) = template(1) - 0.001;
               end
            end
                
            %Fem NCC per tot el frame 2
            res = normxcorr2(template(:,:,:),im2(:,:,:));
            aux = max(res(:));
            if maxim < aux
               %Mirem si hi ha molts valors elevats. Busquem el valor m�xim i a
               %la vegada un valor singular que sigui molt diferenciat de la
               %resta de la imatge, per guanyar aix� m�s robustesa.
               quantitat_de_valors_elevats = res>aux-llindar_valor_maxim ;
               quantitat_de_valors_elevats = sum(quantitat_de_valors_elevats(:));
               if quantitat_de_valors_elevats < llindar_quantitat_valors_maxims
                   trobat = true;
                   maxim = aux;
                   [ypeak, xpeak] = find(res==aux);
                   resMax = res;
                   rectMax = rect;
               elseif not(trobat) && maximProvisional < aux
                   %Emmagatzemar un m�xim provisional per si cap m�xim compleix la
                   %condici� del llindar de valors elevats
                   maximProvisional = aux;
                   [ypeak, xpeak] = find(res==aux);
                   resMax = res;
                   rectMax = rect;          
               end

            end

        end
    end
    y = gather(ypeak)-size(template,1);
    x = gather(xpeak)-size(template,2);
    dades.offsetX(k+1) = rectMax(1)-x;
    dades.offsetY(k+1) = y-rectMax(2);
    dades.FrameName{k+1} = li(i+1).name;
    k = k + 1;
    fprintf('%d/%d\n',i-initFrame+1,sz);
 %   figure(1); hAx1 = axes; 
%imshow(im, 'Parent', hAx1); title('Frame 1', 'FontWeight', 'Bold');
%figure(2); hAx2 = axes;
%imshow(im2, 'Parent', hAx2); title('Frame 2', 'FontWeight', 'Bold');

%h = imrect(hAx1, rectMax);
%h2= imrect(hAx2, [x, y, size(template,2), size(template,1)]);
%figure(3); clf; hold on;clc
%title('Grau de correlaci�', 'FontWeight', 'Bold');
%surf(resMax), shading flat;
%hold off;
% pause;
% delete(h);
% delete(h2);
end
figure(1);hold on;
%Est� malament, has de fer el plot del sumatori de tot el que va abans
sum(1) = dades.offsetX(1);
sum(2) = dades.offsetY(1);
for i=2:size(dades.offsetX,1)
    aux = sum;
    sum(1) = sum(1) + dades.offsetX(i);
    sum(2) = sum(2) + dades.offsetY(i);
    %plot(dades.offsetX(1:i), dades.offsetY(1:i));
    plot([aux(1) sum(1)], [aux(2) sum(2)]);
    
    pause;
end

%Creaci� fitxer
writetable(struct2table(dades), 'trajectoria.txt','Delimiter','\t');
%Per llegir: 
%tdfread('trajectoria.txt','\t');
% figure(1); hAx1 = axes; 
% imshow(im, 'Parent', hAx1); title('Frame 1', 'FontWeight', 'Bold');
% figure(2); hAx2 = axes;
% imshow(im2, 'Parent', hAx2); title('Frame 2', 'FontWeight', 'Bold');
% 
% h = imrect(hAx1, rectMax);
% h2= imrect(hAx2, [x, y, size(template,2), size(template,1)]);
% figure(3); clf; hold on;
% title('Grau de correlaci�', 'FontWeight', 'Bold');
% surf(resMax), shading flat;
% hold off;
% delete(h);
% delete(h2);

%No nom�s hauria d'agafar el que t� una coincid�ncia m�s gran
%sin� tamb� el que sigui m�s singular. Sin� podria passar que un tros
%gris de carretera fos el que tingu�s m�s semblan�a amb l'altra frame,
%per� despr�s probablement no trobaria el tros gris de carretera
%concret a l'altre frame.
%Tamb� es podrien agafar els X amb m�s coincid�ncia, per ser m�s robust.