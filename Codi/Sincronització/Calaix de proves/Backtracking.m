function [outp1, outp2, offset] = Backtracking(p1, p2)
    %Si hem entrat en aquesta funci� �s perqu� hi ha algun punt que s'ha 
    %detectat en un lloc i en l'altre no, i per aix� no podem fer servir RANSAC,
    %ja que requereix que els punts estiguin emparellats. El backtracking
    %no emparella els punts, per� s� que retornar� el offset que minimitza
    %les dist�ncies entre els punts de p1 i p2.
    
    
    %Les LVA, LVNA (i D) contindran els �ndex de p1 (i p2), perqu� volem saber
    %l'emparellament, no el valor del punt en s� (aix� ho volem al final nom�s)
    LVA = []; %Llista variables assignades
    LVNA = 1:size(p1,1); %Llista variables no assignades
    D = 1:size(p2,1); %Domini
    
    %Aix� no serveix, el backtracking no emparella els punts (necessitariem
    %string matching)
    outp1 = zeros(size(LVA,1), size(p1,2)); 
    outp2 = outp1;
    for i=1:size(LVA,1)
        outp1(i,:) = p1(LVA(i,1),:);
        outp2(i,:) = p2(LVA(i,2),:);
    end
end
%PROBLEMA: com ho fas per eliminar 
function recursiveBacktracking


end