close all, clear all

currentpath = cd('..');
parentpath = pwd();
cd(currentpath);

baseDir = [parentpath '\201405161455\'];
li=dir([baseDir '*.png']);

%Primera idea: realitzo un tall del primer frame, i aquest tall ser� la
%matriu que passar� per tot el 2n frame amb una normalized cross
%correlation. Aix� ho far� amb diferents talls. �s a dir, cada tall del 1r
%frame el passo per tota el 2n frame. Em quedo amb aquell tall que hagi
%aconseguit una semblan�a m�s elevada amb alguna posici� del 2n frame, utilitzant 
%aix� aquest punt de refer�ncia (tant la zona del 1r frame com del 2n.
%D'aquesta forma puc saber com s'ha mogut el 1r frame respecte el 2n.

%Inicialitzaci� par�metres
debug = true;
llindar_valor_maxim = 0.10;
llindar_quantitat_valors_maxims = 15;
midaX = 23*2; margesX = 14; 
midaY = 22*2; margesY = 2; 
rangAngle = -3:1.5:3;%-3:1:3;%-3:1.5:3;
initFrame = 925;%933;
sz = 300;%52;% size(li,1);
dades = struct();
dades.FrameName = cell(sz,1);
dades.FrameName{1} = [li(initFrame).name];
dades.offsetX = zeros(sz,1); dades.offsetY = zeros(sz,1); %despla�ament respecte l'anterior frame
dades.grauRotacio= zeros(sz,1);
dades.orientacio = zeros(sz,1);
k = 1;
%Idea de la rotaci�: la faig jo mateix d'output-input, assignant a les
%zones 'negres' un valor 'NaN' o -1, per tal de que a posteriori pugui
%retallar les imatges evitant totes aquestes zones.

for i=initFrame:initFrame+sz-1 %1:sz-1   %Per cada frame
    im=gpuArray(im2double(rgb2hsv(imread([baseDir li(i).name]))));
    %Ens quedem nom�s amb les linies senars
    im = im(1:2:end,:,3);
    im2=gpuArray(im2double(rgb2hsv(imread([baseDir li(i+1).name]))));
    im2 =im2(1:2:end,:,3); 
    im = im(margesY:end, margesX:end-margesX, :); %Els frames contenen marges negres que no interessen.
    im2 = im2(margesY:end, margesX:end-margesX, :); %En el cas de la Y nom�s t� un marge superior, no inferior
    maxim = -inf;
    maximProvisional = -inf;
    trobat = false;
    for grauRotacio=rangAngle
        %Rotaci� imatge
        if grauRotacio~=0
           imAux = gpuArray(myImRotate(gather(im),grauRotacio,-1)); 
        else
           imAux = im;
        end
        %Per cada tall del frame 1
        for offset1X=1:midaX:size(im,2)-midaX
            for offset1Y=1:midaY:size(im,1)-midaY
                %Retallem la imatge
                %[xmin ymin width height]
                rect = [offset1X offset1Y midaX-1 midaY-1];
                template = imcrop(imAux,rect);
                %Despr�s de fer la rotaci�, els elements de fora la imatge
                %quedaran com a -1. Volem evitar agafar un template amb -1.
                if not(any(template(:)==-1)) %sum(template(:)==-1) > 0
                    %Si tots els elements del template s�n iguals
                    if all(template(:) == template(1))
                       %Modifiquem lleugerament un dels elements, ja que sin�
                       %no podem fer normxcorr2
                       if template(1) < 1
                           template(1) = template(1) + 0.001;
                       else
                           template(1) = template(1) - 0.001;
                       end
                    end
                    %Fem NCC per tot el frame 2
                    res = normxcorr2(template(:,:,:),im2(:,:,:));
                    aux = max(res(:));
                    if maxim < aux
                       %Mirem si hi ha molts valors elevats. Busquem el valor m�xim i a
                       %la vegada un valor singular que sigui molt diferenciat de la
                       %resta de la imatge, per guanyar aix� m�s robustesa.
                       quantitat_de_valors_elevats = res>(aux-llindar_valor_maxim) ;
                       quantitat_de_valors_elevats = sum(quantitat_de_valors_elevats(:));
                       if quantitat_de_valors_elevats < llindar_quantitat_valors_maxims
                           trobat = true;
                           maxim = aux;
                           [ypeak, xpeak] = find(res==aux);
                           resMax = res;
                           rectMax = rect;
                           rotacioMax = grauRotacio;
                           auxTemplate = template;
                       elseif not(trobat) && maximProvisional < aux
                           %Emmagatzemar un m�xim provisional per si cap m�xim compleix la
                           %condici� del llindar de valors elevats
                           maximProvisional = aux;
                           [ypeak, xpeak] = find(res==aux);
                           resMax = res;
                           rectMax = rect;
                           rotacioMax = grauRotacio;
                           auxTemplate = template;
                       end

                    end
                end
            end
        end
    end
    dades.FrameName{k+1} = li(i+1).name;
    dades.grauRotacio(k+1) = rotacioMax;
    dades.orientacio(k+1) = dades.orientacio(k)-rotacioMax;
    x1 = rectMax(1);
    y1 = rectMax(2);
    y2 = gather(ypeak)-size(template,1);
    x2 = gather(xpeak)-size(template,2);
    
    %Dibuixar els frames analitzats en aquesta iteraci� (debug)
    %DibuixarFrames(im, im2, x2, y2, template, rectMax, rotacioMax, resMax, auxTemplate);
    
    %Escalem la Y per dos, ja que en el frame nom�s hem agafat la meitat de
    %les files
    y1 = y1*2;
    y2 = y2*2;
    %El sistema de refer�ncia de x1 i y1 �s diferent al de x2 i y2 (si hi ha hagut rotaci�). Aix� s'ha de corregir.
    %Tamb� es corregeix l'orientaci� dels punts aplicant una rotaci� per tenir en compte l'orientaci� del
    %dron i aix� poder dibuixar la trajectoria correctament en el mapa.
    %(Per exemple si el dron est� rotat 90� i va cap endavant, en comptes
    %d'augmentar la Y (des del punt de vista del dron avan�a endavant) hauria
    %d'augmentar la X (des del punt de vista del mapa, es mou cap a
    %l'esquerre el dron)
    [offsetX, offsetY] = RectificarPosicions(x1, y1, x2, y2, im, im2, rotacioMax, dades.orientacio(k+1)); %Si rotessis frame 2 per fer quadrar a frame 1, seria orientacio(k)
   
    dades.offsetX(k+1) = offsetX; %L'offset �s respecte el dron. Si dos frames estan despla�ats 10 unitats cap a l'esquerre
    %aix� indica que el dron s'ha mogut 10 unitats cap a la dreta.
    dades.offsetY(k+1) = offsetY; %En el cas de la Y �s diferent, ja que en una imatge de matlab, la Y s'incrementa cap abaix.
    k = k + 1;
    fprintf('%d/%d\t Rotaci� entre frames: %.2f�\t Orientaci� dron: %.2f�\n',i-initFrame+1,sz, rotacioMax, dades.orientacio(k));

end
figure();hold on; axis([-500 500 -500 500]);
sum(1) = dades.offsetX(1);
sum(2) = dades.offsetY(1);
for i=2:size(dades.offsetX,1)
    aux = sum;
    sum(1) = sum(1) + dades.offsetX(i);
    sum(2) = sum(2) + dades.offsetY(i);
    %plot(dades.offsetX(1:i), dades.offsetY(1:i));
    title(sprintf('Frame %d', i));
    plot([aux(1) sum(1)], [aux(2) sum(2)]);
    
    %pause;
end

%Creaci� fitxer
writetable(struct2table(dades), 'trajectoria.txt','Delimiter','\t');
%Per llegir: 
%tdfread('trajectoria.txt','\t');
% figure(1); hAx1 = axes; 
% imshow(im, 'Parent', hAx1); title('Frame 1', 'FontWeight', 'Bold');
% figure(2); hAx2 = axes;
% imshow(im2, 'Parent', hAx2); title('Frame 2', 'FontWeight', 'Bold');
% 
% h = imrect(hAx1, rectMax);
% h2= imrect(hAx2, [x, y, size(template,2), size(template,1)]);
% figure(3); clf; hold on;
% title('Grau de correlaci�', 'FontWeight', 'Bold');
% surf(resMax), shading flat;
% hold off;
% delete(h);
% delete(h2);

%No nom�s hauria d'agafar el que t� una coincid�ncia m�s gran
%sin� tamb� el que sigui m�s singular. Sin� podria passar que un tros
%gris de carretera fos el que tingu�s m�s semblan�a amb l'altra frame,
%per� despr�s probablement no trobaria el tros gris de carretera
%concret a l'altre frame.
%Tamb� es podrien agafar els X amb m�s coincid�ncia, per ser m�s robust.

