function [offsetX, offsetY, x1, y1, x2, y2] = RectificarPosicionsNCC(x1, y1, x2, y2, im, im2, degree, orientation)
    
%%Rectificar rotaci�
%Aix� est� malament. Amb aix� l'�nic que fas �s posar la cantonada superior
%esquerre en el punt 0 0 (�s el punt invariant de la rotaci�.
%     %Els punts x i y estan respecte la imatge 'im' rotada (que no t� el mateix tamany que la 'im' sense rotar). 
%     %Per� necessitem saber la posici� original de x i y en el sistema de refer�ncia de la imatge 'im' sense rotar
%     if degree ~= 0
%         alpha=-degree*pi/180;
%         %Calculem la mida de la imatge despr�s de la rotaci�
%         lim=[1, size(im,2), 1 ,size(im,2); 1, 1, size(im,1) ,size(im,1) ];
%         plim=[cos(alpha) -sin(alpha);  sin(alpha) cos(alpha)]*lim;
%         xplim=[floor(min(plim(1,:))), ceil(max(plim(1,:)))];
%         yplim=[floor(min(plim(2,:))), ceil(max(plim(2,:)))];
% 
%         x1=x1+xplim(1)+1;
%         y1=y1+yplim(1)+1;
%     end
% Fi malament
    %Els punts x i y de la imatge 1 i la imatge 2 no estan en el mateix
    %sistema de refer�ncia. El que farem ser� centrar la imatge 2 en la
    %imatge 1 rotada (en cas de que ho estigui). I aix� podrem comparar b�
    %les dist�ncies.
    if degree ~= 0
         alpha=degree*pi/180;
         %Calculem la mida de la imatge despr�s de la rotaci�
         lim=[1, size(im,2), 1 ,size(im,2); 1, 1, size(im,1) ,size(im,1) ];
         plim=[cos(alpha) -sin(alpha);  sin(alpha) cos(alpha)]*lim;
         xplim=[floor(min(plim(1,:))), ceil(max(plim(1,:)))];
         yplim=[floor(min(plim(2,:))), ceil(max(plim(2,:)))];
         %S'ha de centrar la imatge 1 en la imatge 2, per tant les meitats
         %(X Y) de la imatge 1 i la imatge 2 han de coincidir.
         meitatIm = [yplim(2)-yplim(1) xplim(2)-xplim(1)]./2;
         meitatIm2 = size(im2)./2;
         offset = round(meitatIm-meitatIm2);%La im1 est� rotada, per tant ser� sempre m�s gran o igual que la im2
         x2=x2+offset(2);
         y2=y2+offset(1);
    end
    
    %No s'ha d'invertir rotaci� ja que aix� el despla�ament entre el frame 1
    %i frame 2 ser� un despla�ament invariant a rotaci� (no tindr� en
    %compte els despla�aments provocats per una rotaci�).   
    %Per exemple, si el robot est� quiet i nom�s rota, hauria de marcar
    %despla�ament 0.
    %beta = -alpha;
    %x=round(xp*cos(beta)-yp*sin(beta));
    %y=round(xp*sin(beta)+yp*cos(beta));
     
 %%Despla�ament
    offsetX = x1-x2; %L'offset �s respecte el dron. Si dos frames estan despla�ats 10 unitats cap a l'esquerre
    offsetY = y2-y1;   %aix� indica que el dron s'ha mogut 10 unitats cap a la dreta.
                     %En el cas de la Y �s diferent, ja que en una imatge de matlab, la Y s'incrementa cap abaix.
    
 %%Rectificar orientaci� 
    %Apliquem la rotaci� de l'orientaci� acumulada perqu� X i Y sempre
    %siguin sobre el mateix sistema de refer�ncia, i no depengui de la
    %posici� del dron.
    if orientation ~= 0
        alpha = orientation*pi/180;
        xp=round(offsetX*cos(alpha)-offsetY*sin(alpha));
        yp=round(offsetX*sin(alpha)+offsetY*cos(alpha));
        offsetX=xp;
        offsetY=yp;
    end
end
