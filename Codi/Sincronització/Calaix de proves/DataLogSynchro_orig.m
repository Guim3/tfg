function [ output_args ] = DataLogSynchro_orig( input_args )
%LOGPROCESS Summary of this function goes here
%   Detailed explanation goes here
currentpath = cd('..');
cd('..');
cd('..');
parentpath = pwd();
cd(currentpath);

baseDir = [parentpath '\201405161414\UAVLog\Processed\'];
%baseDir = '\\srvfile.cvc.uab.es\adas\FireWATCHER\DadesFirewatcher\SequenciesPropies\201405161414\UAVLog\Processed\';
baseName = '2014-05-16 14-14-08 - ';  

baseDir = [parentpath '\201405161455\UAVLog\Processed\'];
%baseDir = '\\srvfile.cvc.uab.es\adas\FireWATCHER\DadesFirewatcher\SequenciesPropies\201405161455\UAVLog\Processed\';
baseName = '2014-05-16 14-56-51 - ';
  
gpsFile = [baseDir baseName 'GPS.tsv'];
imuFile = [baseDir baseName 'IMU.tsv'];
ctunFile = [baseDir baseName 'CTUN.tsv'];
ntunFile = [baseDir baseName 'NTUN.tsv'];
attFile = [baseDir baseName 'ATT.tsv'];
evFile = [baseDir baseName 'EV.tsv'];

framesFile = [baseDir baseName 'Frames.tsv'];

gpsImuFile = [baseDir baseName 'GPS-IMU-TimeMS.tsv'];

gpsImuLog = ReadLogTSVData(gpsImuFile);

gpsLog = ReadLogTSVData(gpsFile);
imuLog = ReadLogTSVData(imuFile);
ctunLog = ReadLogTSVData(ctunFile);
ntunLog = ReadLogTSVData(ntunFile);
attLog = ReadLogTSVData(attFile);
evLog = ReadLogTSVData(evFile);
ctunLog = EstimateTimeMS(ctunLog, gpsImuLog);
ntunLog = EstimateTimeMS(ntunLog, gpsImuLog);
attLog = EstimateTimeMS(attLog, gpsImuLog);
evLog = EstimateTimeMS(evLog, gpsImuLog);

factor = 1/60000;

framesLog = ReadLogTSVData(framesFile);

framesLog = AssignFramesOrientationAngle(framesLog);

% Cerca del frame aproximadament al final de la pujada del UAV
upEventIndexes = find(ismember(num2cell(framesLog.Ev,2),'UP      '))

endUpEvent = upEventIndexes(end)

framesEndUpTimeMS = framesLog.TimeMS(endUpEvent)
framesLog.Ev(endUpEvent,:)
framesLog.Ev(endUpEvent+1,:)

% Buscar l'al�ada a la que es vol generar imatges
% L'assumpci� aqu� es que les imatges es prendran des d'una 
% al�ada constant durant el vol, que ser� el valor m�xim a que es volar�
% (aprox)

thrAltUp = max(gpsLog.RelAlt)*0.9;

indexAltUp = find(gpsLog.RelAlt>=thrAltUp,1);

gpsEndUpTimeMS = gpsLog.T(indexAltUp);

offset = gpsEndUpTimeMS - framesEndUpTimeMS;

% bestOffset = offset;

% Busqueda emparellant timeMS de 'endUpEvent' amb els timeMS de indexAltUp
% en endavant (2 o 3 segons), per identificar el valor menor d'una m�trica.

nSegons = 7;
step = 20;

orientationMatchMetric = [];

framesLog.OrientationRads = framesLog.OrientationAngle*(pi/180);
cosYawImageAnnotation = cos(framesLog.OrientationRads);
sinYawImageAnnotation = sin(framesLog.OrientationRads);

attLog.YawRads = attLog.Yaw*(pi/180);
attLog.cosYaw = cos(attLog.YawRads);
attLog.sinYaw = sin(attLog.YawRads);
offsetValues = (offset-nSegons*1000):step:(offset+nSegons*1000);

for offsetI= offsetValues;
    
    % En lloc de interpolar el valor de Yaw, es desitjable interpolar el
    % seu sinus o cosinus, perqu� si no, entre 0 i 360 s'interpolara 180,
    % que el seu cosinus val -1. Per altra banda, si interpolem el seu     
    % cosinus obtenim el valor 1
    % yawValues = interp1(attLog.TimeMSMean, attLog.YawRads, framesLog.TimeMS+offsetI);

    cosYawValues = interp1(attLog.TimeMSMean, attLog.cosYaw, framesLog.TimeMS+offsetI);
    sinYawValues = interp1(attLog.TimeMSMean, attLog.sinYaw, framesLog.TimeMS+offsetI);
  
    isnanMask = ~isnan(cosYawValues);   

    cosDiffAngle = cosYawImageAnnotation.*cosYawValues+sinYawImageAnnotation.*sinYawValues;
    
    metric = mean(cosDiffAngle(isnanMask & framesLog.OrientationMask));

    orientationMatchMetric = [orientationMatchMetric; metric];
    
if 0
    cla;
     plot(cosYawValues,'b-');
    hold on;
    plot(cos(framesLog.OrientationRads),'r--');
    plot(framesLog.OrientationMask,'g.');
    plot(isnanMask*0.5,'k.');
    
   [metric sum(isnan(cosYawValues)) ]
   % pause
end

end

clf;
plot(orientationMatchMetric,'b.')
%pause
[val,index] = max(orientationMatchMetric);

offset = offsetValues(index);


%clf;
%polar(framesLog.OrientationAngle*pi/180,framesLog.TimeMS*factor,'b-');
%hold on
%polar(attLog.Yaw*pi/180, attLog.TimeMSMean*factor,'r-');

%offset1 = (2.472-1.045)+(3.056-3.019)

% Punt d'inici on comen�ar a buscar el offset ideal
%offset2 = (gpsEndUpTimeMS - framesEndUpTimeMS) * factor

%(offset1-offset2)/factor

SaveLogDataSynchronizedToFrames(baseDir,baseName, offset, framesLog, gpsLog, attLog, ntunLog);

plot((framesLog.TimeMS+offset)*factor, framesLog.OrientationAngle, 'b.');
hold on
plot(attLog.TimeMSMean*factor, attLog.Yaw, 'r.');

%pause

if 0
    clf;
    subplot 331
    plot(gpsLog.T*factor,gpsLog.RelAlt,'b-');
    title('RelAlt');
    subplot 332
    plot(gpsLog.T*factor,gpsLog.GCrs,'b-');
    %plot(gpsLog.T*factor,gpsLog.Spd,'b-');
    %title('Spd');
    title('GCrs');
    subplot 333
    %plot(gpsLog.T*factor,gpsLog.GCrs,'b-');
    polar(gpsLog.GCrs*pi/180,gpsLog.T*factor,'b-');
    title('GCrs');
    subplot 334
    plot(attLog.TimeMSMean*factor,attLog.Roll,'b-');
    title('Roll');
    subplot 335
    plot(attLog.TimeMSMean*factor,attLog.Yaw,'b-');
    hold on
    plot(gpsLog.T*factor,gpsLog.GCrs,'r-');
    title('Yaw');
    %plot(gpsLog.T*factor,gpsLog.VZ,'b-');
    %title('Vz');
    subplot 336
    polar(attLog.Yaw*pi/180, attLog.TimeMSMean*factor);
    title('Yawr');
    %plot(attLog.TimeMSMean*factor,attLog.Pitch,'b-');
    %title('Pitch');
    subplot 337
    plot(ctunLog.TimeMSMean*factor,ctunLog.BarAlt,'b-');
    title('BarAlt');
    subplot 338
    %plot(ntunLog.TimeMSMean*factor,ntunLog.VelX,'b-');
    %title('VelX');
    plot(imuLog.TimeMS*factor,imuLog.AccZ,'b-');
    title('AccZ');
    subplot 339
    %plot(ntunLog.TimeMSMean,ntunLog.VelY,'b-');
    %title('VelY');
    plot(evLog.TimeMSMean*factor,evLog.Id,'b.');
    title('Ev');

end

%legend('RelAlt','Spd','GCrs','Roll','Yaw','BarAlt','VelX','VelY');
end


% =========================================================================


% Funci� pensada per intentar establir la relaci� temporal entre missatges
% IMU i GPS. Despr�s he vist que hi ha un camp no documentat a GPS, que
% indica el temps del missatge GPS usant el mateix sistema de refer�ncia
% que IMU.


function [dataLog] = EstimateTimeMS(dataLog, gpsImuLog)
    
    dataLog.TimeMSMin = [];
    dataLog.TimeMSMax = [];
    
    for i=1:size(dataLog.Index,1)
        
        seqNum = dataLog.Index(i);
        
        nearestUp = find(gpsImuLog.Index>seqNum,1);
        
        if ~isempty(nearestUp)
            dataLog.TimeMSMax(i,1) =  gpsImuLog.TimeMS(nearestUp);
            if(nearestUp>1)
                dataLog.TimeMSMin(i,1) =  gpsImuLog.TimeMS(nearestUp-1);
            else
                dataLog.TimeMSMin(i,1) =  0;
            end        
        else
            dataLog.TimeMSMin(i,1) = max(gpsImuLog.TimeMS);
            dataLog.TimeMSMax(i,1) = dataLog.TimeMSMin(i);
        end
        
    end
    
    dataLog.TimeMSMean = (dataLog.TimeMSMin+dataLog.TimeMSMax)/2;
end


function SynchroIMUGPS

% Reading from a tsv file
gpsFile = 'Log20140516-1342 - GPS.tsv';
imuFile = 'Log20140516-1342 - IMU.tsv';
gpsLog = ReadLogTSVData(gpsFile)

imuLog = ReadLogTSVData(imuFile);
unique(imuLog.TimeMS(2:end)-imuLog.TimeMS(1:(end-1)))

clf;
plot(gpsLog.T, gpsLog.RelAlt,'b-');
hold on
% plot(gpsLog.T, gpsLog.Alt,'r-');
plot(gpsLog.T, gpsLog.Spd,'g-');
plot(gpsLog.T, gpsLog.VZ,'c-');
plot(imuLog.TimeMS, imuLog.AccZ,'y-');
legend('RelAlt','Spd','VZ','AccZ');
%pause

gap = [];
indexGap = [];   %Registers the seq.num. of the gpsLog starting gpsPeriod
firstInnerImuIndex = []; % Registers the seq.num. of the first imu msg inside gpsPeriod
lastInnerImuIndex = [];
for i=1:(size(gpsLog.TimeMS,1)-1)
  indexL = gpsLog.Index(i);
  indexR = gpsLog.Index(i+1);
  gpsPeriod = gpsLog.TimeMS(i+1)-gpsLog.TimeMS(i);
  
  innerImus = find((imuLog.Index>indexL)&(imuLog.Index<indexR));
  
  if (~isempty(innerImus) && (gpsPeriod>0))
  
      imuSpan = imuLog.TimeMS(innerImus(end))-imuLog.TimeMS(innerImus(1));
           
      gap = [gap; gpsPeriod-imuSpan];
      indexGap = [indexGap indexL];
      firstInnerImuIndex = [firstInnerImuIndex innerImus(1)];
      lastInnerImuIndex = [lastInnerImuIndex innerImus(end)];
  else
      
      gap = [gap; 1];
      indexGap = [indexGap indexL];
      firstInnerImuIndex = [firstInnerImuIndex -1];
      lastInnerImuIndex = [lastInnerImuIndex -1];
  end
end

plot(gap)

[minVal,indexMin] = min(gap)

'Aix� hauria de ser coincident (casi) '
[ gpsLog.T(indexMin) imuLog.TimeMS(firstInnerImuIndex(indexMin)) ]
[ gpsLog.T(indexMin+1) imuLog.TimeMS(lastInnerImuIndex(indexMin)) ]

indexL = gpsLog.Index(indexMin)
indexR = gpsLog.Index(indexMin+1)

innerImus = find((imuLog.Index>indexL)&(imuLog.Index<indexR));

imuLog.TimeMS(innerImus)



gpsTimeMS = gpsLog.TimeMS(indexMin);
imuTimeMS = imuLog.TimeMS(firstInnerImuIndex(indexMin));

% imu TimeMS is shifted, in order to match gpsTimeMS when log messages
% temporarily match (theoretically exactly, or with a temporal
% missalignment between 0 and minVal.

shiftTimeMS = gpsTimeMS - imuTimeMS;
imuLog.TimeMS = imuLog.TimeMS + shiftTimeMS;

clf;
plot(gpsLog.TimeMS, gpsLog.VZ,'c-');
hold on
plot(imuLog.TimeMS, imuLog.AccZ,'r-');
plot(imuLog.TimeMS, imuLog.AccX,'g-');
end


function [data ] = ReadLogTSVData(fileName)
% Conversi� simbol ',' per '.', en l'expressi� de decimals
wholeFile = fileread(fileName);

% Cal eliminar els punts de milers, milions, etc. Sino es pren com l'inici
% dels decimals en matlab.
%newFileData = strrep(wholeFile,'.','');
newFileData = strrep(wholeFile,',','.');

fid = fopen(fileName,'w');
fprintf(fid,'%s',newFileData);
fclose(fid);

data = tdfread(fileName,'\t');
end

function [data] = AssignFramesOrientationAngle(data)

north = 360;
south = 180;
east = 90;
west= 270;

data.OrientationMask = (data.Orientation=='N') | (data.Orientation=='S')...
                     | (data.Orientation=='E')| (data.Orientation=='W');

                
data.OrientationAngle = zeros(size(data.Orientation));

data.OrientationAngle(data.Orientation=='N') = north;
data.OrientationAngle(data.Orientation=='S') = south;
data.OrientationAngle(data.Orientation=='E') = east;
data.OrientationAngle(data.Orientation=='W') = west;

%clf;
%plot(data.OrientationAngle,'g-');
%pause

end



function SaveLogDataSynchronizedToFrames(baseDir, baseName, offset, framesLog, gpsLog, attLog, ntunLog)

  % Data to be associated to each frame is first detailed in a cell array
  
  % GPS msg log data
  nRows = size(framesLog.TimeMS,1);
  nCols = 1;
  
  timeMS = round(interp1(gpsLog.T, gpsLog.TimeMS, framesLog.TimeMS+offset));
  lat = interp1(gpsLog.T, gpsLog.Lat, framesLog.TimeMS+offset);
  lng = interp1(gpsLog.T, gpsLog.Lng, framesLog.TimeMS+offset);
  hDop = interp1(gpsLog.T, gpsLog.HDop, framesLog.TimeMS+offset);
  nSats = interp1(gpsLog.T, gpsLog.NSats, framesLog.TimeMS+offset, 'nearest');
  alt = interp1(gpsLog.T, gpsLog.Alt, framesLog.TimeMS+offset);
  relAlt =  interp1(gpsLog.T, gpsLog.RelAlt, framesLog.TimeMS+offset);
  spd = interp1(gpsLog.T, gpsLog.Spd, framesLog.TimeMS+offset);
  gcrs = interp1(gpsLog.T, gpsLog.GCrs, framesLog.TimeMS+offset);

  % ATT msg log data
  
  cosRoll = interp1(attLog.TimeMSMean, cos(attLog.Roll*pi/180), framesLog.TimeMS+offset);
  sinRoll = interp1(attLog.TimeMSMean, sin(attLog.Roll*pi/180), framesLog.TimeMS+offset);
  cosYaw = interp1(attLog.TimeMSMean, cos(attLog.Yaw*pi/180), framesLog.TimeMS+offset);
  sinYaw = interp1(attLog.TimeMSMean, sin(attLog.Yaw*pi/180), framesLog.TimeMS+offset);
  cosPitch = interp1(attLog.TimeMSMean, cos(attLog.Pitch*pi/180), framesLog.TimeMS+offset);
  sinPitch = interp1(attLog.TimeMSMean, sin(attLog.Pitch*pi/180), framesLog.TimeMS+offset);
  
  % NTUN msg log data
  velX = interp1(ntunLog.TimeMSMean, ntunLog.VelX, framesLog.TimeMS+offset);
  velY = interp1(ntunLog.TimeMSMean, ntunLog.VelY, framesLog.TimeMS+offset);
  
  frameData = [mat2cell(framesLog.FrameName, ones(size(framesLog.FrameName,1),1), size(framesLog.FrameName,2))];
 
  frameData = [ frameData mat2cell(timeMS, ones(1,nRows), nCols)];
  frameData = [ frameData mat2cell(lat, ones(1,nRows), nCols) ];
  frameData = [ frameData mat2cell(lng, ones(1,nRows), nCols) ];
  frameData = [ frameData mat2cell(hDop, ones(1,nRows), nCols) ];
  frameData = [ frameData mat2cell(nSats, ones(1,nRows), nCols) ];
  frameData = [ frameData mat2cell(alt, ones(1,nRows), nCols) ];
  frameData = [ frameData mat2cell(relAlt, ones(1,nRows), nCols) ];
  frameData = [ frameData mat2cell(spd, ones(1,nRows), nCols) ];
  frameData = [ frameData mat2cell(gcrs, ones(1,nRows), nCols) ];
   
  frameData = [ frameData mat2cell(cosRoll, ones(1,nRows), nCols) ];
  frameData = [ frameData mat2cell(sinRoll, ones(1,nRows), nCols) ];
  frameData = [ frameData mat2cell(cosYaw, ones(1,nRows), nCols) ];
  frameData = [ frameData mat2cell(sinYaw, ones(1,nRows), nCols) ];
  frameData = [ frameData mat2cell(cosPitch, ones(1,nRows), nCols) ];
  frameData = [ frameData mat2cell(sinPitch, ones(1,nRows), nCols) ];

  frameData = [ frameData mat2cell(velX, ones(1,nRows), nCols) ];
  frameData = [ frameData mat2cell(velY, ones(1,nRows), nCols) ];

  header = {'FrameName', 'TimeMS', 'Lat', 'Lng', 'HDop', 'NSats', 'Alt', ...
             'RelAlt', 'Spd', 'GCrs', 'CosRoll', 'SinRoll', 'CosYaw', 'SinYaw',...
             'CosPitch', 'SinPitch', 'VelX', 'VelY'};
  
  % which is then saved to disk as a .tsv file.
  fileName = [baseDir baseName 'SynchronizedData.tsv'];
  
  format long
  dlmcell(fileName,[header; frameData]);
  format short
  
  % Saving the 3D coords of the cam location associated to each frame, in
  % KML format
  fileName = [baseDir baseName 'SynchronizedData.kml'];
  
%  kmlwrite(fileName, lat, lng, alt);

  % Writting all the information associated to each frame
 % (By means of attributes)

 mRows = size(timeMS,1);
 
 Geometry = repmat({'Point'},1,nRows);
 [s(1:nRows,1).Geometry] = Geometry{:};

 tmp = num2cell(timeMS);
 [s(1:nRows,1).TimeMS] = tmp{:};
  
 tmp = num2cell(lat);
 [s(1:nRows,1).Lat] = tmp{:};
 
 tmp = num2cell(lng);
 [s(1:nRows,1).Lon] = tmp{:};
 
 tmp = num2cell(hDop);
 [s(1:nRows,1).Hdop] = tmp{:};
 tmp = num2cell(nSats);
 [s(1:nRows,1).NSats] = tmp{:};
 tmp = num2cell(alt);
 [s(1:nRows,1).Altitude] = tmp{:};
 tmp = num2cell(relAlt);
 [s(1:nRows,1).RelAlt] = tmp{:};
 tmp = num2cell(spd);
 [s(1:nRows,1).Spd] = tmp{:};
 tmp = num2cell(gcrs);
 [s(1:nRows,1).Gcrs] = tmp{:};
 tmp = num2cell(cosRoll);
 [s(1:nRows,1).CosRoll] = tmp{:};
 tmp = num2cell(sinRoll);
 [s(1:nRows,1).SinRoll] = tmp{:};
 tmp = num2cell(cosYaw);
 [s(1:nRows,1).CosYaw] = tmp{:};
 tmp = num2cell(sinYaw);
 [s(1:nRows,1).SinYaw] = tmp{:};
 tmp = num2cell(cosPitch);
 [s(1:nRows,1).CosPitch] = tmp{:};
 tmp = num2cell(sinPitch);
 [s(1:nRows,1).SinPitch] = tmp{:}; 
 tmp = num2cell(velX);
 [s(1:nRows,1).VelX] = tmp{:};
 tmp = num2cell(velY);
 [s(1:nRows,1).VelY] = tmp{:};

 % Construct an attribute specification
 attribspec = makeattribspec(s);
 
 % Register the likely camera pose, currently stated from the UAV pose
 
 % Some attributes are required to be finite. So NaN values are changed to
 % -1 or another value, depending on the attribute concep
 
 distortedCameraInfo = isnan(relAlt) | isnan(cosYaw) | isnan(cosPitch) | isnan(cosRoll);
 
 relAlt(isnan(relAlt)) = -1;
 cosYaw(isnan(cosYaw)) = -1;
 cosPitch(isnan(cosPitch)) = -1;
 cosRoll(isnan(cosRoll)) = -1;
 
 cameraInfo = geopoint(lat,lng,'Altitude',relAlt,...
                       'Heading',acosd(cosYaw),...
                       'Tilt',acosd(cosPitch),...
                       'Roll',acosd(cosRoll),...
                       'AltitudeMode','relativeToGround')

 kmlwrite(fileName, s, 'Description', attribspec, 'Camera', cameraInfo);


end



