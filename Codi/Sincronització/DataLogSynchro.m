function fileNameLog = DataLogSynchro(logSensorPath, logFeaturePath)
    %Sincronitza les dades del sensor (especificat en logSensorPath) i les
    %dades creades per un feature matching (especificat per logFeaturePath).
    %fileNameLog cont� la ruta del fitxer amb totes les dades sincronitzades
    if ~exist('logSensorPath','var')
        currentpath = cd('..');
        cd('..');
        parentpath = pwd();
        cd(currentpath);
        baseDir = [parentpath '\201405161455\UAVLog\Raw\'];
        baseName = '2014-05-16 14-56-51';
        logSensorPath = [baseDir baseName '.log'];
    else
        baseName = strsplit(logSensorPath,'\');
        baseName = strsplit(baseName{end},'.');
        baseName = baseName{1};
    end
    if ~exist('logFeaturePath','var')
            logFeaturePath = 'Fitxers generats\trajectoriaFAST.txt';
    end

    format long;
    try
        load(['Fitxers generats\' baseName ' logStruct.mat']);
    catch
        mkdir('Fitxers generats');
        fprintf('Llegint dades de %s...\n', logSensorPath);
        log = ReadLogFile(logSensorPath, baseName);
    end


    gpsImuLog = log.gpsImu;

    gpsLog = log.gps;
%     imuLog = log.imu;
%     ctunLog = log.ctun;
    ntunLog = log.ntun;
    attLog = log.att;
%     evLog = log.ev;

    %Trajectoria GPS
%     GenerarTrajectoriaLog(gpsLog);
%     pause;

%     ctunLog = EstimateTimeMS(ctunLog, gpsImuLog);
    ntunLog = EstimateTimeMS(ntunLog, gpsImuLog);
    attLog = EstimateTimeMS(attLog, gpsImuLog);
%     evLog = EstimateTimeMS(evLog, gpsImuLog);

    framesLog = tdfread(logFeaturePath,'\t'); %Txt amb la traject�ria del descriptor

    %Calcular l'offset perqu� el temps de les dades del sensor estigui
    %emparellat amb el temps de la traject�ria del descriptor

    offset = -SincronitzarSensorDescriptor(attLog, framesLog); 
    %Restem l'offset perqu� el SaveLogDataSynchronized considera el despla�ament 
    %al rev�s (trajectoria automatica mappejada a la del sensor)
    
    %L'offset t� dos camps: offset(1) �s respecte el temps (el que busquem)
    %                       offset(2) �s respecte els graus d'orientaci�

    %L'offset de l'anotaci� manual �s de 88071.
    %L'obtinguda autom�ticament �s de 88056.
    fileNameLog = SaveLogDataSynchronizedToFrames(baseName, offset(1), framesLog, gpsLog, attLog, ntunLog);

end


% =========================================================================

% Llegeix el log del RAW, creant totes les variables que cont� el fitxer.
function [log] = ReadLogFile(fileName, baseName)
    %format long;
    %S'assumeix que les 5 primeres l�nies s�n irrellevants.
    skipLines = 5;
    %I que el codi de la cap�alera est� a la posici� 4.
    posNomEstructura = 4;
    nomBase = 'log';
    %Aqu� es guardar� els noms de les cap�aleres per despr�s poder poder
    %indexar els noms i omplir les estructures amb els valors del log
    llistaCapcaleres = cell(0);
    fID = fopen(fileName, 'r');
    log = struct();
    
    %Comencem creant l'estructura GPS-IMU, ja que �s una barreja de dos
    %estructures, i per tant s'ha de crear manualment
    eval(sprintf('%s.gpsImu = struct();', nomBase));
    %I les seves cap�aleres les introduim manualment tamb�
    eval(sprintf('%s.gpsImu.Index = [];', nomBase));
    eval(sprintf('%s.gpsImu.Code = char;', nomBase));
    eval(sprintf('%s.gpsImu.TimeMS = [];', nomBase));
    
    %%Llegir les cap�aleres. Con�ixer quines variables estan en el log, i
    %quins camps tenen aquestes variables.
    tline = fgetl(fID);
    comptLinies = 1;
    fiCapcalera = false;
    while ischar(tline) && not(fiCapcalera)
        if comptLinies > skipLines %Saltar les l�nies que no aporten informaci�
            cellStr = strrep(tline,' ','');
            cellStr = strsplit(cellStr,',');
            if strcmp(cellStr{1}, 'FMT')
                sz = size(llistaCapcaleres,1);
                %Creaci� de l'estructura (per ex, gps)
                nomEstructura = lower(cellStr{posNomEstructura});
                llistaCapcaleres{sz+1,1} = nomEstructura;
                eval(sprintf('%s.%s = struct();', nomBase, nomEstructura));
                %Camp extra (index)
                eval(sprintf('%s.%s.Index = [];', nomBase, nomEstructura));
                %Resta de camps
                k=2;
                for j=(posNomEstructura+2):size(cellStr,2)
                    %Inicialitzaci� dels camps de cada estructura
                    %Per ex, gpsLog.TimeMS = []
                    eval(sprintf('%s.%s.%s = [];', nomBase, nomEstructura, cellStr{j}));
                    llistaCapcaleres{sz+1,k} = cellStr{j};
                    k=k+1;
                end
                
            else
                fiCapcalera = true;
            end
        end
        tline = fgetl(fID);
        comptLinies=comptLinies+1;
    end
    
    %%Omplir totes les estructures amb les dades del log
    
    %Despla�ament de les linies irrellevants
    cellStr = strrep(tline,' ','');
    cellStr = strsplit(cellStr,',');
    while ischar(tline) && (strcmp(cellStr{1},'PARM') || strcmp(cellStr{1},'MODE') || strcmp(cellStr{1},'MSG'))
        tline = fgetl(fID);
        comptLinies=comptLinies+1;
        cellStr = strrep(tline,' ','');
        cellStr = strsplit(cellStr,',');
    end
    
    %Per cada linia llegida del log, omplim el camp d'una estructura
    while ischar(tline)
       cellStr = strrep(tline,' ','');
       cellStr = strsplit(cellStr,','); 
       %Cal eliminar els punts de milers, milions, etc. Sino es pren 
       %com l'inici dels decimals en matlab.
       cellStr = strrep(cellStr,',','.');
       nomEstructura = lower(cellStr{1});
       %Busquem les cap�aleres de l'estructura trobada
       row = strcmpi(nomEstructura, llistaCapcaleres(:,1));
       capcaleres = llistaCapcaleres(row,:);
       capcaleres = capcaleres(~cellfun('isempty',capcaleres));  
       for i=2:size(capcaleres,2)
           if strcmp(nomEstructura, 'gps') && (strcmp(capcaleres{i}, 'Lat') || strcmp(capcaleres{i}, 'Lng'))
               tipus_dada = '%.7f;'; %Volem la m�xima presici� en la posici� del GPS.
           else
               tipus_dada = '%d;';
           end
           eval(sprintf(['%s.%s.%s(end+1, 1) = ' tipus_dada], nomBase, nomEstructura, capcaleres{i}, str2double(cellStr{i})));
       end
       eval(sprintf('%s.%s.Index(end+1, 1) = %d;', nomBase, nomEstructura, comptLinies));
       %Si es tracta de GPS o IMU s'ha d'omplir l'estructura GPS-IMU
       if strcmp(nomEstructura, 'gps') || strcmp(nomEstructura, 'imu')
           eval(sprintf('%s.gpsImu.Index(end+1, 1) = %d;', nomBase, comptLinies));
           eval(sprintf('%s.gpsImu.Code(end+1, 1:3) = ''%s'';', nomBase, upper(nomEstructura)));
           if strcmp(nomEstructura, 'gps')
               row = 14;
           else
               row = 2;
           end
           eval(sprintf('%s.gpsImu.TimeMS(end+1, 1) = %d;', nomBase, str2double(cellStr(row))));
       end
       tline = fgetl(fID);
       comptLinies=comptLinies+1;
    end
    fclose(fID);
    save(['Fitxers generats\' baseName ' logStruct.mat'], 'log');
    %format short;
end

%Mostra la trajectoria que realitza el dron segons les dades del GPS
function GenerarTrajectoriaLog(gpsLog)
    % Latitud = Y. Longitud = X.
    
    format long
    offsetX = (gpsLog.Lng(2:end)-gpsLog.Lng(1:end-1));%x2-x1
    offsetY = (gpsLog.Lat(2:end)-gpsLog.Lat(1:end-1));%y2-y1
    
    sum(1) = offsetX(1);
    sum(2) = offsetY(1);
    figure(1);hold on; axis equal;
    title('Coordenades GPS', 'FontWeight', 'Bold');
    ylabel('Latitud'); xlabel('Longitud');
    for i=2:size(offsetX,1)
        aux = sum;
        sum(1) = sum(1) + offsetX(i);
        sum(2) = sum(2) + offsetY(i);
        %plot(dades.offsetX(1:i), dades.offsetY(1:i));
        plot([aux(1) sum(1)], [aux(2) sum(2)]);

        %pause;
    end

end

% Sincronitza les dades del sensor amb les del descriptor. �s a dir, retorna
% l'offset de difer�ncia perqu� els dos estiguin en el mateix sistema de
% refer�ncia en el temps. Aix� es fa trobant punts representatius (canvis
% grans en l'orientaci� del dron) i emparellant-los.

function offset = SincronitzarSensorDescriptor(attLog, framesLog)
    
    orientSensor = -attLog.Yaw; %S'ha d'invertir una de les orientacions
    orientDescr = framesLog.orientacio;
    
    %%Preprocessar traject�ries eliminant canvis sobtats de 0 a 360� i viceversa.
    %Nota: si haguessim de tenir en compte quan els punts representatius
    %augmenten de grau o disminueixen, aquest m�tode podria portar
    %problemes. Per ex: orientSensor "puja" de 0� a 180�, i orientDescr
    %"baixa" perqu� passa de 0� a 181� (>180�), i per tant passa a ser de
    %0� a -179�. De moment no �s el cas, simplement es busquen canvis grans de rotaci�.    
    orientDescr = EliminarCanvisSobtats(orientDescr);
    orientSensor = EliminarCanvisSobtats(orientSensor);

    %%Marcar punts representatius (peaks, punts que en poc temps vari�n molt)
    puntsDescr = TrobarPuntsRepresentatius(framesLog.timeMS,orientDescr);
    puntsSensor = TrobarPuntsRepresentatius(attLog.TimeMSMean,orientSensor);
    
    %%Mappejar els punts del descriptor amb els del sensor (programaci� din�mica)
    [puntsDescrSynch, puntsSensorSynch, offset] = EmparellarPunts(puntsDescr, puntsSensor);
    %L'offset donat s'aproxima a: pDescrSynch = pSensorSynch + offset
    
    %%(DEBUG) Comparem resultats RANSAC VS Dynamic programming
    if 0
        format long g;
        [tform, inlierPuntsDescr, inlierPuntsSensor, status] = ...
                        estimateGeometricTransform(puntsDescr, puntsSensor, 'similarity', ...
                        'MaxNumTrials', 10000, 'Confidence', 99.99999, 'MaxDistance', 1.5);
        offset2 = -tform.T(3,1:2);
        disp('Quant m�s pr�xims al 0, millor');
        RANSAC = mean(puntsDescr-puntsSensor-repmat(offset2,size(puntsSensorSynch,1),1));
        [puntsDescr2, puntsSensor2, offset] = EmparellarPunts(puntsDescrSynch, puntsSensorSynch);
        PD = mean(puntsDescr2-puntsSensor2-repmat(offset,size(puntsSensor2,1),1));
        disp('RANSAC: ');
        disp(RANSAC);
        disp('Dynamic programming: ');
        disp(PD);
    end
    
    %%(DEBUG) Mostrar les dues traject�ries sincrontizades
    if 1
       figure; hold on;
       title('Traject�ries d''orientaci� sincronitzades', 'FontWeight','Bold');
       xlabel('Temps (ms)'); ylabel('Orientaci� (graus)');
       %Traject�ria sensor
       plot(attLog.TimeMSMean+offset(:,1),orientSensor+offset(:,2),'r');
       %Punts representatius sensor
       plot(puntsSensorSynch(:,1)+offset(:,1), puntsSensorSynch(:,2)+offset(:,2), 'go');
       %Traject�ria descriptor
       plot(framesLog.timeMS, orientDescr,'b');
       %Punts representatius sensor
       plot(puntsDescrSynch(:,1), puntsDescrSynch(:,2), 'ko');
       legend('Traject�ria sensor', 'Punts representatius sensor', 'Traject�ria FAST', 'Punts representatius FAST');
       drawnow;
    end
   
end

% Donada una traject�ria, troba aquells punts on es produeixen canvis m�s
% grans. Aix� ho fa a partir de trobar els 'peaks' de la 1a i 2a derivada
% de la traject�ria.
% Format de 'punts': [temps grausOrientacio]

function punts = TrobarPuntsRepresentatius(t, y)
    %Codi basat en: http://www.mathworks.com/matlabcentral/answers/37103-finding-turning-points-of-a-dataset
    ySmooth = smooth(y,20); %Suavitzem la traject�ria per eliminar soroll
    dt = mean(t(2:end)-t(1:end-1)); %Increment aprox. de les dades de t
    %C�lcul de derivades: En l'espai de derivades podem diferenciar 
    %aquells 'peaks' m�s representatius a partir de la seva al�ada (kmax).
    yp = nan(size(ySmooth));
    ypp = nan(size(ySmooth));
    yp(2:end-1) = (ySmooth(3:end) - ySmooth(1:end-2))/(2*dt);
    ypp(2:end-1) = (ySmooth(3:end) + ySmooth(1:end-2) - 2*ySmooth(2:end-1)) / (dt^2);
    k = abs(ypp) ./ (1 + yp.^2).^1.5;
    
    %Per tal d'eliminar tots els 'peaks' petits apliquem una aproximaci�
    %que consisteix en nom�s quedar-nos amb aquells peaks que siguin, com a
    %m�nim, la meitat d'alts que el peak m�s elevat.
    minPeakHeight = max(k)/2;
    minPeakDistance = 50;
    %Potser no s'haurien d'ordenar per altitud del 'peak', el que importa
    %�s correlacionar els punts segons el temps, i si "desordenes" segons el valor m�xim l'ordre ho perds.
    %El m�xim del sensor no t� perqu� correspondre's amb el m�xim del descriptor.
    [pks, locs] = findpeaks(k, 'MinPeakHeight', minPeakHeight, 'MinPeakDistance',minPeakDistance);
    punts = [t(locs) y(locs)];
    if 0
        figure; hold on; plot(t, y); plot(punts(:,1), punts(:,2), 'ro');
        xlabel('Temps (ms)'); ylabel('Orientaci� (graus)');
        text(t(locs)+50,y(locs)-15,num2str((1:numel(pks))')); hold off;
    end
end

%Elimina canvis sobtats en els graus, ja que de 0� a 359� hi ha 1� de
%dist�ncia, i no 359�.

function x = EliminarCanvisSobtats(x)
     for i=1:size(x,1)-1
           if abs(x(i)-x(i+1)) > 180 %En una esfera de 360� la m�xima dist�ncia entre 2 punts s�n 180�
               %Sempre has de modificar i+1 per no tenir problemes 
               if x(i) < x(i+1) %Cas 0� a 359�
                   x(i+1) = x(i+1)-360; %0� a -1�  
               else %Cas 360� a 1�
                   x(i+1) = x(i+1)+360; %360� a 361�
               end
           end
    end
end

% Funci� pensada per intentar establir la relaci� temporal entre missatges
% IMU i GPS. Despr�s he vist que hi ha un camp no documentat a GPS, que
% indica el temps del missatge GPS usant el mateix sistema de refer�ncia
% que IMU.


function [dataLog] = EstimateTimeMS(dataLog, gpsImuLog)
    
    dataLog.TimeMSMin = [];
    dataLog.TimeMSMax = [];
    
    for i=1:size(dataLog.Index,1)
        
        seqNum = dataLog.Index(i);
        
        nearestUp = find(gpsImuLog.Index>seqNum,1);
        
        if ~isempty(nearestUp)
            dataLog.TimeMSMax(i,1) =  gpsImuLog.TimeMS(nearestUp);
            if(nearestUp>1)
                dataLog.TimeMSMin(i,1) =  gpsImuLog.TimeMS(nearestUp-1);
            else
                dataLog.TimeMSMin(i,1) =  0;
            end        
        else
            dataLog.TimeMSMin(i,1) = max(gpsImuLog.TimeMS);
            dataLog.TimeMSMax(i,1) = dataLog.TimeMSMin(i);
        end
        
    end
    
    dataLog.TimeMSMean = (dataLog.TimeMSMin+dataLog.TimeMSMax)/2;
end

function fileNameLog = SaveLogDataSynchronizedToFrames(baseName, offset, framesLog, gpsLog, attLog, ntunLog)

  % Data to be associated to each frame is first detailed in a cell array
  
  % GPS msg log data
  nRows = size(framesLog.timeMS,1);
  nCols = 1;
  
  timeMS = round(interp1(gpsLog.T, gpsLog.TimeMS, framesLog.timeMS+offset));
  lat = interp1(gpsLog.T, gpsLog.Lat, framesLog.timeMS+offset);
  lng = interp1(gpsLog.T, gpsLog.Lng, framesLog.timeMS+offset);
  hDop = interp1(gpsLog.T, gpsLog.HDop, framesLog.timeMS+offset);
  nSats = interp1(gpsLog.T, gpsLog.NSats, framesLog.timeMS+offset, 'nearest');
  alt = interp1(gpsLog.T, gpsLog.Alt, framesLog.timeMS+offset);
  relAlt =  interp1(gpsLog.T, gpsLog.RelAlt, framesLog.timeMS+offset);
  spd = interp1(gpsLog.T, gpsLog.Spd, framesLog.timeMS+offset);
  gcrs = interp1(gpsLog.T, gpsLog.GCrs, framesLog.timeMS+offset);

  % ATT msg log data
  
  cosRoll = interp1(attLog.TimeMSMean, cos(attLog.Roll*pi/180), framesLog.timeMS+offset);
  sinRoll = interp1(attLog.TimeMSMean, sin(attLog.Roll*pi/180), framesLog.timeMS+offset);
  cosYaw = interp1(attLog.TimeMSMean, cos(attLog.Yaw*pi/180), framesLog.timeMS+offset);
  sinYaw = interp1(attLog.TimeMSMean, sin(attLog.Yaw*pi/180), framesLog.timeMS+offset);
  cosPitch = interp1(attLog.TimeMSMean, cos(attLog.Pitch*pi/180), framesLog.timeMS+offset);
  sinPitch = interp1(attLog.TimeMSMean, sin(attLog.Pitch*pi/180), framesLog.timeMS+offset);
  yaw = interp1(attLog.TimeMSMean, EliminarCanvisSobtats(attLog.Yaw), framesLog.timeMS+offset);
  pitch = interp1(attLog.TimeMSMean, EliminarCanvisSobtats(attLog.Pitch), framesLog.timeMS+offset);
  roll = interp1(attLog.TimeMSMean, EliminarCanvisSobtats(attLog.Roll), framesLog.timeMS+offset);
  % NTUN msg log data
  velX = interp1(ntunLog.TimeMSMean, ntunLog.VelX, framesLog.timeMS+offset);
  velY = interp1(ntunLog.TimeMSMean, ntunLog.VelY, framesLog.timeMS+offset);
  
  frameData = mat2cell(framesLog.FrameName, ones(size(framesLog.FrameName,1),1), size(framesLog.FrameName,2));
 
  frameData = [ frameData mat2cell(timeMS, ones(1,nRows), nCols)];
  frameData = [ frameData mat2cell(lat, ones(1,nRows), nCols) ];
  frameData = [ frameData mat2cell(lng, ones(1,nRows), nCols) ];
  frameData = [ frameData mat2cell(hDop, ones(1,nRows), nCols) ];
  frameData = [ frameData mat2cell(nSats, ones(1,nRows), nCols) ];
  frameData = [ frameData mat2cell(alt, ones(1,nRows), nCols) ];
  frameData = [ frameData mat2cell(relAlt, ones(1,nRows), nCols) ];
  frameData = [ frameData mat2cell(spd, ones(1,nRows), nCols) ];
  frameData = [ frameData mat2cell(gcrs, ones(1,nRows), nCols) ];
   
  frameData = [ frameData mat2cell(roll, ones(1,nRows), nCols) ];
  frameData = [ frameData mat2cell(cosRoll, ones(1,nRows), nCols) ];
  frameData = [ frameData mat2cell(sinRoll, ones(1,nRows), nCols) ];
  frameData = [ frameData mat2cell(yaw, ones(1,nRows), nCols) ];
  frameData = [ frameData mat2cell(cosYaw, ones(1,nRows), nCols) ];
  frameData = [ frameData mat2cell(sinYaw, ones(1,nRows), nCols) ];
  frameData = [ frameData mat2cell(pitch, ones(1,nRows), nCols) ];
  frameData = [ frameData mat2cell(cosPitch, ones(1,nRows), nCols) ];
  frameData = [ frameData mat2cell(sinPitch, ones(1,nRows), nCols) ];

  frameData = [ frameData mat2cell(velX, ones(1,nRows), nCols) ];
  frameData = [ frameData mat2cell(velY, ones(1,nRows), nCols) ];

  header = {'FrameName', 'TimeMS', 'Lat', 'Lng', 'HDop', 'NSats', 'Alt', ...
             'RelAlt', 'Spd', 'GCrs', 'Roll','CosRoll', 'SinRoll', 'Yaw', 'CosYaw', 'SinYaw',...
             'Pitch', 'CosPitch', 'SinPitch', 'VelX', 'VelY'};
  
  % which is then saved to disk as a .tsv file.
  %fileName = [baseDir baseName 'SynchronizedData.tsv'];
  fileNameLog = ['Fitxers generats\' baseName ' SynchronizedData.tsv'];
  format long
  dlmcell(fileNameLog,[header; frameData]);
  fprintf('Fitxer amb les dades sincronitzades guardat a %s\n', fileNameLog);
  format short
  
  % Saving the 3D coords of the cam location associated to each frame, in
  % KML format
  %fileName = [baseDir baseName 'SynchronizedData.kml'];
  fileNameKML = ['Fitxers generats\' baseName ' SynchronizedData.kml'];
  fprintf('Generant kml...\n');
%  kmlwrite(fileName, lat, lng, alt);

  % Writting all the information associated to each frame
 % (By means of attributes)

 mRows = size(timeMS,1);
 
 Geometry = repmat({'Point'},1,nRows);
 [s(1:nRows,1).Geometry] = Geometry{:};

 tmp = num2cell(timeMS);
 [s(1:nRows,1).TimeMS] = tmp{:};
  
 tmp = num2cell(lat);
 [s(1:nRows,1).Lat] = tmp{:};
 
 tmp = num2cell(lng);
 [s(1:nRows,1).Lon] = tmp{:};
 
 tmp = num2cell(hDop);
 [s(1:nRows,1).Hdop] = tmp{:};
 tmp = num2cell(nSats);
 [s(1:nRows,1).NSats] = tmp{:};
 tmp = num2cell(alt);
 [s(1:nRows,1).Altitude] = tmp{:};
 tmp = num2cell(relAlt);
 [s(1:nRows,1).RelAlt] = tmp{:};
 tmp = num2cell(spd);
 [s(1:nRows,1).Spd] = tmp{:};
 tmp = num2cell(gcrs);
 [s(1:nRows,1).Gcrs] = tmp{:};
 tmp = num2cell(roll);
 [s(1:nRows,1).roll] = tmp{:};
 tmp = num2cell(cosRoll);
 [s(1:nRows,1).CosRoll] = tmp{:};
 tmp = num2cell(sinRoll);
 [s(1:nRows,1).SinRoll] = tmp{:};
 tmp = num2cell(yaw);
 [s(1:nRows,1).yaw] = tmp{:};
 tmp = num2cell(cosYaw);
 [s(1:nRows,1).CosYaw] = tmp{:};
 tmp = num2cell(sinYaw);
 [s(1:nRows,1).SinYaw] = tmp{:};
 tmp = num2cell(pitch);
 [s(1:nRows,1).pitch] = tmp{:};
 tmp = num2cell(cosPitch);
 [s(1:nRows,1).CosPitch] = tmp{:};
 tmp = num2cell(sinPitch);
 [s(1:nRows,1).SinPitch] = tmp{:}; 
 tmp = num2cell(velX);
 [s(1:nRows,1).VelX] = tmp{:};
 tmp = num2cell(velY);
 [s(1:nRows,1).VelY] = tmp{:};

 % Construct an attribute specification
 attribspec = makeattribspec(s);
 
 % Register the likely camera pose, currently stated from the UAV pose
 
 % Some attributes are required to be finite. So NaN values are changed to
 % -1 or another value, depending on the attribute concep
 
 distortedCameraInfo = isnan(relAlt) | isnan(cosYaw) | isnan(cosPitch) | isnan(cosRoll);
 
 relAlt(isnan(relAlt)) = -1;
 cosYaw(isnan(cosYaw)) = -1;
 cosPitch(isnan(cosPitch)) = -1;
 cosRoll(isnan(cosRoll)) = -1;
 
 cameraInfo = geopoint(lat,lng,'Altitude',relAlt,...
                       'Heading',acosd(cosYaw),...
                       'Tilt',acosd(cosPitch),...
                       'Roll',acosd(cosRoll),...
                       'AltitudeMode','relativeToGround');

 kmlwrite(fileNameKML, s, 'Description', attribspec, 'Camera', cameraInfo);
  fprintf('Kml guardat a %s\n', fileNameKML);
end



