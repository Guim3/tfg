function rutaFitxerSincronitzat = mainSincronitzacio(framesPath, logSensorPath, tipusDeteccio, despFrames)
    close all;
    %framesPath: directori on es troben tots els frames de la seq��ncia de vol
    %logSensorPath: ruta al fitxer que cont� les dades dels sensors
    %tipusDeteccio: tipus de detector de caracter�stiques a utilitzar
    %despFrames: dist�ncia entre frames quan es realitzi el feature matching
    if ~exist('framesPath','var')                     
        framesPath = 'D:\User\Documentos\Guim\Treballs\4t Carrera\Treball Fi de Grau\SequenciesPropies\201405161455\';  %201405161414    201405161455
    end
    if ~exist('logSensorPath','var')
        %logSensorPath = 'UAVLog\Raw\2014-05-16 14-14-08.log'; %25m
        logSensorPath = 'UAVLog\Raw\2014-05-16 14-56-51.log'; %50m
    end
    if ~exist('tipusDeteccio','var')                     
        tipusDeteccio = 'FAST';
    end
    if ~exist('tipusDeteccio','var')                     
        despFrames = 1;
    end
    
    logFeaturePath = FeatureMatching(framesPath, tipusDeteccio, despFrames);

    rutaFitxerSincronitzat = DataLogSynchro([framesPath logSensorPath], logFeaturePath);
end