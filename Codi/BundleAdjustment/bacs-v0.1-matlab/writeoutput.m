function writeoutput( ld, Xd, Md, Sdd, vr, w_huber, dataset )
%WRITEOUTPUT  writes bacs output in results folder.
%
%   WRITEOUTPUT( ld, Xd, Md, Sdd, vr, w_huber, dataset )
%
%   writes the output of BACS ld, Xd, Md, Sdd, vr and w_huber in ascii
%   files. Ascii files are stored in a subfolder of results named according
%   to the string dataSet. Take a look at the help of BACS to enquire about
%   the conventions of the output variables.
%
%   Examples:
%     WRITEOUTPUT( ld, Xd, Md, Sdd, vr, w_huber, dataset )
%
%     writes e.g. with dataSet = 'test' all input variables as ascii files
%     into the folder 'results/test' of the current directory.
%
%   See also BACS, READINPUT.
%
%   Copyright 2012 Johannes Schneider, Department of Photogrammetry, IGG,
%   University of Bonn (johannes.schneider@uni-bonn.de)
%

% do it into the results folder
dataset = fullfile( 'results', dataset );

% check on existance of directory
if ~isdir( dataset )
    mkdir( dataset );
end

% estimated camera rays
dlmwrite( fullfile( dataset, 'rays.dat' ), ld', 'precision', '%.6f' );

% estimated scene points
dlmwrite( fullfile( dataset, 'points.dat' ), Xd', 'precision', '%.6f' );

% estimated motion matrices
if size( Md, 1 ) < size( Md, 2 ); Md = Md'; end
dlmwrite( fullfile( dataset, 'motions.dat' ), cell2mat( Md ), 'precision', '%.6f' );

% covariance matrix of estimated parameters
dlmwrite( fullfile( dataset, 'motioncovariance.dat' ), Sdd, 'precision', '%.12f' );

% estimated corrections of the camera rays
dlmwrite( fullfile( dataset, 'corrections.dat' ), vr', 'precision', '%.12f' );

% wights on the inverse covariance matrix
dlmwrite( fullfile( dataset, 'wights.dat' ), w_huber, 'precision', '%.6f' );

