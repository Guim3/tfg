function [l, Sll, ict, Xa, Ma, P] = readinput( dataset ) 
%READINPUT  reads input for bacs from the data folder.
%
%   [l, Sll, ict, Xa, Ma, P] = READINPUT( dataSet )
%
%   reads l, Sll, ict, Xa, Ma, P from the ascii files in the data folder
%   specified by the string dataset for the use in BACS.
%
%   See also BACS, WRITEOUTPUT.
%
%   Copyright 2012 Johannes Schneider, Department of Photogrammetry, IGG,
%   University of Bonn (johannes.schneider@uni-bonn.de)
%

% dataset has to lie in the input folder
dataset = fullfile( 'input', dataset );

% rays
l = dlmread( fullfile( dataset, 'rays.dat' ) )';
N = size( l, 2 );

% covariances
sll = dlmread( fullfile( dataset, 'covariances.dat' ) );
Sll = arrayfun(@(n) diag( sll(n,1:3) ) ...
    + diag( sll(n,4:5),1 ) + diag( sll(n,4:5),-1 ) ...
    + diag( sll(n,6),2 ) + diag( sll(n,6),-2 ), 1 : N, 'UniformOutput', 0 );

% linkage
ict = dlmread( fullfile( dataset, 'linkage.dat' ) );

% scene points
Xa = dlmread( fullfile( dataset, 'points.dat' ) )';
% I = size( Xa, 2 );

% motion matrices
Ma = dlmread( fullfile( dataset, 'motions.dat' ) );
T = size( Ma, 1 ) / 4;
Ma = mat2cell( Ma, repmat( 4, T, 1 ), 4 );

% projections
P = dlmread( fullfile( dataset, 'projections.dat' ) );
J = size( P, 1 ) / 3;
P = mat2cell( P, repmat( 3, J, 1 ), 4 );



