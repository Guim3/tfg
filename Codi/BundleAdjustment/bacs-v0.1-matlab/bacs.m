function [la, Xa, Ma, Sdd, s0dsq, vr, w, iter] = ...
    bacs( l, ijt, Sll, Shh, Xa, Ma, P, varargin )
%BACS performs a bundle adjustment for multi-view cameras based on
% corresponding camera rays.
%
% On the supposition that the deviations of the observed camera rays are
% normally distributed and mutually independent, the estimated orientation
% parameters and object point coordinates correspond to the maximum-
% likelihood estimation.
% Line preserving cameras with known inner calibration as well as their
% orientation within the multi-camera system (MCS) are assumed to be known.
% For initialization sufficiently accurate approximate values for scene
% point coordinates and orientations of the MCS (translation and rotation)
% for different instances of time are needed.
%
% DIMS:
%        N  number of observed camera rays      (n = 1,...,N)
%        I  number of object points             (i = 1,...,I)
%        C  number of cameras within the MCS    (c = 1,...,C)
%        T  number of instances in time         (t = 1,...,T)
%           
% INPUT:
%        l  camera rays [3xN]
%      ict  observation linkage [Nx3]
%               * contains in n-th row [i,c,t] 
%      Sll  covariance matrices of each camera ray l(:,n) {N}[3x3]
%      Shh  covariance matrix of centroid constraints [7x7]
%               * translation (3), rotation (3), scale (1)
%       Xa  approximate values for homogeneous object points [4xI]
%       Ma  approximate values for MCS transformations {T}[4x4]
%        P  projection matrices of the single-view cams {C}[3x4]
%               * projects form the mcs into the C camera systems
%
% OPTIONAL:
%      eps  convergence criterion: max(abs(dl)) / sigma_l < eps
%               * (default: eps = 1e-6)
%  maxIter  maximum number of iterations
%               * (default: maxIter = 10)
%      tau  scale factor for levenberg-marquardt initialization
%               * (default:  tau = 0  -> no levenberg-marquardt)
% nearRatio only quantil*I scene points are used for datum definition
%               * (default:  quantil = 1  -> uses all scene points)
%        k  threshold for rewighting according to Huber
%               * (default:  k_huber = Inf  -> no rewighting)
%
% OUTPUT:
%       la  estimated camera rays [3xN]
%       Xa  estimated object point coordinates (homogeneous) [4xN]
%       Ma  estimated motion matrices {T}[4x4] (from object system to MCS)
%      Sdd  estimated covariance matrix of oriantation parameters [6Tx6T]
%    s0dsq  estimated variance factor
%       vr  estimated corrections on l in tangent space
%        w  estimated weights on diagonal elements of Sll
%     iter  number of iterations
%
%   Examples:
%     [la, Xa, Ma, Sdd, s0dsq, vr, w, iter] = ...
%        BACS(l, ijt, Sll, Shh, Xa, Ma, P )
%
%     is equivalent to calling BACS with default values
%
%     [la, Xa, Ma, Sdd, s0dsq, vr, w, iter] = ...
%        BACS(l, ijt, Sll, Shh, Xa, Ma, P, 'eps', '1e-6', 'maxIter', 10,
%        'nearRatio', 1, 'k', Inf, 'tau', 0 )
%
%   Copyright 2012 Johannes Schneider and Falko Schindler,
%   Department of Photogrammetry, IGG, University of Bonn
%
%   Contact person:
%     Johannes Schneider (johannes.schneider@uni-bonn.de)
%
%   Status: 24-05-2012
%


% parse parameters
if numel(varargin) == 1, params = varargin{1};
else params = parseparams(varargin{:}); end


% define helper functions
skew = @(x) [0, -x(3), x(2); x(3), 0, -x(1); -x(2), x(1), 0];
normS = @(x) x ./ sqrt(ones(size(x, 1)) * x.^2);
normS_jacobian = @(x) (eye(size(x, 1)) - x * x' / (x' * x)) / norm(x);
Rdr = @(dr) (eye(3) + skew(dr)) / (eye(3) - skew(dr));


% preprocess input for adjustment
Ma = cellfun(@(Ma) Ma^-1, Ma, 'UniformOutput', 0);
N = size(l, 2);
I = size(Xa, 2);
J = numel(P);
T = numel(Ma);
d = size(Shh, 2);
r = 2 * N - 3 * I - 6 * T + d;
if r < 0, error('Not enough constraints (redundancy = %d', r); end
qlsls = zeros( 3, 3, N );
for n = 1 : N
    qlsls( :, :, n ) = normS_jacobian(l(:, n)) * Sll{n} * normS_jacobian(l(:, n))';
end
l = normS(l);
Xa = normS(Xa);
la = normS(cell2mat(arrayfun(@(n) ...
    P{ijt(n, 2)} * Ma{ijt(n, 3)} * Xa(:, ijt(n, 1)), ...
    1 : N, 'UniformOutput', 0)));
w = ones( N, 1 );
W = sparse( 1 : 2 * N, 1 : 2 * N, 1 );
beta = - 2 * params.k * normpdf( params.k ) - 1 + ...
         2 * normcdf( params.k ) * ( 1 - params.k^2 ) + 2 * params.k^2;
nu = 2;


% precompute indices for C, D and Q matrices
C_rows = 2 * ones(6, 1) * (0 : N - 1) + [1; 2; 1; 2; 1; 2] * ones(1, N);
C_cols = [1; 1; 2; 2; 3; 3] * ones(1, N) + 3 * ones(6, 1) * (ijt(:, 1)' - 1);
D_rows = 2 * ones(12, 1) * (0 : N - 1) + [1; 2; 1; 2; 1; 2; 1; 2; 1; 2; 1; 2] * ones(1, N);
D_cols = [1; 1; 2; 2; 3; 3; 4; 4; 5; 5; 6; 6] * ones(1, N) + 6 * ones(12, 1) * (ijt(:, 3)' - 1);
Q_rows = 2 * ones(4, 1) * (0 : N - 1) + [1; 2; 1; 2] * ones(1, N);
Q_cols = [1; 1; 2; 2] * ones(1, N) + 2 * ones(4, 1) * (0 : N - 1);


% iterative estimation process
fprintf('... Starting BACS ...\nI=%d | C=%d | T=%d | N=%d\neps=%.0e | maxIter=%d | tau=%.0e | nearRatio=%.0f%% | k=%.1f\n',...
    I, J, T, N, params.eps, params.maxIter, params.tau, params.nearRatio*100, params.k )
for iter = 1 : params.maxIter
    
    % nullspaces of object points
    nullXa = cell2mat(arrayfun(@(i) null(Xa(:, i)'), permute(1 : I, [1 3 2]), 'UniformOutput', 0));
    
    % Jacobians C and D, covariance matrix Qrr and reduced observations lr
    C = zeros(2, 3, N);
    D = zeros(2, 6, N);
    Qrr = zeros(2, 2, N);
    lr = zeros(2, N);
    for n = 1 : N
        i = ijt(n, 1);
        j = ijt(n, 2);
        t = ijt(n, 3);
        nullLa = null(la(:, n)');
        tmp = nullLa' * normS_jacobian(P{j} * Ma{t} * Xa(:, i)) * P{j};
        C(:, :, n) = tmp * Ma{t} * nullXa(:, :, i);
        D(:, :, n) = tmp * [-skew(Ma{t}(1 : 3, :) * Xa(:, i)), Xa(4, i) * eye(3); zeros(1, 6)];
        Qrr(:, :, n) = nullLa' * qlsls(:,:,n) * nullLa;
        lr(:, n) = nullLa' * l(:, n);
    end
    C = sparse(C_rows, C_cols, C(:));
    D = sparse(D_rows, D_cols, D(:));
    invQrr = [Qrr(2, 2, :), -Qrr(1, 2, :); -Qrr(1, 2, :), Qrr(1, 1, :)] ...
        ./ repmat(Qrr(1, 1, :) .* Qrr(2, 2, :) - Qrr(1, 2, :).^2, 2, 2);
    invQrr = sparse( Q_rows, Q_cols, invQrr(:)) * W;
    
    % linear centroid constraints for datum definition
    nb_fix = round( params.nearRatio * I );
    [dmy, idx_fix] = sort( Xa( end, : ), 'descend' );
    idx_fix = sort( idx_fix( 1 : nb_fix ) );
    Xa_fix = Xa( :, idx_fix );
    H_fix = cell2mat(arrayfun(@(i) ...
        nullXa(:,:,idx_fix(i))' * ...
        ( 1/Xa_fix(4,i)^2 .* [Xa_fix(4,i).*eye(3), -Xa_fix(1:3,i)]' ) * ...
        [eye(3), -skew(Xa_fix(1:3,i)/Xa_fix(4,i)), Xa_fix(1:3,i)/Xa_fix(4,i)], ...
        1 : nb_fix, 'UniformOutput', 0)');
    H = zeros( 3 * I, d );
    H( reshape([ 3*idx_fix-2; 3*idx_fix-1; 3*idx_fix],3*nb_fix,1), : ) = H_fix;
    Nside = [H; zeros(6 * T, d)];
    
    % parameter and observation updates
    A = [C, D];
    Nmat = [A' * invQrr * A, Nside; Nside', -Shh];
    nvec = [A' * invQrr * lr(:); zeros(d, 1)];
    
    % Levenberg-Marquardt (see LOURAKIS und ARGYROS (2004) SBA: A Software
    %    Package for Generic Sparse Bundle Adjustment, page 7)
    if iter == 1
        mu = params.tau * sqrt( max( diag(Nmat) ) );
    end
    dx = (Nmat + mu*speye(size(Nmat))) \ nvec;
    
    % parameter update
    dk = dx(1 : 3 * I);
    Xa = cell2mat(arrayfun(@(i) ...
        normS(Xa(:, i) + nullXa(:, :, i) * dk(3 * i - 2 : 3 * i)), ...
        1 : I, 'UniformOutput', 0));
    dd = dx(3 * I + 1 : end - d);
    Ma = arrayfun(@(t) [ ... 
        Rdr(dd(6 * (t - 1) + (1 : 3)) / 2), dd(6 * (t - 1) + (4 : 6));
        0 0 0 1] * Ma{t}, 1 : T, 'UniformOutput', 0);
    
    % observation update
    dl = A * dx(1 : end - d);
    
    % approximate observations for next iteration
    la = normS(cell2mat(arrayfun(@(n) ...
        P{ijt(n, 2)} * Ma{ijt(n, 3)} * Xa(:, ijt(n, 1)), ...
        1 : N, 'UniformOutput', 0)));
    
    % corrections non-linear
    vr = cell2mat(arrayfun(@(n) ...
         - null( la(:, n)' )' * l(:, n), ...
         1 : N, 'UniformOutput', 0));
    
    % check for convergence
    if max(abs(dl) .* sqrt(diag(invQrr))) <= params.eps
        fprintf('Iter: %d // mu = %g // convCrit: %1.0e // %.2f%% rewighted\n', iter,full(mu),full(max(abs(dl) .* sqrt(diag(invQrr)))),sum(w<1)/N*100)
        if mu == 0
            break;
        else
            mu = 0;
            fprintf('setting mu := 0\n');
        end
    else
        fprintf('Iter: %d // mu = %g // convCrit: %1.0e // %.2f%% rewighted\n', iter,full(mu),full(max(abs(dl) .* sqrt(diag(invQrr)))),sum(w<1)/N*100)
        % Levenberg-Marquardt (see LOURAKIS und ARGYROS (2004) SBA: A Software
        %    Package for Generic Sparse Bundle Adjustment, page 7)
        phi = ( lr(:)'*invQrr*lr(:) - vr(:)'*invQrr*vr(:) ) / ...
              ( [dk;dd]' * ( mu * [dk;dd] + A'*invQrr*lr(:) ) );
        if phi > 0
            mu = mu * max( 1/3, 1 - ( 2*phi - 1 )^3 );
            nu = 2;
        else
            mu = mu * nu;
            nu = 2 * nu;
        end
    end
    
    % rewighting
    % (see Koch (1997): Parameterschaetzung und Hypothesentests in
    % linearen Modellen, Section 383, Page 281)
    % robust variance factor
    diag_Qrr = reshape( [Qrr(1, 1, :), Qrr(2, 2, :)], [], 1 );
    res = abs( vr(:) ) ./ sqrt( diag_Qrr );
    res( res > params.k ) = params.k;
    s0 = sqrt( res'*res / (r*beta) ); % s0 = sqrt( vr(:)' * invQrr * vr(:) / r );
    % pointwise rewighting according to corrections in tangent space
    w = ones( N, 1 );
    res = abs( vr(:) ) ./ sqrt( diag_Qrr );
    res = sqrt( sum( reshape( res, 2, [] ) ) )';
    btk = res > params.k * s0;
    if sum( btk ) > 0;
        w(btk) = s0 * params.k * w(btk) ./ res(btk); %maybe res^2
        W = sparse( 1:2*N, 1:2*N, reshape( [w,w]', [], 1 ) );
    end
    
end

% correct direction of homogeneus scene points at the horizon
Xa( end, Xa(end, : ) < 0 ) = - Xa( end, Xa(end, : ) < 0 );

% invert for motion matrix from world to mcs
Ma = cellfun(@(Ma) Ma^-1, Ma, 'UniformOutput', 0);


% empirical covariance matrix
% (Photogrammetric Computer Vision, Volume I: Geometry and Orientation,
% Part II - Orientation and Reconstruction, 26.12.2010, page 50, app. B.3,
% Alg. 1: Iterative parameter estimation in non-linear Gauss-Helmert model)
if r > 0
    s0dsq = vr(:)' * invQrr * vr(:) / r;
    if r < 30
        f = 1;
    else
        f = s0dsq;
    end
else
    s0dsq = NaN;
end
Sdd = f * ( Nmat(3*I+1:end,3*I+1:end) - Nmat(3*I+1:end,1:3*I) * ( Nmat(1:3*I,1:3*I) \ Nmat(1:3*I,3*I+1:end) ) )^-1; 
Sdd = Sdd( 1 : 6 * T, 1 : 6 * T );




function params = parseparams( varargin )

% default parameters
params.tau        = 0;
params.k          = Inf;
params.nearRatio  = 1;
params.maxIter    = 10;
params.eps        = 1e-6;

% modifications via optional parameters
for i = 1 : 2 : numel(varargin) - 1
    if ismember(varargin{i}, fieldnames(params))
        params.(varargin{i}) = varargin{i + 1};
    else
        error('Unknown parameter ''%s''.', varargin{i});
    end
end
