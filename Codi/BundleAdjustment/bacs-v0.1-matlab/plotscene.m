function plotscene( X, M, P, varargin)

%% parse parameters
if numel(varargin) == 1, params = varargin;
else params = plotsceneparams( M, varargin{:}); end


%% helper function
hom2euk = @(x) x(1 : end - 1, :) ./ x(end * ones(1, end - 1), :);


%% plot settings
hold on; grid on; box on; axis equal; view(3);
xlabel('x-axis'); ylabel('y-axis'); zlabel('z-axis')
axis( [params.xm(1) params.xm(1) params.xm(2) params.xm(2) params.xm(3) params.xm(3)] ...
    + params.sidelength * [-1 1 -1 1 -1 1] );


%% scene points
% convert to Euclidean coordinates
X( end, : ) = X( end, : ) + eps;
X = hom2euk( X );
I = size( X, 2 );

% plot all points
plot3( X( 1,: ), X( 2,: ), X( 3,: ), '.', 'Color', params.colorX );

% find points in and out of plotbox
inbox = all( abs( X - repmat( params.xm, 1, I ) ) < params.sidelength );

% plot points out of box at the border of the box
X = X( :, ~inbox ) ./ repmat( max( abs( X( :, ~inbox ) ) ), 3, 1) .* params.sidelength;
plot3( X( 1,: ), X( 2,: ), X( 3,: ), '*', 'Color', params.colorX );
plot3( X( 1,: ), X( 2,: ), X( 3,: ), 'o', 'Color', params.colorXinfty );


%% motion
T = length( M );
J = length( P );
Tt = zeros( 3, T );

for t = 1 : T
    
    Tt( :, t ) = M{t}( 1:3, 4 );
    Rt = M{t}( 1:3, 1:3 )';
    quiver3(Tt(1,t),Tt(2,t),Tt(3,t),Rt(1,1),Rt(1,2),Rt(1,3),params.scaleM,'Color','red','LineWidth',2);
    quiver3(Tt(1,t),Tt(2,t),Tt(3,t),Rt(2,1),Rt(2,2),Rt(2,3),params.scaleM,'Color','green','LineWidth',2);
    quiver3(Tt(1,t),Tt(2,t),Tt(3,t),Rt(3,1),Rt(3,2),Rt(3,3),params.scaleM,'Color','blue','LineWidth',2);
    
    for j = 1 : J
        Mj = M{t} * [P{j}(:,1:3)', -P{j}(:,1:3)'*P{j}(:,4); 0 0 0 1];
        Tj = Mj( 1:3, 4 );
        Rj = Mj( 1:3, 1:3 )';
        quiver3(Tj(1),Tj(2),Tj(3),Rj(1,1),Rj(1,2),Rj(1,3),params.scaleM/2,'Color','red','LineWidth',2);
        quiver3(Tj(1),Tj(2),Tj(3),Rj(2,1),Rj(2,2),Rj(2,3),params.scaleM/2,'Color','green','LineWidth',2);
        quiver3(Tj(1),Tj(2),Tj(3),Rj(3,1),Rj(3,2),Rj(3,3),params.scaleM/2,'Color','blue','LineWidth',2);
    end 
end

% plot3( Tt(1,:), Tt(2,:), Tt(3,:), 'o--', 'Color', params.colorM );




function params = plotsceneparams( M, varargin )

% calculations
Mts = cell2mat(arrayfun(@(t) M{t}(1:3,4), 1:numel(M), 'UniformOutput', 0));
box = [ min( Mts, [], 2 )' ; max( Mts, [], 2 )' ];

% default parameters
params.colorX      = 'black';
params.colorXinfty = 'red';
params.colorM      = 'blue';
params.scaleM      = median( sqrt( sum( diff(Mts,1,2).^2 ) ) ) / 2;
params.xm          = mean( box )';
params.sidelength  = max( diff( box ) );

% modifications via optional parameters
for i = 1 : 2 : numel(varargin) - 1
    if ismember(varargin{i}, fieldnames(params))
        params.(varargin{i}) = varargin{i + 1};
    else
        error('Unknown parameter ''%s''.', varargin{i});
    end
end















