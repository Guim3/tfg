%% Define Input
dataset = 'parkinglot_far';

%% Read Input Data Files
[l, Sll, ijt, Xa, Ma, P] = readinput( dataset );

%% Execute BACS
params.tau       = 0;
params.k         = Inf;
params.nearRatio = 0.5;
params.maxIter   = 10;
params.eps       = 1e-6;
Shh = diag( [zeros(1,6) , 1e4] );

[ld, Xd, Md, Sdd, s0dsq, vr, w, iter] = ...
    bacs(l, ijt, Sll, Shh, Xa, Ma, P, params );

%% Write Output Data Files
writeoutput( ld, Xd, Md, Sdd, vr, w, dataset );
