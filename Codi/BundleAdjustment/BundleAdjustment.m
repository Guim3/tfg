function tforms = BundleAdjustment(tforms, pointsFrame)
    %Importem la llibreria vincentToolbox on s'inclou el Sparse Bundle Adjustment
    %https://code.google.com/p/vincents-structure-from-motion-matlab-toolbox/
    %https://code.google.com/p/vincents-structure-from-motion-matlab-toolbox/wiki/gettingStarted
    %http://wiki.ros.org/sba/Tutorials/IntroductionToSBA
    %http://ceres-solver.org/nnls_tutorial.html#bundle-adjustment
    %http://grail.cs.washington.edu/projects/bal/
    load('tforms'); load('points'); clear('imgNames');
    tall = 10;
    tforms = tforms(1:tall); points = points(:,:,1:tall);
    pointsFrame = points; clear('points');
    addpath(genpath('vincentToolbox_3.1.1'));
    anim=Animation(); 
    
    %W: [ 2 x nPoint x nFrame ] matrix: the projected 2D points (x,y).
    anim.W = zeros(2, size(pointsFrame,2), size(tforms,2));
    anim.mask = not(isnan(reshape(pointsFrame(1,:,:), size(pointsFrame,2),size(pointsFrame,3))));
    %anim.mask = not(isnan(pointsFrame));
    %S: [ 3 x nPoint x nFrame ] matrix: the position of the 3D points (X,Y,Z).
    pointsFrame(isnan(pointsFrame)) = 0;
    anim.S = pointsFrame;
    
    P=zeros(3,3,anim.nFrame);
    for i=1:anim.nFrame
        tmp = tforms(i).T' * [anim.S(:,anim.mask(:,i),i); ones(1, sum(anim.mask(:,i)))];
        anim.W(:,anim.mask(:,i),i)=  tmp(1:2,:,:);
        P(:,:,i) = tforms(i).T';
        fprintf('%d/%d\n', i, anim.nFrame);
    end
    anim.P = P;
    
    err = anim.computeError();
    errTot = [ err(1), 0 ];

    [ anim ] = bundleAdjustment( anim );

    err = anim.computeError();
    errTot(2) = err(1);

    out =sprintf( 'Reprojection error %0.4f/%0.4f, before/after BA\n\n',...
                    errTot(1), errTot(2) );
    fprintf(out);
end