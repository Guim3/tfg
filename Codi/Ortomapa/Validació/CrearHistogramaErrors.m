function CrearHistogramaErrors()
    fields = {'Image based', 'Sensor based', ''};
    hist = [234036.71, 85399.82, 0];
    bar(hist);
    colormap summer;
    set(gca,'XTickLabel',fields);
    ylabel('Error final acumulat');
    title('Comparativa m�todes de creaci� d''orto mapes','FontWeight','Bold');

end