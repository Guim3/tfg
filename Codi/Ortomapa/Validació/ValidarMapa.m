function ValidarMapa()
    close all;
    %%Inicialitzar par�metres
    addpath('Arxius auxiliars');
    imgNames = [];
    tforms = [];
    GT = [];
    imageSz = [573 693 3];
    load('tformsSensorBasedFrameCentralMean'); %Carrega imgNames i tforms
    load('GTfiltrat'); %Carrega Ground Truth  
    
    %DEBUG
%     a = zeros(size(GT,2),1); a(1) = size(GT(1).pFrame,2);
%     for i=2:size(a,1)
%         a(i) = a(i-1) + size(GT(i).pFrame,2);
%     end
    %FI DEBUG
    
    %%Posem en correspond�ncia el primer frame amb el mapa de Google Maps
    %Per fer-ho necessitem saber la rotaci� i l'escala inicials (s'han de
    %treure emp�ricament)
    %thetaInit = 188.4; %Image based
    thetaInit = -0.6512; %Sensor based.
    scaleInit = 0.163; %Valors emp�rics
    tforms = ReescalarTforms(tforms, imageSz, thetaInit, scaleInit, GT(1).pMapa, GT(1).pFrame{2,1});
    
    %%Verificar que els par�metres correlacionen mapa i 1r frame
    ComprovarQueLesImatgesEncaixen(imread('Gr�fics/Mapa Sant Cugat.jpg'), imread('Gr�fics/MyVideo_14_57_56_781_064.jpg'), tforms(1));
    
    %%Calculem l'error acumulat per cada punt marcat en els frames 
    j = 1; k = 1;
    jAnterior = j;
    kAnterior = k;
    errorPerFrame = [];%zeros(1,size(imgNames,1)); %En aquest array es guardar� l'error per cada frame
    errorAcumulat = [0];%zeros(1,size(imgNames,1)); %En aquest array s'acumular� l'error
    %canviPunt = [];%zeros(1,size(imgNames,1));
    for i=2:size(imgNames,1)
       name = imgNames{i};
       %Donat el nom associat a una tform, s'ha de buscar aquest nom dins del GT.
       trobat = false;
       while j<=size(GT,2) && not(trobat)%Per cada punt marcat en el mapa
           
            while k <= size(GT(j).pFrame,2) && not(trobat) %Comparem els punts corresponent dels frames
                
                if strcmp(GT(j).pFrame{1,k}, name)
                    trobat = true;
                    pFrame = GT(j).pFrame{2,k};
                else
                    k = k + 1;
                end
            end
            if trobat
                pMapa = GT(j).pMapa;
                jAnterior = j;
                kAnterior = k;
                %canviPunt(end+1) = 0;
            else
                %canviPunt(end+1) = 1;
                j = j + 1;
                k=1;
            end
       end
       if not(trobat)
            %errorPerFrame('Error, no s''ha trobat el frame %s al ground truth', name);
            j = jAnterior;
            k = kAnterior;
       else
             %Transformem el punt del frame perqu� estigui en el mateix sistema
           %de refer�ncia que el mapa real.
           pFrameTransformed = [pFrame 1] * tforms(i).T;

           %Calculem l'error mitjan�ant la dist�ncia euclidiana.
           errorPerFrame(end+1) = abs(pdist([pMapa; pFrameTransformed(1:2)], 'euclidean'));
           errorAcumulat(end+1) = errorAcumulat(end) + errorPerFrame(end);
       
       end
     
    end
    figure(); plot(errorPerFrame); hold on;
    %[~,ind] = find(canviPunt);
    %hx = graph2d.constantline(ind, 'LineStyle',':', 'Color',[.7 .7 .7]);
    %changedependvar(hx,'x');
    title('Error per frame (sensor based)', 'FontWeight', 'Bold');
    xlabel('Frames'); ylabel('Dist�ncia euclidiana (p�xels)');
    
    figure(); plot(errorAcumulat); hold on;
    title('Error acumulat (sensor based)', 'FontWeight', 'Bold');
    xlabel('Frames'); ylabel('Dist�ncia euclidiana (p�xels)');
    fprintf('Error final acumulat: %.2f\n', errorAcumulat(end));
end


function tforms = ReescalarTforms(tforms, imageSize, thetaInit, scaleInit, pMapa, pFrame)
    %Reescala el valor de les tforms perqu� l'orto mapa creat sigui m�s
    %petit i per tant es computi m�s r�pid.
    
    T1 = eye(3); T1(3,1:2) = [-imageSize(2)/2 -imageSize(1)/2];
    T2 = eye(3); T2(3,1:2) = [imageSize(2)/2 imageSize(1)/2];
    
    Rinit = eye(3); 
    Rinit(1:2,1:2) = [cosd(thetaInit) sind(thetaInit); ...
                     -sind(thetaInit) cosd(thetaInit)];
    Sinit = eye(3); Sinit(1,1) = scaleInit; Sinit(2,2) = Sinit(1,1);
    tformInit = T1*Sinit*Rinit*T2;
    %Calcular la translaci� entre el punt del mapa
    %amb el del 1r frame perqu� tingui error 0.
    biasXY = CalcularTranslacio(tformInit, tforms(1).T, pMapa, pFrame);
    Tinit = eye(3); 
    Tinit(3,1:2) = biasXY(1:2);
    tformInit = tformInit*Tinit;
    %Propaguem el canvi a la resta de tforms
    for i=1:size(tforms,2)
       tforms(i).T = tforms(i).T*tformInit; 
    end
end

function [biasXY] = CalcularTranslacio(tformInit, tformFrame1, pMapa, pFrame)
    pFrameTransformed = [pFrame 1] * tformFrame1 * tformInit;
    biasXY = pMapa - pFrameTransformed(1:2);
end

function ComprovarQueLesImatgesEncaixen(imMap, imFrame, tform)
    %Mostra les dues imatges superposades per verificar que el 1r frame i
    %el mapa estan ben correlacionats. A partir d'aqu� es pot ajustar la
    %rotaci� o l'escalatge.
    mosaicView = imref2d([size(imMap,1) size(imMap,2)]);
    warpedImage = imwarp(imFrame, tform, 'OutputView',mosaicView);
    figure();
    %imshow(warpedImage);
    showMatchedFeatures(imMap, warpedImage, SURFPoints, SURFPoints);
end