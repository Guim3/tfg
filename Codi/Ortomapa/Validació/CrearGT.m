function GT = CrearGT(pathMap, sensorLog, pathSequence)
    %%Crear el Ground Truth anotant les correspond�ncies entre un mapa real
    %i una seq��ncia de frames. Donat un punt en el mapa real, se
    %selecciona aquest mateix punt en la seq��ncia de frames. Quan aquest
    %punt ja no estigui en els frames, es seleccionar� un punt nou en el
    %mapa.
    %%OUTPUT: 
    %          GT: array d'estructures on GT(i) cont� el pMapa (punt del
    %          mapa) i pFrames (cell de 2xN frames que es corresponen al punt pMapa.
    %          Aquesta cell t� per una banda el nom del frame pFrames{1,i}, i per
    %          l'altra el punt del frame corresponent a pMapa pFrames{2,i}

    close all;
    if ~exist('pathMap','var')
        pathMap = 'Mapa Sant Cugat.jpg';
    end
    if ~exist('sensorLog','var')
        sensorLog =  '2014-05-16 14-56-51 SynchronizedData.tsv';
    end
    if ~exist('pathSequence','var')
        currentpath = cd('..');
        cd('..');
        cd('..');
        parentpath = pwd();
        cd(currentpath);
        pathSequence = [parentpath '\201405161455\'];
    end
    %'Enter' = actualitzar punt en mapa sense guardar punt frame
    %'z' = guardar punt frame i passar frame
    %'r' = sobreescriure anterior punt (per equivocacions)
    
    %%Inicialitzar par�metres
    imMap = imread(pathMap);
    GT = struct();
    margesX = 14; 
    margesY = 3;
    %Llegir log sensor per obtenir els noms dels frames
    [~,~,frameNames] = tblread(sensorLog,'\t');
    frameNames = cellstr(frameNames);
    figure(1); imshow(imMap); title('Mapa real','FontWeight','Bold');
    actualitzar = true;
    try
        load('GTtmp');
        sum=0;
        for i=1:size(GT,2)
            sum = sum + size(GT(i).pFrame,2); 
        end
        i = sum;
    catch
        i=1;
    end
    while i<=size(frameNames,1)
        if actualitzar
            %%Selecci� punt mapa
            figure(1); hold on;
            fprintf('Fes zoom o mou a la zona d''inter�s i prem una tecla per continuar.\n');
            pause;
            fprintf('Selecciona punt d''inter�s en el mapa:\n');
            sz = size(GT,2);
            if i==1
                sz = 0;
            end
            GT(sz+1).pMapa = ginput(1);
            fprintf('X:%d Y:%d\n',GT(sz+1).pMapa(1),GT(sz+1).pMapa(2));
            GT(sz+1).pFrame = cell(2,0);
            delete(findobj(1,'Type','line','Marker','o'));  %Esborrem l'anterior plot
            plot(GT(sz+1).pMapa(1), GT(sz+1).pMapa(2), 'o');
            hold off;
        end
        actualitzar = false;
        %%Selecci� punt frame
        imFrame = LlegirFrame([pathSequence frameNames{i}], margesX, margesY);
        figure(1);
        figure(2); imshow(imFrame); title(sprintf('Frame %d/%d',i,size(frameNames,1)), 'FontWeight', 'Bold');
        hold on;
        fprintf('(%d/%d) Selecciona el punt corresponent al punt del mapa\n',i,size(frameNames,1));
        tmpP = ginput(1);
        if ~isempty(tmpP)
            plot(tmpP(1), tmpP(2), 'o'); hold off;
            tmpP(2) = tmpP(2)*2;
        end
        if isempty(tmpP)
            actualitzar = true;
        else
            str = input('String buit = seg�ent frame. A= actualitzar punt mapa. R=tornar a seleccionar punt frame.\n', 's');
            switch lower(str)
                case 'a'
                    actualitzar= true; %Actualitzar punt mapa               
                case 'r'
                    %Repetir iteraci�
                otherwise 
                    %Guardar punt i continuar
                    sz = size(GT(end).pFrame,2);
                    GT(end).pFrame{1,sz+1} = frameNames{i};
                    GT(end).pFrame{2,sz+1} = tmpP;
                    i = i+1;
            end
        end
        save('GTtmp', 'GT');
    end
end

function frame = LlegirFrame(path, margesX, margesY)
    frame=im2double(imread(path));
    %Els frames contenen marges negres que no interessen.
    frame = frame(margesY:end-1, margesX:end-margesX, :); %En el cas de la Y nom�s t� un marge superior, no inferior
    %Ens quedem nom�s amb les linies senars
    frame = frame(1:2:end,:,:);
 
end