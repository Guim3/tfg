%Link de refer�ncia: http://es.mathworks.com/help/vision/examples/feature-based-panoramic-image-stitching.html

function OrtomapaFeatureMatching(baseDir, tipusDeteccio)

    if ~exist('baseDir','var')
        currentpath = cd('..');
        cd('..');
        cd('..');
        parentpath = pwd();
        cd(currentpath);

        baseDir = [parentpath '\201405161455\'];
    end
    
    if ~exist('tipusDeteccio','var')
        tipusDeteccio = 'FAST';
    end
    warning('off','images:initSize:adjustingMag');
    margesX = 14; 
    margesY = 3;
    %Extraiem les matrius de transformaci� que mappejaran totes les imatges
    %relatives a la primera imatge. Tamb� retorna el nom de les imatges
    %que formaran part de l'orto mapa, �s a dir, que no tenen soroll.
    [tforms, imgNames] = FeatureMatchingTform(baseDir, tipusDeteccio);
    save('tformsEscalatSensor', 'tforms', 'imgNames');
    %load('tformsEscalatSensor');
    %Recalculem les tforms perqu� les imatges siguin relatives a la imatge
    %central, ja que aix� no es propaga tant l'efecte de distorsi�
    %Aix� s'aconsegueix invertint la tform de la imatge central i aplicant
    %aquesta transformaci� a totes les altres
    li=dir([baseDir '*.png']);
    sampleImage = imread([baseDir li(1).name]);
    sampleImage = sampleImage(margesY:end-1, margesX:end-margesX, :);
    imageSize = size(sampleImage);
    [tforms, indCentralImage] = RecalcularTformsRespecteImatgeCentral(tforms, imageSize);

    %Reescalem les tforms perqu� l'orto mapa es crei m�s r�pid
    thetaInit = 188.4; scaleInit = 0.163; %Valors emp�rics
    tforms = ReescalarTforms(tforms, imageSize, thetaInit, scaleInit);
    
    %Inicialitzaci� del mosaic
    [mosaic, xLimits, yLimits] = InicialitzarMosaic(tforms, sampleImage, imageSize);
    
    %Creaci� del mosaic
    mosaic = CreacioMosaic(tforms, mosaic, xLimits, yLimits, baseDir, imgNames, margesX, margesY);
    
    figure();
    imshow(mosaic);
    imwrite(mosaic, 'mapa.jpg');
end

function tforms = ReescalarTforms(tforms, imageSize, thetaInit, scaleInit)
    %Reescala el valor de les tforms perqu� l'orto mapa creat sigui m�s
    %petit i per tant es computi m�s r�pid.
    
    T1 = eye(3); T1(3,1:2) = [-imageSize(2)/2 -imageSize(1)/2];
    T2 = eye(3); T2(3,1:2) = [imageSize(2)/2 imageSize(1)/2];
    
    Rinit = eye(3); 
    Rinit(1:2,1:2) = [cosd(thetaInit) sind(thetaInit); ...
                     -sind(thetaInit) cosd(thetaInit)];
    Sinit = eye(3); Sinit(1,1) = scaleInit; Sinit(2,2) = Sinit(1,1);
    tformInit = T1*Sinit*Rinit*T2;
    
    %Propaguem el canvi a la resta de tforms
    for i=1:size(tforms,2)
       tforms(i).T = tforms(i).T*tformInit; 
    end
end

function mosaic = CreacioMosaic(tforms, mosaic, xLimits, yLimits, baseDir, imgNames, margesX, margesY)
    %Use imwarp to map images into the mosaic and 
    %use vision.AlphaBlender to overlay the images together.
    blender = vision.AlphaBlender('Operation', 'Binary mask', ...
    'MaskSource', 'Input port');
    
    % Create a 2-D spatial reference object defining the size of the mosaic.
    %An imref2d object encapsulates the relationship between the intrinsic 
    %coordinates anchored to the rows and columns of a 2-D image and the 
    %spatial location of the same row and column locations in a world coordinate system
    mosaicView = imref2d([size(mosaic,1) size(mosaic,2)], xLimits, yLimits);
    idWarning = 'MATLAB:nearlySingularMatrix';
    warning('off',idWarning);
    % Create the mosaic.
    for i = 1:3:size(imgNames,1)

        I = imread([baseDir imgNames{i}]);
        I = I(margesY:end-1, margesX:end-margesX, :);
        %I = I(1:2:end,:,:);
        
        % Transform I into the mosaic.
        warpedImage = imwarp(I, tforms(i), 'OutputView', mosaicView);

        % Create a mask for the overlay operation.
%         warpedMask = imwarp(ones(size(I(:,:,1))), tforms(i), 'OutputView', mosaicView);

        % Clean up edge artifacts in the mask and convert to a binary image.
%         warpedMask = warpedMask >= 1;
        warpedMask = rgb2gray(warpedImage) > 0.5;
        % Overlay the warpedImage onto the mosaic.
        mosaic = step(blender, mosaic, warpedImage, warpedMask);
        fprintf('%d/%d\n', i, size(imgNames,1));
%          figure(1); imshow(I);
%         figure(2); imshow(warpedImage);
         imshow(mosaic);
          drawnow;
%          pause;
    end
    warning('on',idWarning);
end

function [mosaic, xLimits, yLimits] = InicialitzarMosaic(tforms, I, imageSize)
    xlim = zeros(numel(tforms),2);
    ylim = zeros(numel(tforms),2);
    for i = 1:numel(tforms)
        [xlim(i,:), ylim(i,:)] = outputLimits(tforms(i), [1 imageSize(2)], [1 imageSize(1)]);
    end

    % Find the minimum and maximum output limits
    xMin = min(xlim(:));
    xMax = max(xlim(:));
    yMin = min(ylim(:));
    yMax = max(ylim(:));
    
    xLimits = [xMin xMax];
    yLimits = [yMin yMax];

    % Width and height of mosaic.
    width  = round(xMax - xMin);
    height = round(yMax - yMin);
    if width > 6000 || height > 6000
        err = MException('ValueError:ExceededSize', ...
        'La mida del mosaic �s massa gran com perqu� pugui ser processat eficientment.'); 
       throw(err);
    end
    
    % Initialize the "empty" mosaic.
    mosaic = zeros([height width size(I,3)], 'like', I);
end

function [tforms, indCentralImage] = RecalcularTformsRespecteImatgeCentral(tforms, imageSize)
    %% Compute the output limits  for each transform
    % Com que volem trobar la imatge central, primer mirem la mida de tot
    % el "mapa". Aleshores, com que sabrem el centre del mapa, podem trobar
    % la imatge central.
    xlim = zeros(numel(tforms),2);
    ylim = zeros(numel(tforms),2);
    for i = 1:numel(tforms)
        [xlim(i,:), ylim(i,:)] = tforms(i).outputLimits([1 imageSize(2)], [1 imageSize(1)]);
    end
    
    %% Busquem la imatge central a partir la mitja de X i Y
    %Obtens els centres X i Y de cada imatge
    avgXYLim = [mean(xlim, 2) mean(ylim, 2)];
    %Calculem el centre del mosaic
%     centerMosaic = [round(max(avgXYLim(:,1))+min(avgXYLim(:,1)))/2 ...
%                     round(max(avgXYLim(:,2))+min(avgXYLim(:,2)))/2];
    centerMosaic = [mean(avgXYLim(:,1),1) mean(avgXYLim(:,2),1)];
    
    %Restes a totes les imatges el centre del mapa per veure quina �s la
    %que est� m�s propera al centre
    errorImages = abs(avgXYLim(:,1)-centerMosaic(1))+abs(avgXYLim(:,2)-centerMosaic(2));
    %Ordenes les imatges segons l'error respecte el centre i et quedes amb
    %el que menys error t� (el primer)
    [~, idxy] = sort(errorImages);
    
    %%Aplicar la transformada inversa de la imatge central a la resta
    %Si la inversa �s poc precisa, calculem la seg�ent imatge pr�xima al centre
    idWarning = 'MATLAB:nearlySingularMatrix';
    warning('off',idWarning);
    lastwarn('');
    Tinv = invert(tforms(idxy(1)));
    indCentralImage = idxy(1);
    i = 2;
    [~, msgid] = lastwarn;
    while strcmp(msgid, idWarning)
        lastwarn('');
        Tinv = invert(tforms(idxy(i)));
        [~, msgid] = lastwarn;
        indCentralImage = idxy(i);
        i = i + 1;
    end
    warning('on',idWarning)
    for i = 1:numel(tforms)
        tforms(i).T = tforms(i).T * Tinv.T ;
    end

end

function [tforms, imgNames] = FeatureMatchingTform(baseDir, tipusDeteccio)
    %Inicialitzaci� de par�metres
    li=dir([baseDir '*.png']);
    despFrames = 1;
    margesX = 14; 
    margesY = 3;
    imgNames = cell(1,1);
    %initFrame = 1;%825;%1078;%1011;%925; 
    sz = size(li,1);%2300;300;
    tforms(1) = affine2d(eye(3));
    llindarRotacio = 10*despFrames; %Rotaci� m�xima permesa entre frames (per descartar outliers)
    llindarEscala = 0.08;%0.10 %Variaci� d'escala m�xima i m�nima permesa entre frames
    numMaxFramesSorollosos = 10; %Si s'han om�s X frames (tant sigui per falta d'inliers com per ser sorollosos)
                                %Aleshores el frame 1 (el frame que es comparava amb tota la resta),
                                %avan�ar� al seg�ent frame per intentar evitar entrar en un bucle on no es
                                %troben frames correlacionats.
                                
    %Par�metres per controlar l'aterrament
    llindarEscalaAterrament = 0.0008;
    maxFramesPerAvaluarAterrament = 40;
    llindarPercentatgeAterrament = 0.6;
    faseAterrament = false;
    framesAterrant = 0;   
    sensorLog = '2014-05-16 14-56-51 SynchronizedData.tsv';
    [data,~,frameNames] = tblread(sensorLog,'\t');
    frameNames = cellstr(frameNames);
    %El video pot contenir un fragment inicial on el drone no es mou o
    %s'est� enlairant, el qual no ens interessa ja que pot afegir soroll.
    %Per aix� hem de mirar a partir de quin frame el video �s estable.
    %initFrame = DescartarIniciVideo(tipusDeteccio, margesX, margesY, llindarRotacio, baseDir, li);
    initFrame = 428;
    imgNames{1,1} = li(initFrame).name; 
    noPassarFrame = true;
    [~, f2Points, f2Features] = LlegirFrame([baseDir frameNames{initFrame}], margesX, margesY, tipusDeteccio);
    numFramesSorollosos = 0;
     i = initFrame;
     tic
     while i<=size(frameNames,1)-1% && not(faseAterrament)
        if noPassarFrame
            %No ens hem de saltar cap frame. El f2 anterior passa a ser el nou f1
            %f1 = f2;
            f1Points = f2Points;
            f1Features = f2Features;
        elseif numFramesSorollosos >= numMaxFramesSorollosos 
            i = i - numFramesSorollosos*despFrames + despFrames; %Retrocedeixes al frame seg�ent de l'�ltim frame1.
            numFramesSorollosos = 0;
            [~, f1Points, f1Features] = LlegirFrame([baseDir frameNames{i}], margesX, margesY, tipusDeteccio);
            fprintf('Masses frames amb soroll acumulats. Es retrocediran %d frames per tornar a trobar coincid�ncies.\n', numMaxFramesSorollosos-1);
        end
        noPassarFrame = true;

        [~, f2Points, f2Features] = LlegirFrame([baseDir frameNames{i+1}], margesX, margesY, tipusDeteccio);

        %Match features by using their descriptors
        pairs = matchFeatures(f1Features, f2Features, 'MaxRatio', 0.6);

        matchedf1Points = f1Points(pairs(:, 1),:);
        matchedf2Points = f2Points(pairs(:, 2),:);

        %Escalem la Y per dos, ja que en el frame nom�s hem agafat la meitat de
        %les files
        matchedf1Points.Location(:,2) = matchedf1Points.Location(:,2)*2;
        matchedf2Points.Location(:,2) = matchedf2Points.Location(:,2)*2;
        
        % Estimate the transformation between I(n) and I(n-1).
        [tform, ~, ~, status] = ...
        estimateGeometricTransform(matchedf1Points, matchedf2Points, 'similarity', 'MaxNumTrials', 100000000, 'Confidence', 99.99999, 'MaxDistance', 1.5);

        if status ~= 0 %No hi ha suficients inliers, per tant passarem al seg�ent frame
            noPassarFrame = false;
            fprintf('%d/%d\tNo hi ha prous inliers entre els dos frames. S''agafar� el seg�ent.\n', i, sz);
        end

        if noPassarFrame
            %C�lcul de rotaci� mitjan�ant matriu inversa (matriu 3x3 amb escalatge, rotaci� i traslaci�).
            %(http://www.mathworks.es/es/help/vision/examples/find-image-rotation-and-scale-using-automated-feature-matching.html)
            %tform.invert.T --> matriu de transformaci� que mapeja els punts de
            %original a distorted
            %pDistorted = [inlierOriginal(6).Location 1]*tform.invert.T;
            %
            %tform.T --> mapeja punts de distorted a original
            %pOriginal = [inlierDistorted(6).Location 1]*tform.T;
            Tinv  = tform.invert.T;
            ss = Tinv(2,1);
            sc = Tinv(1,1);
            scale = sqrt(ss*ss + sc*sc);
            theta = atan2(ss,sc);%*180/pi;
            theta = theta*180/pi;
            
            if 1.00-scale > llindarEscalaAterrament
                    if framesAterrant == 0
                       comptadorPerValorarframesAterrant =  maxFramesPerAvaluarAterrament;
                    end
                    framesAterrant = framesAterrant + 1;
                    %fprintf('Frame Aterrant\n'); %Aterra al frame 3169-3267
            end
            
            if abs(theta) > llindarRotacio || abs(scale-1) > llindarEscala
                noPassarFrame = false;
                fprintf('%d/%d\tFrame soroll�s. S''agafar� el seg�ent.\tEscala: %.2f\tRotaci� entre frames: %.2f�\n', i, sz, scale, theta);
            else
                imgNames{end+1,1} = li(i+1).name;
                % Compute T(1) * ... * T(n-1) * T(n)
                n = size(tforms,2)+1;
                z1 = data(i,7); z2 = data(i+1,7);
                S = eye(3); S(1,1) = z2/z1; S(2,2) = S(1,1);
                R = eye(3); R(1:2,1:2) = [cosd(theta) sind(theta); -sind(theta) cosd(theta)];
                imageSize =  [287   693];
                T1 = eye(3); T1(3,1:2) = [-imageSize(2)/2 -imageSize(1)/2];
                T2 = eye(3); T2(3,1:2) = [imageSize(2)/2 imageSize(1)/2];
                tform.T = T1*S*R*T2;
                tform.T(3,1:2) = tform.T(3,1:2)+Tinv(3,1:2);
                %tform.T = tform.invert.T;
                tforms(n) = tform;
                tforms(n).T = tforms(n).T * tforms(n-1).T;
                if framesAterrant > 0
                    comptadorPerValorarframesAterrant = comptadorPerValorarframesAterrant - 1;
                end
                fprintf('%d/%d\tEscala: %.2f\tRotaci� entre frames: %.2f�\n', i, sz, scale, -theta);
            end
        end

        if not(noPassarFrame)
            numFramesSorollosos = numFramesSorollosos + 1;
        else
            numFramesSorollosos = 0;
        end
        %pause;
         if framesAterrant > 0 && comptadorPerValorarframesAterrant == 0
            if framesAterrant/maxFramesPerAvaluarAterrament >= llindarPercentatgeAterrament
                 faseAterrament = true;
            end
            framesAterrant = 0;
        end
        i = i + despFrames;
     end
    toc
    %Mirem si s'ha deixat de processar imatges pel drone aterrant
    if faseAterrament == true
       %En cas afirmatiu, esborrem els �ltims frames que hem processar per
       %verificar que s'estava aterrant
       tforms = tforms(1:end-maxFramesPerAvaluarAterrament);
       imgNames = imgNames(1:end-maxFramesPerAvaluarAterrament);
    end

end

function [frame, points, features] = LlegirFrame(path, margesX, margesY, tipusDeteccio, frame)
    if ~exist('tipusDeteccio','var')
        tipusDeteccio = 'FAST';
    end
    if ~exist('frame','var')
        frame=im2double(rgb2gray(imread(path)));
    end
    %Els frames contenen marges negres que no interessen.
    frame = frame(margesY:end-1, margesX:end-margesX, :); %En el cas de la Y nom�s t� un marge superior, no inferior
    %Ens quedem nom�s amb les linies senars
    frame = frame(1:2:end,:,:);
    %Detect feature points
    if strcmpi(tipusDeteccio, 'SURF')                    %4
        points = detectSURFFeatures(frame, 'NumOctaves', 4, 'NumScaleLevels', 4, 'MetricThreshold', 1000);
    elseif strcmpi(tipusDeteccio, 'MSER')
        points = detectMSERFeatures(frame, 'ThresholdDelta', 2, 'RegionAreaRange', [30 14000], 'MaxAreaVariation', 0.25);
    elseif strcmpi(tipusDeteccio, 'BRISK')              
        points = detectBRISKFeatures(frame, 'MinContrast', 0.01, 'MinQuality', 0.001, 'NumOctaves', 4, 'ROI', [1 1 size(frame,1) size(frame,1)]);
    elseif strcmpi(tipusDeteccio, 'Eigen')     
        points = detectMinEigenFeatures(frame,'MinQuality', 0.01, 'FilterSize', 5, 'ROI', [1 1 size(frame,1) size(frame,1)]);
    elseif strcmpi(tipusDeteccio, 'FAST')     
        points = detectFASTFeatures(frame,'MinQuality', 0.01, 'MinContrast', 0.01, 'ROI', [1 1 size(frame,1) size(frame,1)]);
    elseif strcmpi(tipusDeteccio, 'Harris')     
        points = detectHarrisFeatures(frame,'MinQuality', 0.01, 'FilterSize', 5, 'ROI', [1 1 size(frame,1) size(frame,1)]);
    else
       err = MException('TypeError:UnrecognizedType', ...
        'El tipus especificat ha de ser un dels seg�ents: SURF, MSER, BRISK, Eigen, FAST, Harris.'); 
       throw(err);
    end
        
    %Extract feature descriptors
    [features, points] = extractFeatures(frame, points, 'SURFSize', 64, 'Method', 'Auto');
    
end

function stableFrame = DescartarIniciVideo(tipusDeteccio, margesX, margesY, llindarRotacio, baseDir, li)
    %Aquesta funci� retorna el n� del frame on el video s'estabilitza. El
    %criteri per considerar que el video s'ha estabilitzat �s que es doni
    %un m�nim de frames consecutius (minNFramesEstables) on es compleixi:
    %  - El drone no est� parat/quiet (la translaci� ha de ser m�s gran que
    %       llindarTranslaci�)
    %  - No hi ha frames sorollosos. Aix� tan pot ser perqu� el frame �s
    %  soroll�s, com perqu� l'escala superi el llindarEscala, i per tant el
    %  drone estigui en fase d'enlairar-se
    % Si es troben minNFramesEstables consecutius que compleixin aquestes
    % condicions, el primer frame de tots els consecutius ser� el frame per
    % on es comen�ar� a realitzar el feature matching.
    
    sz = size(li,1);
    llindarTranslacio = 1; %Llindar pel qual direm que el drone est� quiet.
    llindarEscala = 0.001; %Aquest llindar �s m�s restrictiu que el llindar general de l'escala.
    %Es podria donar el cas de que el drone estigu�s quiet, es produ�s
    %soroll, per� no suficient com per detectar-lo, i com a tal es compl�s
    %que hi ha moviment i no soroll. Per evitar aix�, hi ha d'haver un
    %m�nim de frames seguits sense soroll i amb moviment.
    %Nota: per no soroll s'ent�n una escala pr�xima a 1(+-0.08).
    minNFramesEstables = 10;
    [~, f2Points, f2Features] = LlegirFrame([baseDir li(1).name], margesX, margesY, tipusDeteccio);
    i = 1;
    framesOK = 0; %Nombre de frames sense soroll i amb moviment
    %Mentre (no hi hagi moviment OR hi hagi frames sorollosos) AND no s'arribi al final
    while framesOK < minNFramesEstables && i<sz
%         f1 = f2;
        f1Points = f2Points;
        f1Features = f2Features;
        [~, f2Points, f2Features] = LlegirFrame([baseDir li(i+1).name], margesX, margesY, tipusDeteccio);
        %Match features by using their descriptors
        pairs = matchFeatures(f1Features, f2Features, 'MaxRatio', 0.6);

        matchedf1Points = f1Points(pairs(:, 1),:);
        matchedf2Points = f2Points(pairs(:, 2),:);

        %Escalem la Y per dos, ja que en el frame nom�s hem agafat la meitat de
        %les files
        matchedf1Points.Location(:,2) = matchedf1Points.Location(:,2)*2;
        matchedf2Points.Location(:,2) = matchedf2Points.Location(:,2)*2;

        [tform, inlierf1Points, inlierf2Points, status] = ...
        estimateGeometricTransform(matchedf1Points, matchedf2Points, 'similarity', 'MaxNumTrials', 100000000, 'Confidence', 99.99999, 'MaxDistance', 1.5);
        if status ~= 0 %No hi ha suficients inliers
            fprintf('%d/%d\tNo hi ha prous inliers.\n', i, sz);
          
            framesOK = 0;
        else
%             figure(5); 
%             showMatchedFeatures(f1, f2, inlierf1Points, inlierf2Points);
%             title('Matched Points (Inliers Only)');
%             legend('ptsFrame1','ptsFrame2');
%             hold off;
            Tinv  = tform.invert.T;
            ss = Tinv(2,1);
            sc = Tinv(1,1);
            scale = sqrt(ss*ss + sc*sc);
            theta = atan2(ss,sc);%*180/pi;
            translation = Tinv(3,1:2);
            %Mirem que el frame no sigui soroll�s (o que el drone no estigui quiet)
            if abs(theta) > llindarRotacio || abs(scale-1) > llindarEscala
                framesOK = 0;
                fprintf('%d/%d\tFrame soroll�s o drone enlairant-se.\tEscala: %.2f\tRotaci� entre frames: %.2f�\tTranslaci�: %.2f\n', i, sz, scale, theta, sum(abs(translation)));
            elseif sum(abs(translation)) < llindarTranslacio
                framesOK = 0;
                fprintf('%d/%d\tFrame sense moviment.\tEscala: %.2f\tRotaci� entre frames: %.2f�\tTranslaci�: %.2f\n', i, sz, scale, theta, sum(abs(translation)));
            else
                fprintf('%d/%d\tEscala: %.2f\tRotaci� entre frames: %.2f�\tTranslaci�: %.2f\n', i, sz, scale, -theta, sum(abs(translation)));
                framesOK = framesOK +1;
            end
        end
        i = i + 1;
        %pause();
    end
    stableFrame = i-framesOK; %Hauria de donar vora 544
    fprintf('El video s''estabilitza a partir del frame %d, �s a dir, el drone no est� parat ni s''est� enlairant.\n', stableFrame);
end