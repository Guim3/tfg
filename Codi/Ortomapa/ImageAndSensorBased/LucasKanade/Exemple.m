
function Exemple()
  verbose = 1;
  %addpath('../lk20-common');

  load('LucasKanade/data/takeo');

  % Cal que imatge i template siguin double
  
  img = double(tdata.img);
  tmplt = img(tdata.tmplt(2):tdata.tmplt(4), ...
		       tdata.tmplt(1):tdata.tmplt(3));
  
  subplot(121); imshow(img,[]); title('Imatge'); subplot(122), ...
      imshow(tmplt,[]); title('template');
  
  disp('Mostra imatge i template. Prem una tecla');
  pause
  
  % Inicialitzaci� de l'homogradia de partida, que 'afina' el lucas
  % - kanade
  p_init = zeros(2,3);
  p_init(1, 3) = 1;%tdata.tmplt(1) - 1;
  p_init(2, 3) = tdata.tmplt(2) - 1;
  p_init(3, 3) = 1;
  
  %Exemple de com indicar que la template en la imatge 2 estar� rotada
  %segons yaw i translladada segons T (punt invariant centre template)
  yaw = -10;
  R = [cosd(yaw) sind(yaw) 0; -sind(yaw) cosd(yaw) 0; 0 0 1];
  T1 = [1 0 0; 0 1 0; -size(tmplt,2)/2 -size(tmplt,1)/2 1];
  T2 = [1 0 0; 0 1 0; size(tmplt,2)/2 size(tmplt,1)/2 1];
  T = [1 0 0; 0 1 0; 100 0 1];
  p_init = T1*R*T2*T;
  p_init(1,1) = p_init(1,1)-1;
  p_init(2,2) = p_init(2,2)-1;
  p_init = p_init';
  
  % Translate by 0.5 pixels to avoid identity warp. Warping introduces a little
  % smoothing and this avoids the case where the first iteration for a forwards
  % algorithm is on the "unsmoothed" unwarped image
  p_init(1, 3) = p_init(1, 3) + 0.5;
  p_init(2, 3) = p_init(2, 3) + 0.5;
  
  % Resultat si partim de tenir ja el template on toca 
  n_iters = 1;
  %fit = homo_fa(img,tmplt,p_init,n_iters,verbose); 
  fit = affine_fa(img,tmplt,p_init(1:2,:),n_iters,verbose); 
  final_fit = fit(end).warp_p;
  p_init
  final_fit
  
  disp('Cas p_init gaireb� al punt de convergencia.Prem una tecla');
  pause
  
  % Roto la imatge, per veure si Lucas-Kanade ajusta b� el template
  % (podria haver partir de uns valors inicials a p_init que
  %  assum�s que el template est� rotat)
  n_iters = 20;
  imgR = imrotate(img,-12,'crop');
  %fit = homo_fa(imgR,tmplt,p_init,n_iters,verbose); 
  fit = affine_fa(imgR,tmplt,p_init(1:2,:),n_iters,verbose); 
  final_fit = fit(end).warp_p;
  p_init
  final_fit 
  disp('Cas imatge rotada, p_init transformaci� nula. Prem una tecla');
  pause
  
  % Similar a l'exemple previ, partint d'una homografia inicial que
  % correspon al template rotat
  
  
  p_init = final_fit;
  %fit = homo_fa(img,tmplt,p_init,20,verbose); 
  fit = affine_fa(img,tmplt,p_init(1:2,:),n_iters,verbose); 
  final_fit = fit(end).warp_p;
  p_init
  final_fit
  disp('Cas imatge inicial, p_init indicant rotaci� inexistent');
  
