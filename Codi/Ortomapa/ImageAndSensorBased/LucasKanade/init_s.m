% init_s.m
% Common initialisation things for all similarity algorithms
% By Guim

% Need to process image data, must be real
if ~isa(img, 'double')
	img = double(img);
end

% Similarity warp
N_p = 4;
%[S Theta tx ty]
if prod(size(p_init)) ~= N_p
	error('Number of warp parameters incorrect');
end

% Initial warp parameters
% | s�cos   s�sin   tx |
% | -s�sin  s�cos   ty |
% |  0        0      1 |
s = p_init(1); theta = p_init(2);
tx = p_init(3); ty = p_init(4);
warp_p = [s*cosd(theta)-1 s*sind(theta) tx; ...
          -s*sin(theta) s*cos(theta)-1  ty;];

% Template size
h = size(tmplt, 1);
w = size(tmplt, 2);

% Template verticies, rectangular [minX minY; minX maxY; maxX maxY; maxX minY]
tmplt_pts = [1 1; 1 h; w h; w 1]';

% Verbose display of fitting?
if verbose
	verb_info = verb_init_a(img, tmplt, tmplt_pts, warp_p);
end