% Author: Daniel Ponsa Mussarra
% 2009/03/06

% Input image is smoothed (aprox.) with a sigma = 1 Gaussian.
% Levels of the pyramid correspond to the resampling of the input
% image, once has been smoothed by a sigma = 2 Gaussian. The
% matlab built-in function imPyramid does that just with sigma = 1;

% The smoothing is done with an aproximation of the Gaussian
% kernel, which allows performing that using integer arithmetic.
% Burt & Adelson kernel

% It could be implemented using directly a bigger kernel, with
% the weights [1 4 6 4 1].

% Since matlab takes no advantage of integer arithmetic, I have
% changed the implementation, using generic built in functions in
% the image processing library.

% The parameter borderOpt controls how the problem of the kernel
% convolution on the image borders is treated. By default, the
% 'out-of-image' values are set to the neighbouring pixel value
% (i.e. 'replicate'). If the parameter is set to 0, then zero
% values are considered for 'out-of-image' pixels, and the kernel
% convolution is rounded to the smallest integer, obtaining the
% same results than an implementation based on integer
% arithmetic. 

function pyramid = MultiresolutionPyramid(img, nLevels, borderOpt)

  assert(isfloat(img), 'Input image must be double');
  
  switch nargin
   case 1
    nLevels = 3;
    borderOpt = 'replicate';
   case 2
    borderOpt = 'replicate';
  end
  
  h = conv2([1 4 6 4 1],[1 4 6 4 1]')/256;

  
  % Level 0: source image convolved with sigma=1 gaussian.
    
%    l = bitshift(conv2([1 2 1],[1 2 1],...
%	          conv2([1 2 1],[1 2 1],img,'same'),'same'),-8);

    l = imfilter(img,h,borderOpt);

    if ~borderOpt 
      l = floor(l);
    end
    
    pyramid = {l};
    
  % Other levels in the pyramid: sigma=2 convolution + resampling

    for i=1:nLevels
      if (size(l,1)>4) && (size(l,2)>4)
	% sigma = sqrt(2)
%	tmp = bitshift(conv2([1 2 1],[1 2 1],...
%	           conv2([1 2 1],[1 2 1],l,'same'),'same'),-8);
        tmp = imfilter(l,h,borderOpt);

	% sigma = 2; 
	
%	tmp = bitshift(conv2([1 2 1],[1 2 1],...
%	           conv2([1 2 1],[1 2 1],tmp,'same'),'same'),-8);
%	tmp = bitshift(conv2([1 2 1],[1 2 1],...
%	           conv2([1 2 1],[1 2 1],tmp,'same'),'same'),-8);
  
        tmp = imfilter(tmp,h,borderOpt);
        tmp = imfilter(tmp,h,borderOpt);
      
	l = tmp(1:2:size(tmp,1), 1:2:size(tmp,2));
	
	
	if ~borderOpt 
	  l = floor(l);
	end

	pyramid(i+1) = {l};
      end      
    end
    
