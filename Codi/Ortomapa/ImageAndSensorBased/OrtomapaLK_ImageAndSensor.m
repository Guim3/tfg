%Link de refer�ncia: http://es.mathworks.com/help/vision/examples/feature-based-panoramic-image-stitching.html

function OrtomapaLK_ImageAndSensor(baseDir, tipusDeteccio, sensorLog, focal)
    close all;
    if ~exist('baseDir','var')
        currentpath = cd('..');
        cd('..');
        cd('..');
        parentpath = pwd();
        cd(currentpath);

        baseDir = [parentpath '\201405161455\'];
    end
    
    if ~exist('tipusDeteccio','var')
        tipusDeteccio = 'FAST';
    end
    
    if ~exist('sensorLog','var')
        sensorLog = '2014-05-16 14-56-51 SynchronizedData.tsv';
    end
    
    if ~exist('focal','var')
                 %X           Y
        focal = [1297.6342180147724 1227.3899167162];
    end
    warning('off','images:initSize:adjustingMag');
    margesX = 14; 
    margesY = 3;
    %Extraiem les matrius de transformaci� que mappejaran totes les imatges
    %relatives a la primera imatge. Tamb� retorna el nom de les imatges
    %que formaran part de l'orto mapa, �s a dir, que no tenen soroll.
    [tforms, imgNames] = FeatureMatchingTform(baseDir, sensorLog, tipusDeteccio, focal);
    save('Fitxers generats\tformsLK_image+sensor', 'tforms', 'imgNames');
    %load('tformsAux');
    
    %Recalculem les tforms perqu� les imatges siguin relatives a la imatge
    %central, ja que aix� no es propaga tant l'efecte de distorsi�
    %Aix� s'aconsegueix invertint la tform de la imatge central i aplicant
    %aquesta transformaci� a totes les altres
    li=dir([baseDir '*.png']);
    sampleImage = imread([baseDir li(1).name]);
    sampleImage = sampleImage(margesY:end-1, margesX:end-margesX, :);
    imageSize = size(sampleImage);
    [tforms, indCentralImage] = RecalcularTformsRespecteImatgeCentral(tforms, imageSize);
    %imshow(imread([baseDir imgNames{indCentralImage}]));
    
    %Reescalem les tforms perqu� l'orto mapa es crei m�s r�pid
    thetaInit = 188.4; scaleInit = 0.163; %Valors emp�rics
    tforms = ReescalarTforms(tforms, imageSize, thetaInit, scaleInit);
    
    %Inicialitzaci� del mosaic
    [mosaic, xLimits, yLimits] = InicialitzarMosaic(tforms, sampleImage, imageSize);
    
    %Creaci� del mosaic
    mosaic = CreacioMosaic(tforms, mosaic, xLimits, yLimits, baseDir, imgNames, margesX, margesY);
    
    figure();
    imshow(mosaic);
    imwrite(mosaic, 'Fitxers generats\mapaLK_image+sensor.jpg');
end

function mosaic = CreacioMosaic(tforms, mosaic, xLimits, yLimits, baseDir, imgNames, margesX, margesY)
    %Use imwarp to map images into the mosaic and 
    %use vision.AlphaBlender to overlay the images together.
    blender = vision.AlphaBlender('Operation', 'Binary mask', ...
    'MaskSource', 'Input port');
    
    % Create a 2-D spatial reference object defining the size of the mosaic.
    %An imref2d object encapsulates the relationship between the intrinsic 
    %coordinates anchored to the rows and columns of a 2-D image and the 
    %spatial location of the same row and column locations in a world coordinate system
    mosaicView = imref2d([size(mosaic,1) size(mosaic,2)], xLimits, yLimits);
    idWarning = 'MATLAB:nearlySingularMatrix';
    warning('off',idWarning);
    % Create the mosaic.
    for i = 1:3:size(imgNames,1)

        I = imread([baseDir imgNames{i}]);
        I = I(margesY:end-1, margesX:end-margesX, :);
        %I = I(1:2:end,:,:);
        
        % Transform I into the mosaic.
        warpedImage = imwarp(I, tforms(i), 'OutputView', mosaicView);

        % Create a mask for the overlay operation.
%         warpedMask = imwarp(ones(size(I(:,:,1))), tforms(i), 'OutputView', mosaicView);

        % Clean up edge artifacts in the mask and convert to a binary image.
%         warpedMask = warpedMask >= 1;
        warpedMask = rgb2gray(warpedImage) > 0.5;
        % Overlay the warpedImage onto the mosaic.
        mosaic = step(blender, mosaic, warpedImage, warpedMask);
        fprintf('%d/%d\n', i, size(imgNames,1));
%          figure(1); imshow(I);
%         figure(2); imshow(warpedImage);
        figure(3); imshow(mosaic);
         drawnow;
         pause;
    end
    warning('on',idWarning);
end

function [mosaic, xLimits, yLimits] = InicialitzarMosaic(tforms, I, imageSize)
    xlim = zeros(numel(tforms),2);
    ylim = zeros(numel(tforms),2);
    for i = 1:numel(tforms)
        [xlim(i,:), ylim(i,:)] = outputLimits(tforms(i), [1 imageSize(2)], [1 imageSize(1)]);
    end

    % Find the minimum and maximum output limits
    xMin = min(xlim(:));
    xMax = max(xlim(:));
    yMin = min(ylim(:));
    yMax = max(ylim(:));
    
    xLimits = [xMin xMax];
    yLimits = [yMin yMax];

    % Width and height of mosaic.
    width  = round(xMax - xMin);
    height = round(yMax - yMin);
    if width > 6000 || height > 6000
        err = MException('ValueError:ExceededSize', ...
        sprintf('La mida del mosaic �s massa gran com perqu� pugui ser processat eficientment. %dx%d', height, width)); 
       throw(err);
    end
    % Initialize the "empty" mosaic.
    mosaic = zeros([height width size(I,3)], 'like', I);
end

function tforms = ReescalarTforms(tforms, imageSize, thetaInit, scaleInit)
    %Reescala el valor de les tforms perqu� l'orto mapa creat sigui m�s
    %petit i per tant es computi m�s r�pid.
    
    T1 = eye(3); T1(3,1:2) = [-imageSize(2)/2 -imageSize(1)/2];
    T2 = eye(3); T2(3,1:2) = [imageSize(2)/2 imageSize(1)/2];
    
    Rinit = eye(3); 
    Rinit(1:2,1:2) = [cosd(thetaInit) sind(thetaInit); ...
                     -sind(thetaInit) cosd(thetaInit)];
    Sinit = eye(3); Sinit(1,1) = scaleInit; Sinit(2,2) = Sinit(1,1);
    tformInit = T1*Sinit*Rinit*T2;
    
    %Propaguem el canvi a la resta de tforms
    for i=1:size(tforms,2)
       tforms(i).T = tforms(i).T*tformInit; 
    end
end

function [tforms, indCentralImage] = RecalcularTformsRespecteImatgeCentral(tforms, imageSize)
    %% Compute the output limits  for each transform
    % Com que volem trobar la imatge central, primer mirem la mida de tot
    % el "mapa". Aleshores, com que sabrem el centre del mapa, podem trobar
    % la imatge central.
    xlim = zeros(numel(tforms),2);
    ylim = zeros(numel(tforms),2);
    for i = 1:numel(tforms)
        [xlim(i,:), ylim(i,:)] = tforms(i).outputLimits([1 imageSize(2)], [1 imageSize(1)]);
    end
    
    %% Busquem la imatge central a partir la mitja de X i Y
    %Obtens els centres X i Y de cada imatge
    avgXYLim = [mean(xlim, 2) mean(ylim, 2)];
    %Calculem el centre del mosaic
%     centerMosaic = [round(max(avgXYLim(:,1))+min(avgXYLim(:,1)))/2 ...
%                     round(max(avgXYLim(:,2))+min(avgXYLim(:,2)))/2];
    centerMosaic = [mean(avgXYLim(:,1),1) mean(avgXYLim(:,2),1)];
    
    %Restes a totes les imatges el centre del mapa per veure quina �s la
    %que est� m�s propera al centre
    errorImages = abs(avgXYLim(:,1)-centerMosaic(1))+abs(avgXYLim(:,2)-centerMosaic(2));
    %Ordenes les imatges segons l'error respecte el centre i et quedes amb
    %el que menys error t� (el primer)
    [~, idxy] = sort(errorImages);
    
    %%Aplicar la transformada inversa de la imatge central a la resta
    %Si la inversa �s poc precisa, calculem la seg�ent imatge pr�xima al centre
    idWarning = 'MATLAB:nearlySingularMatrix';
    warning('off',idWarning);
    lastwarn('');
    Tinv = invert(tforms(idxy(1)));
    indCentralImage = idxy(1);
    i = 2;
    [~, msgid] = lastwarn;
    while strcmp(msgid, idWarning)
        lastwarn('');
        Tinv = invert(tforms(idxy(i)));
        [~, msgid] = lastwarn;
        indCentralImage = idxy(i);
        i = i + 1;
    end
    warning('on',idWarning)
    for i = 1:numel(tforms)
        tforms(i).T = tforms(i).T * Tinv.T ;
    end

end

function [tforms, imgNames] = FeatureMatchingTform(baseDir, sensorLog, tipusDeteccio, focal)
    %Inicialitzaci� de par�metres
    despFrames = 1;
    margesX = 14; 
    margesY = 3;
    midaXtemplate = 50; %80; %Ha de ser divisible entre dos
    midaYtemplate = 50;%80; %Ha de ser divisible entre dos
    n_iters_LK = 500;
    imgNames = cell(1,1);
    tforms(1) = affine2d(eye(3));
    llindarRotacio = 10*despFrames; %Rotaci� m�xima permesa entre frames (per descartar outliers)
    llindarEscala = 0.08;%0.10 %Variaci� d'escala m�xima i m�nima permesa entre frames
    numMaxFramesSorollosos = 10; %Si s'han om�s X frames (tant sigui per falta d'inliers com per ser sorollosos)
                                %Aleshores el frame 1 (el frame que es comparava amb tota la resta),
                                %avan�ar� al seg�ent frame per intentar evitar entrar en un bucle on no es
                                %troben frames correlacionats.
    %Lectura del fitxer amb les dades del sensor
    [data,~,frameNames] = tblread(sensorLog,'\t');
    frameNames = cellstr(frameNames);
    %Descartem els frames on el drone s'est� enlairant o est� aterrant
    %a partir de les dades del sensor
    [frameNames, data] = DescartarAterramentEnlairamentVideo(frameNames, data);
    sz = size(frameNames,1);
%     try 
%         load('tformsAux');
%         initFrame = size(tforms,2);
%     catch
%         initFrame = 1; %Variable DEBUG
        imgNames{1,1} = frameNames{1}; 
%     end

    x2 = data(initFrame,3); y2 = data(initFrame,2); z2 = data(initFrame,7); 
    yaw2 = data(initFrame,13); pitch2 = data(initFrame,16); roll2 = data(initFrame,10);
    noPassarFrame = true;
    [f2, nextf2Points, ~] = LlegirFrame([baseDir frameNames{initFrame}], margesX, margesY, tipusDeteccio);
    imageSize = size(f2);
    numFramesSorollosos = 0;
    translacioPitchRollAnterior = [];
    %Variables debug
    sum = [0 0];
    orientacio = 0;
    %Fi variables debug
    i = initFrame;
     tic
     while i<=size(frameNames,1)-1
        %%PAS 1: Detectar punts caracter�stics amb el FAST en imatge 1
        if noPassarFrame
            %Tot correcte, cap frame soroll�s
            f1 = f2;
            f1Points = nextf2Points;
            x1=x2; y1=y2; z1=z2;
            yaw1=yaw2; pitch1=pitch2; roll1=roll2;
        elseif numFramesSorollosos >= numMaxFramesSorollosos 
            %Si entres aqu� �s perqu� hi ha hagut masses frames sorollosos
            %seguits. En aquest cas es tornaran a buscar correspond�ncies
            %entre els frames n+1 i n+2, sent el frame n el primer que ha
            %comen�ar a ser soroll�s.
            i = i - numFramesSorollosos*despFrames + despFrames; %Retrocedeixes al frame seg�ent de l'�ltim frame1.
            numFramesSorollosos = 0;
            [f1, f1Points, ~] = LlegirFrame([baseDir frameNames{i}], margesX, margesY, tipusDeteccio);
            x1 = data(i,3); y1 = data(i,2); z1 = data(i,7); 
            yaw1 = data(i,13); pitch1 = data(i,16); roll1 = data(i,10);
            fprintf('Masses frames amb soroll acumulats. Es retrocediran %d frames per tornar a trobar coincid�ncies.\n', numMaxFramesSorollosos-1);
        end
        noPassarFrame = true;
        x2 = data(i+despFrames,3); y2 = data(i+despFrames,2); z2 = data(i+despFrames,7); 
        yaw2 = data(i+despFrames,13); pitch2 = data(i+despFrames,16); roll2 = data(i+despFrames,10);
        
        [f2, nextf2Points, ~] = LlegirFrame([baseDir frameNames{i+1}], margesX, margesY, tipusDeteccio);

      %%PAS 2: Crear templates segons els punts detectats en la imatge 1.
        %Eliminem els punts que estiguin al marge de la imatge i no
        %permetin crear una template completa
        mask = f1Points.Location(:,1)-midaXtemplate >= 1 & f1Points.Location(:,1)+midaXtemplate <= size(f1,2) ...
             & f1Points.Location(:,2)-midaYtemplate >= 1 & f1Points.Location(:,2)+midaYtemplate <= size(f1,1);
        f1Points = f1Points(mask,:);
        %Si hi ha molt solapament entre templates es pot eliminar punts
        %que puguin estar molt aprop
        count = 1;
        while count <=size(f1Points,1) %Per cada punt
           point = f1Points.Location(count,:);
           %Eliminem la resta de punts pr�xims
           tmp = f1Points(count+1:end,:);
           mask = abs(tmp.Location(:,1)-point(1))>15 | ...%midaXtemplate | ...
                  abs(tmp.Location(:,2)-point(2))>15;%midaYtemplate;
           
           f1Points = [f1Points(1:count); tmp(mask,:)];
           count = count + 1;
        end
        %Les templates es crearan tenint el punts de FAST com a centre, i
        %no en el (0,0), tot i que es podria fer aix� tamb�
        templates = zeros(midaYtemplate, midaXtemplate, size(f1Points,1));
        for p=1:size(f1Points,1)
           templates(:,:,p) = f1(...
                            f1Points.Location(p,2)-midaYtemplate/2:f1Points.Location(p,2)+midaYtemplate/2-1, ...
                            f1Points.Location(p,1)-midaXtemplate/2:f1Points.Location(p,1)+midaXtemplate/2-1 ...
                            ); 
        end
        
       %%% CODI AFEGIT
            f1PointsCopy = f1Points;
       %%%% End codi afegit 
       
      %%PAS 3: Utilitzar Lucas-Kanade per trobar on est� aquesta template en la imatge 2
        %La gr�cia d'aplicar LK tamb� �s en part detectar caracter�stiques
        %nom�s quan es perdi el tracking. Ara per ara es detecten a cada
        %iteraci� per la m�xima robustesa, per� potser no faria falta. Si
        %es fes aix�, s'haurien de passar a la seg�ent iteraci� nom�s els
        %inliers, i no tots els punts (�s el que es fa ara).
        
        %Prediure la posici� dels punts detectats pel FAST en el seg�ent
        %frame utilitzant la informaci� proporcionada pel sensor.
        %fprintf('%d/%d\t',i,size(frameNames,1));
        [tformPrediction, translacioPitchRollAnterior, params]=TransformarPuntsSegonsSensor(imageSize, ...
                        x1, x2, y1, y2, z1, z2, yaw1, yaw2, pitch1, pitch2, roll1, roll2, translacioPitchRollAnterior, focal);
                    
        %%Transformem els punts
        %f1Points marquen el centre de les templates. Ara es cridar� a la
        %funci� LK, a la qual se li ha de passar el (0,0) de la template a
        %buscar, i no el centre. Per aix� mateix ara recol�locarem f1Points 
        %al (0,0) de la template creada, ja que aix� els punts transformats
        %indicaran el (0,0) de la template.
        f1Points.Location = [f1Points.Location(:,1)-midaXtemplate/2 f1Points.Location(:,2)-midaYtemplate/2];
        transformedPoints = [f1Points.Location ones(size(f1Points,1),1)]*tformPrediction;
        transformedPoints = transformedPoints(:,1:2);
                    
        %Eliminem els punts que quedin fora de la imatge despr�s de la transformaci�
        isValid = transformedPoints(:,1) >= 1 & transformedPoints(:,1) <= size(f2,2) &...
                  transformedPoints(:,2) >= 1 & transformedPoints(:,2) <= size(f2,1);
        f1Points = f1Points(isValid,:);
        transformedPoints = transformedPoints(isValid,:);
        templates = templates(:,:,isValid);
%         figure(1);imshow(f1);hold on; plot(f1Points.Location(:,1), f1Points.Location(:,2), 'o');
%         figure(2);imshow(f2);hold on; plot(transformedPoints(:,1), transformedPoints(:,2),'o');

        LKtforms = cell(1, size(f1Points,1));
        %f2Points cont� la posici� d'on est� situat el (0,0) de la template
        %en el f2, mentre que f1Point cont� la posici� (0,0) de la template en el f1
        f2Points = zeros(size(f1Points,1), 2);
        %p_init equival al punt de partida (coordenades imatge) on LK comen�a a fer el descens de gradient
        p_init = eye(3,3);
        p_init(1:2,1:2) = tformPrediction(1:2,1:2)';
        p_init(1,1) = p_init(1,1)-1; %S'ha de restar -1 per seguir el format que demana la funcio LK
        p_init(2,2) = p_init(2,2)-1; %ja que dins de la funci� se suma +1 a aquests valors
        
        %%% CODI AFEGIT: Aplicaci� Lucas Kanade en una pir�mide multiresoluci� ESCALA
        %%% -------------------------------------------------------
        nLevels = 2; %3;
        image1Pyr = MultiresolutionPyramid(f1,nLevels);
        image2Pyr = MultiresolutionPyramid(f2,nLevels);                 
        pyramidMode = 1;
        %%% End CODE AFEGIT
        
        for t=1:size(f1Points,1) %Per cada template apliquem LK
            %Totes les templates estan expresades per la mateixa tform ja que
            %la imatge �s est�tica, �s a dir, no hi ha objectes movent-se en
            %direccions diferents i independents (no seria el cas de fer el tracking d'un cotxe, per ex).
            p_init(1:2,3) = transformedPoints(t,:)'; 
            
            %%% CODI AFEGIT
            if pyramidMode
                              
                pInitSS = p_init;
                pInitSS(1, 3) = pInitSS(1, 3)/(2^(nLevels+1));
                pInitSS(2, 3) = pInitSS(2, 3)/(2^(nLevels+1));   
                
                top = f1PointsCopy.Location(t,2)-midaYtemplate/2;
                bottom = f1PointsCopy.Location(t,2)+midaYtemplate/2-1;
                left = f1PointsCopy.Location(t,1)-midaXtemplate/2;
                right = f1PointsCopy.Location(t,1)+midaXtemplate/2-1;
                
                for j=size(image1Pyr,2):-1:1

                    image1SS = image1Pyr{j};
                    image2SS = image2Pyr{j};

                    factorDiv = 2^(j-1);
                                               
                    % We use ceil, to avoid generating coordinates < 1.
                    topSS = ceil(top/factorDiv);
                    leftSS = ceil(left/factorDiv);

                    bottomSS = round(bottom/factorDiv);
                    rightSS = round(right/factorDiv);
		
                    tmpltSS = image1SS(topSS:bottomSS,leftSS:rightSS);
	
                    % Propagation of the affine parameters at the preceding
                    % pyramid level up to this level -> only translation
                    % parameters are affectede.
	
                    pInitSS(1, 3) = pInitSS(1, 3)*2;
                    pInitSS(2, 3) = pInitSS(2, 3)*2;    
	
	
                    fit = affine_fa(image2SS, tmpltSS, ...
                                pInitSS(1:2,:),n_iters_LK,0,1);

                    pInitSS = fit(end).warp_p;
                end    
            else
            %%% End CODE AFEGIt
                tmplt = templates(:,:,t);
                fit = affine_fa(f2,tmplt,p_init(1:2,:),n_iters_LK,0); %modificar sd_update per canviar afinitat
            end
            %Extraiem la tform de LK i la posem en el format amb el que
            %es crear� l'ortomapa
            LKtforms{t} = [fit(end).warp_p' [0; 0; 1]];
            %Extraiem la translaci�
            tmp = [0 0 1]*LKtforms{t};
            f2Points(t,:) = tmp(1:2);
%             tmp = [f1Points(t,:).Location 1] * LKtforms{t};
%             f2Points(t,:) = tmp(1:2);
            %fprintf('\tLK templates: %d/%d\n', t, size(f1Points,1));%pause; 
        end
       
%         figure(2);imshow(f1);hold on; plot(f1Points.Location(:,1), f1Points.Location(:,2), 'o');
%         figure(3);imshow(f2);hold on; plot(transformedPoints(:,1), transformedPoints(:,2),'o');
%         figure(4);imshow(f2);hold on; plot(f2Points(:,1), f2Points(:,2),'o');
        
        %Eliminem els f2Points que estiguin fora de la imatge.
        mask = f2Points(:,1) >= 1 & f2Points(:,1) <= size(f2,2) ...
             & f2Points(:,2) >= 1 & f2Points(:,2) <= size(f2,1);
        f1Points = f1Points(mask,:);
        f2Points = f2Points(mask,:);
        %Escalem la Y per dos, ja que en el frame nom�s hem agafat la meitat de
        %les files
        f1Points.Location(:,2) = f1Points.Location(:,2)*2;
        f2Points(:,2) = f2Points(:,2)*2;
        
        %Convertim f2Points la mateixa classe que f1Points (cornerPoints)
        f2Points = cornerPoints(f2Points);
        
        
        % Estimate the transformation between I(n) and I(n-1).
        [tform, inlierf1Points, inlierf2Points, status] = ...
        estimateGeometricTransform(f1Points, f2Points, 'similarity', 'MaxNumTrials', 10000000, 'Confidence', 99.99, 'MaxDistance', 1.5);
%         figure(5); 
%         showMatchedFeatures(f1, f2, inlierf1Points, inlierf2Points);
%         title('Matched Points (after RANSAC)');
%         legend('ptsFrame1','ptsFrame2');
%         hold off;
%         drawnow;
        if status ~= 0 %No hi ha suficients inliers, per tant passarem al seg�ent frame
            noPassarFrame = false;
            fprintf('%d/%d\tNo hi ha prous inliers entre els dos frames. S''agafar� el seg�ent. \tN� templates: %d\n', i, sz, size(templates,3));
        end

        if noPassarFrame
            %C�lcul de rotaci� mitjan�ant matriu inversa (matriu 3x3 amb escalatge, rotaci� i traslaci�).
            %(http://www.mathworks.es/es/help/vision/examples/find-image-rotation-and-scale-using-automated-feature-matching.html)
            %tform.invert.T --> matriu de transformaci� que mapeja els punts de
            %original a distorted
            %pDistorted = [inlierOriginal(6).Location 1]*tform.invert.T;
            %
            %tform.T --> mapeja punts de distorted a original
            %pOriginal = [inlierDistorted(6).Location 1]*tform.T;
            Tinv  = tform.invert.T;
            ss = Tinv(2,1);
            sc = Tinv(1,1);
            scale = sqrt(ss*ss + sc*sc);
            theta = atan2(ss,sc);%*180/pi;
            theta = theta*180/pi;


            if abs(theta) > llindarRotacio || abs(scale-1) > llindarEscala
                noPassarFrame = false;
                fprintf('%d/%d\tFrame soroll�s. S''agafar� el seg�ent.\tEscala: %.2f\tRotaci� entre frames: %.2f�\tN� templates: %d\n', i, sz, scale, theta, size(templates,3));
            else
                imgNames{end+1,1} = frameNames{i+1,1};
                % Compute T(1) * ... * T(n-1) * T(n)
                n = size(tforms,2)+1;
                tform.T = tform.invert.T;
                tforms(n) = tform;
                tforms(n).T = tforms(n-1).T * tforms(n).T;
                fprintf('Frame: %d/%d\tEscala: %.2f\tRotaci� entre frames: %.2f�\tN� templates: %d\n', i, sz, scale, -theta, size(templates,3));
                %%DEBUG
%                     %C�lcul de dist�ncia recorreguda
%                     aux = orientacio(end)+theta;
%                     if aux > 0;
%                         graus = 360;
%                     else
%                         graus = -360;
%                     end
%                     orientacio(end+1) = mod(aux,graus);
%                     aux1 = inlierf1Points.Location;
%                     aux2 = inlierf2Points.Location;
%                     [f1Location, f2Location] = RectificarPosicions(aux1, aux2, f2, -theta, orientacio(end-1));
%                     distancies = zeros(size(f1Location,1),2);
%                     distancies(:,1) = f1Location(:,1) - f2Location(:,1); 
%                     distancies(:,2) = f2Location(:,2) - f1Location(:,2);
%                     offsetX = mean(distancies(:,1));
%                     offsetY = mean(distancies(:,2));
%                     sum = DibuixarTrajectoria(offsetX, offsetY, sum);
                %FI DEBUG
            end
        end

        if not(noPassarFrame)
            numFramesSorollosos = numFramesSorollosos + 1;
        else
            numFramesSorollosos = 0;
        end

        %pause;
        i = i + despFrames;
%         save('tformsAux', 'tforms', 'imgNames');
     end
    toc

end

function [tform, translacioPitchRollAnterior, params]=TransformarPuntsSegonsSensor(imageSz, x1, x2, y1, y2, z1, z2, yaw1, yaw2, pitch1, pitch2, roll1, roll2, translacioPitchRollAnterior, focal)
    %C�lcul de la posici� inicial on buscar els punts a partir de les
    %dades del sensor
    
    %C�lculem la translaci� del pitch i el roll del primer frame
    if isempty(translacioPitchRollAnterior)
        thetaPitch = pitch1;
        offsetPitch = [0 meters2pixel(z1*sind(thetaPitch),z2,focal(2))];
        thetaRoll = roll1;
        offsetRoll = [meters2pixel(z1*sind(thetaRoll),z1,focal(1)) 0];
        translacioPitchRollAnterior = [offsetPitch(1)+offsetRoll(1) ...
                                       offsetPitch(2)+offsetRoll(2)];
    end
    %%Translaci�
        %Convertim punts de latitud i longitud en dist�ncies de metres
        [distXmtr,~] = distance([0 x1],[0 x2]);
        distXmtr = distXmtr*111319.9; %Degree to meters
        if x1 > x2
            %La funci� d'abans et d�na la dist�ncia sempre positiva, per� s'ha de saber el sentit (positiu o negatiu)
            distXmtr = -distXmtr; 
        end

        [distYmtr,~] = distance([y1 0],[y2 0]);
        distYmtr = distYmtr*111319.9; %Degree to meters
        if y1 < y2
            distYmtr = -distYmtr;
        end
        %Per la m�xima precisi� s'ha de separar la focal X i la focal Y
        %Explicaci�:
        %Arribats a aquest punt tenim la dist�ncia en metres RESPECTE la
        %latidud i longitud. Per passar de metres a p�xels, per�,
        %necessitem saber d'aquesta dist�ncia quina part es correspon a
        %moviment horitzontal i vertical en p�xels, ja que la focal de la
        %c�mera pot ser diferent verticalment o horitzontalment.
        
        %Rotem la dist�ncia perqu� sigui segons X Y de la imatge, i no
        %latitud longitud.
        distMtrRotated = [distXmtr distYmtr] * [cosd(-yaw2) sind(-yaw2); -sind(-yaw2) cosd(-yaw2)];
        %Conversi� de metres a p�xels
        distPxlRotated = [meters2pixel(distMtrRotated(1), z2, focal(1)) ...
                          meters2pixel(distMtrRotated(2), z2, focal(2))];
        %Anules pitch i roll anterior.
        distXpxl = distPxlRotated(1)-translacioPitchRollAnterior(1);
        distYpxl = distPxlRotated(2)-translacioPitchRollAnterior(2);
        
    %%Rotem el punts de f1 (n) a la mateixa orientaci� que els punts de f2 (n+1)
        %ROTACI� YAW (Z)
        %S'ha de traslladar la imatge en el centre (0,0) de coordenades m�n
        %per escalar-la i rotar-la, i despr�s fer una altra traslaci� per
        %deixar la imatge on estava.
        thetaYaw = yaw1-yaw2; %Angle entre f1 i f2. Rotaci� per Z
        cosYaw = cosd(thetaYaw);
        sinYaw = sind(thetaYaw);
        RYaw = eye(3); RYaw(1:2,1:2) = [cosYaw sinYaw; -sinYaw cosYaw];
        
%         translacioPitchRollAnterior = translacioPitchRollAnterior*[cosYaw sinYaw; -sinYaw cosYaw];
%         
%         distXpxl = distXpxl+translacioPitchRollAnterior(1);
%         distYpxl = distYpxl+translacioPitchRollAnterior(2);
        %ROTACI� PITCH (X)
        %thetaPitch positiu: imatge ha de baixar (Y+offset)
        %thetaPitch negatiu: imatge ha de pujar (Y-offset)
        thetaPitch = pitch2;
        sinPitch = sind(thetaPitch);
        offsetPitch = [0 meters2pixel(z2*sinPitch,z2,focal(2))];
        offsetPitchRotated = offsetPitch*[cosYaw sinYaw; -sinYaw cosYaw];%Rotar segons yaw de f2
        %Apliquem la rotaci� per X directament en la translaci�, en comptes
        %de rotar tota la imatge. 
        distXpxl = distXpxl+offsetPitchRotated(1);
        distYpxl = distYpxl+offsetPitchRotated(2);
        
        %ROTACI� ROLL (Y)
        thetaRoll = roll2;
        sinRoll = sind(thetaRoll);
        offsetRoll = [meters2pixel(z2*sinRoll,z2,focal(1)) 0];
        offsetRollRotated = offsetRoll*[cosYaw sinYaw; -sinYaw cosYaw];
        distXpxl = distXpxl+offsetRollRotated(1);
        distYpxl = distYpxl+offsetRollRotated(2);
        T = eye(3); T(3,1:2)= [distXpxl distYpxl];
        
        %MOLT IMPORTANT: has de rotar respecte el centre de massa de la
        %imatge, �s a dir, la imatge ha d'estar centrada en el (0,0).
        %Per aix� mateix s'ha de traslladar el centre de massa fins al (0,0) 
        %(T1), rotar (R), i invertir la translaci� (T2)
        T1 = eye(3); T1(3,1:2) = [-imageSz(2)/2 -imageSz(1)/2];
        T2 = eye(3); T2(3,1:2) = [imageSz(2)/2 imageSz(1)/2];
        
        %Guardem la translaci� provocada pel pitch i el roll ja que per la
        %seg�ent imatge necessitarem invertir aquesta translaci�: la imatge
        %s'ha de crear respecte la posici� del GPS de l'UAV, i no respecte
        %la projecci� de la imatge. Si guardes la translaci� en una
        %variable a part pots evitar que es propagui en les matrius.
        %No guardes els punts rotats perqu� per la seg�ent iteraci� el
        %frame N-1 (que �s l'actual frame N) no estar� rotat de base.
        translacioPitchRollAnterior = [offsetPitch(1)+offsetRoll(1) ...
                                       offsetPitch(2)+offsetRoll(2)];
        
      %%Escalatge
        %Si f1 m�s lluny que f2, escala > 1.00 (cas aterrament)
        S = eye(3); S(1,1) = z2/z1; S(2,2) = S(1,1);
        
      %%Creem la matriu de transformaci� geom�trica
        tform = T1*S*RYaw*T2*T;
      
      %%Par�metres que defineixen la matriu de transformaci� geom�trica (similaritat)
        params = [S(1,1) thetaYaw T(3,1:2)];
%         fprintf('Yaw: %.2f�\tPitch:%.2f�\tRoll: %.2f�\n', thetaYaw, thetaPitch, thetaRoll);
end

function [sum] = DibuixarTrajectoria(offsetX, offsetY, sum)
            figure(1); %set(1,'Visible','off');
            hold on; axis equal; %axis([-500 500 -500 500]);
            aux = sum;
            sum(1) = sum(1) + offsetX;
            sum(2) = sum(2) + offsetY;
            %plot(offsetX(1:i), offsetY(1:i));
            title(sprintf('Frame %d', size(offsetX,1)));
            plot([aux(1) sum(1)], [aux(2) sum(2)]);
            hold off;
end

function [frame, points, features] = LlegirFrame(path, margesX, margesY, tipusDeteccio, frame)
    if ~exist('tipusDeteccio','var')
        tipusDeteccio = 'FAST';
    end
    if ~exist('frame','var')
        frame=im2double(rgb2gray(imread(path)));
    end
    %Els frames contenen marges negres que no interessen.
    frame = frame(margesY:end-1, margesX:end-margesX, :); %En el cas de la Y nom�s t� un marge superior, no inferior
    %Ens quedem nom�s amb les linies senars
    frame = frame(1:2:end,:,:);
    %Detect feature points
    if ~strcmpi(tipusDeteccio, 'None') 
        if strcmpi(tipusDeteccio, 'SURF')                    %4
            points = detectSURFFeatures(frame, 'NumOctaves', 4, 'NumScaleLevels', 4, 'MetricThreshold', 1000);
        elseif strcmpi(tipusDeteccio, 'MSER')
            points = detectMSERFeatures(frame, 'ThresholdDelta', 2, 'RegionAreaRange', [30 14000], 'MaxAreaVariation', 0.25);
        elseif strcmpi(tipusDeteccio, 'BRISK')              
            points = detectBRISKFeatures(frame, 'MinContrast', 0.01, 'MinQuality', 0.001, 'NumOctaves', 4, 'ROI', [1 1 size(frame,1) size(frame,1)]);
        elseif strcmpi(tipusDeteccio, 'Eigen')     
            points = detectMinEigenFeatures(frame,'MinQuality', 0.01, 'FilterSize', 5, 'ROI', [1 1 size(frame,1) size(frame,1)]);
        elseif strcmpi(tipusDeteccio, 'FAST')     
            points = detectFASTFeatures(frame,'MinQuality', 0.01, 'MinContrast', 0.01, 'ROI', [1 1 size(frame,1) size(frame,1)]);
        elseif strcmpi(tipusDeteccio, 'Harris')     
            points = detectHarrisFeatures(frame,'MinQuality', 0.01, 'FilterSize', 5, 'ROI', [1 1 size(frame,1) size(frame,1)]);
        else
           err = MException('TypeError:UnrecognizedType', ...
            'El tipus especificat ha de ser un dels seg�ents: SURF, MSER, BRISK, Eigen, FAST, Harris.'); 
           throw(err);
        end

        %Extract feature descriptors
        [features, points] = extractFeatures(frame, points, 'SURFSize', 64, 'Method', 'Auto');
        points = points.selectStrongest(size(points,1));
    else
        features = NaN;
        points = NaN;
    end
    
end

function [frameNamesLog, data] = DescartarAterramentEnlairamentVideo(frameNamesLog, data)
    %Aquesta funci� retorna el l'interval de frames on el drone no est� enlairant-se
    %ni aterrant. El criteri considerat ha sigut l'altitud del sensorLog.
    % L'assumpci� aqu� es que les imatges es prendran des d'una 
    % al�ada constant durant el vol, que ser� el valor m�xim a que es volar�
    % (aprox)
    
%     sz = size(li,1);
    %%Busquem el punt on deixa de guanyar altitud
    thrAltUp = max(data(:,7))*0.9;
    mask = data(:,7)>=thrAltUp;
    ind = find(mask); 
    initFrameSensor = ind(1);%�ndex del 1r frame del sensorLog que supera el threshold
    endFrameSensor = ind(end);%�ndex de l'�ltim frame del sensor log que supera el threshold
%     tmp = frameNamesLog(mask,1);
%     initFrameName = tmp{1,:}; %Nom del 1r frame que supera el threshold
%     endFrameName = tmp{end,:};
%     %%Busquem l'�ndex del frame segons el seu nom
%     initFrameIm = findStrStruct(initFrameName, li, 'normal');
%     endFrameIm = findStrStruct(endFrameName, li, 'backwards');
    frameNamesLog = frameNamesLog(initFrameSensor:endFrameSensor,:);
    data = data(initFrameSensor:endFrameSensor,:);
%     offset = initFrameIm-initFrameSensor;
%     fprintf('El video �s estable entre els frames %d-%d, �s a dir, entre aquests frames el drone no s''enlaira ni aterra.\n', initFrameIm, endFrameIm);
end

function [i, trobat] = findStrStruct(str, li, direction)
    if ~exist('direction','var')
       direction = 'normal'; 
    end
    trobat = false;
    if strcmp(direction,'normal')
        i=1;
        while i <= size(li,1) && not(trobat)
              if strcmpi(str, li(i).name)
                  trobat = true;
              else
                  i = i + 1;
              end
        end
    elseif strcmp(direction,'backwards')
        i = size(li,1);
        while i > 0 && not(trobat)
              if strcmpi(str, li(i).name)
                  trobat = true;
              else
                  i = i - 1;
              end
        end
    end
end

function [f1Points, f2Points]=  RectificarPosicions(f1Points, f2Points, f2, degree, orientation)
    %%FUNCI� DEBUG
    %%Rectificar rotaci�
    %Primer de tot rotem els punts del frame 2 perqu� tinguin la mateixa
    %correspondencia amb els punts del frame 1.
    %Per mantenir el sistema de refer�ncia, el frame 2 rotar� sobre el seu
    %centre de massa, i no sobre el (0, 0). Per tant es far�:
    %punts(x,y)*T(-sz(1)/2,-sz(2)/2)*R(alpha)*T(sz(1)/2,sz(2)/2)
     %Per exemple, si el robot est� quiet i nom�s rota, hauria de marcar
    %despla�ament 0.
    %1a forma: multiplicaci� de matrius
    alpha=degree*pi/180;
    T1 = eye(3); T1(3,1:2) = [-size(f2,2)/2 -size(f2,1)/2];
    T2 = T1; T2(3,1:2) = [size(f2,2)/2 size(f2,1)/2];
    R = eye(3); R(1:2,1:2) = [cos(alpha) sin(alpha); -sin(alpha) cos(alpha)];
    f2Points = [f2Points ones(size(f2Points,1),1)]*T1*R*T2;
    f2Points = f2Points(:,1:2);
    %2a forma: estalviar-se multiplicacions
    %T(-sz(1)/2,-sz(2)/2)
%     T = repmat([size(f2,2)/2 size(f2,1)/2],size(f2Points,1),1); 
%     f2Points = f2Points-T; 
%     %R(alpha)
%     xp = f2Points(:,1)*cos(alpha)-f2Points(:,2)*sin(alpha); 
%     yp = f2Points(:,1)*sin(alpha)+f2Points(:,2)*cos(alpha);
%     f2Points = [xp yp];
%     %T(sz(1)/2,sz(2)/2)
%     f2Points = f2Points+T;

    %%Dibuixar resultat (opcional)
    %Si f2Points t� algun punt negatiu donar� problemes per representar-ho
    %Borrem aquest punt tant en f1Points com en f2Points
    dibuixar_resultat = false;
    if dibuixar_resultat
        [rows, ~] = find(f2Points<0);
        if ~isempty(rows)
            rows = unique(rows); %Mirem a quines files el nombre �s negatiu
            mask =  true(size(f2Points)); %Creem una m�scara de 1=positiu 0=negatiu
            mask(rows,:) = zeros(length(rows),size(f2Points,2));
            %Passem aquesta m�scara pel f2Points, i li donem format (sin� �s un array)
            auxf2 = reshape(f2Points(mask),size(f2Points,1)-length(rows),size(f2Points,2));
            auxf1 = reshape(f1Points(mask),size(f1Points,1)-length(rows),size(f1Points,2));
        else
           auxf1 = f1Points; 
           auxf2 = f2Points; 
        end
        auxf1(:,2) = auxf1(:,2)/2;
        auxf2(:,2) = auxf2(:,2)/2;
        a = append(SURFPoints,auxf1);
        b = append(SURFPoints,auxf2);

        r = imrotate(f2, -degree, 'crop');
        figure(6); 
        showMatchedFeatures(f1, r, a, b);
        title('Matched Points (Inliers Only)');
        legend('ptsFrame1','ptsFrame2');
        hold off;
%         figure(7); 
%         showMatchedFeatures(f1, r, a, b, 'montage');
%         title('Matched Points (Inliers Only)');
%         legend('ptsFrame1','ptsFrame2');
%         hold off;
    end

    %%Rectificar orientaci� 
    %Apliquem la rotaci� de l'orientaci� acumulada perqu� X i Y sempre
    %siguin sobre el mateix sistema de refer�ncia (mapa), i no depengui de la
    %posici� del dron.
    alpha = -orientation*pi/180; %Orientation ha de ser positiu perqu� es fa el canvi
    %d'orientaci� abans de fer offsetY = y2-y1. I �s que s'ha de remarcar
    %que les imatges s�n matrius, i per tant la Y augmenta cap abaix
    %(canviant aix� el sentit del vector, i per tant la rotaci� s'ha de fer
    %al sentit oposat. Per aix� mateis el imrotate fa -degree)
    xp1=f1Points(:,1)*cos(alpha)-f1Points(:,2)*sin(alpha);
    yp1=f1Points(:,1)*sin(alpha)+f1Points(:,2)*cos(alpha);
    xp2=f2Points(:,1)*cos(alpha)-f2Points(:,2)*sin(alpha);
    yp2=f2Points(:,1)*sin(alpha)+f2Points(:,2)*cos(alpha);
    f1Points = [xp1 yp1];
    f2Points = [xp2 yp2];

end
