function [pixels] = meters2pixel(metres, altitud, focal)
    %Converteix els metres especificats en p�xels.
    %metres = metres reals que medeix l'objecte real o metres del moviment
    %altitud = dist�ncia entre la c�mera i el pla avaluable
    %focal = focal de la c�mera (presumiblement en les especificacions) o
    %        tamb� deduible mitjan�ant proves emp�riques
    pixels=(focal*metres)/altitud;

end