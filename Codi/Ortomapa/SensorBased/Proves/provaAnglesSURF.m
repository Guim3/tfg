%tform.invert.T --> matriu de transformaci� que mapeja els punts de
%original a distorted
%pDistorted = [inlierOriginal(6).Location 1]*tform.invert.T;
%
%tform.T --> mapeja punts de distorted a original
%pOriginal = [inlierDistorted(6).Location 1]*tform.T;
format long g
%Imatge traslladada -50 (X) i -100 (Y) i rotada 30�
original = im2double(imread('cameraman.tif'));
theta = 30;
traslacioX = 100;
traslacioY = 50;
distorted = original;
distorted = imrotate(distorted,theta, 'crop'); 
distorted = [ones(size(distorted,1),traslacioX) distorted]; %Traslaci� 100 cap a la dreta
distorted = distorted(:,1:end-traslacioX);
%distorted = [ones(traslacioY,size(distorted,2)); distorted];
%distorted = distorted(1:end-traslacioY,:);
ptsOriginal  = detectFASTFeatures(original);
ptsDistorted = detectFASTFeatures(distorted);
[featuresOriginal,  validPtsOriginal]  = extractFeatures(original,  ptsOriginal);
[featuresDistorted, validPtsDistorted] = extractFeatures(distorted, ptsDistorted);
indexPairs = matchFeatures(featuresOriginal, featuresDistorted);
matchedOriginal  = validPtsOriginal(indexPairs(:,1));
matchedDistorted = validPtsDistorted(indexPairs(:,2));
figure(1);
showMatchedFeatures(original,distorted,matchedOriginal,matchedDistorted);
%showMatchedFeatures(original,distorted,SURFPoints,SURFPoints);
[tform, inlierDistorted, inlierOriginal] = estimateGeometricTransform(...
    matchedDistorted, matchedOriginal, 'similarity');
Tinv  = tform.invert.T;
ss = Tinv(2,1);
sc = Tinv(1,1);
scaleRecovered = sqrt(ss*ss + sc*sc);
theta1 = atan2(ss,sc)*180/pi;
Jregistered = imwarp(distorted,tform,'OutputView',imref2d(size(original)));
falsecolorOverlay = imfuse(original,Jregistered);
figure(2), imshow(falsecolorOverlay,'InitialMagnification','fit');

%Crear la matriu des de 0
tform2 = affine2d(eye(3));
R = eye(3);
R(1:2,1:2) = [cosd(-theta) sind(-theta); -sind(-theta) cosd(-theta)];
T1 = eye(3); T1(3,1:2) = [-size(original,2)/2 -size(original,1)/2];
T2 = eye(3); T2(3,1:2) = [size(original,2)/2 size(original,1)/2];
T = eye(3); T(3,1:2)= [traslacioX 0];
tform2.T = T1*R*T2*T;
tform2.T = tform2.invert.T;
u = [0 1]; 
v = [0 0]; 
[x, y] = transformPointsForward(tform2, u, v); 
dx = x(2) - x(1); 
dy = y(2) - y(1); 
theta2 = (180/pi) * atan2(dy, dx);
Jregistered = imwarp(distorted,tform2,'OutputView',imref2d(size(original)));
falsecolorOverlay = imfuse(original,Jregistered);
figure(3), imshow(falsecolorOverlay,'InitialMagnification','fit');

%Crear la matriu passant dos punts
R = eye(3);
R(1:2,1:2) = [cosd(-theta) sind(-theta); -sind(-theta) cosd(-theta)];
T = eye(3); T(3,1:2)= [traslacioX 0];
distortedPoints = [0 0; size(original,2) size(original,1); size(original,2) 0; 0 size(original,1)];
originalPoints = [distortedPoints ones(size(distortedPoints,1),1)]*T1*R*T2*T;
originalPoints = originalPoints(:,1:2);
tform3 = fitgeotrans(distortedPoints,originalPoints,'similarity');
tform3.T = tform3.invert.T;
u = [0 1]; 
v = [0 0]; 
[x, y] = transformPointsForward(tform3, u, v); 
dx = x(2) - x(1); 
dy = y(2) - y(1); 
theta3 = (180/pi) * atan2(dy, dx);
Jregistered = imwarp(distorted,tform3,'OutputView',imref2d(size(original)));
falsecolorOverlay = imfuse(original,Jregistered);
figure(4), imshow(falsecolorOverlay,'InitialMagnification','fit');
% %Imatge nom�s traslladada -50 (X) i -100 (Y)
% distorted2 = original(50:end,100:end); 
% ptsOriginal  = detectSURFFeatures(original);
% ptsDistorted = detectSURFFeatures(distorted2);
% [featuresOriginal,  validPtsOriginal]  = extractFeatures(original,  ptsOriginal);
% [featuresDistorted, validPtsDistorted] = extractFeatures(distorted2, ptsDistorted);
% indexPairs = matchFeatures(featuresOriginal, featuresDistorted);
% matchedOriginal  = validPtsOriginal(indexPairs(:,1));
% matchedDistorted = validPtsDistorted(indexPairs(:,2));
% figure;
% showMatchedFeatures(original,distorted2,matchedOriginal,matchedDistorted);
% [tform2, inlierDistorted, inlierOriginal] = estimateGeometricTransform(...
%     matchedDistorted, matchedOriginal, 'similarity');

disp('Matriu de transformaci� de la imatge traslladada i rotada (MATLAB)');
disp(tform.T);
fprintf('Theta extreta de la matriu: %.2f\n\n', theta1);
disp('Matriu de transformaci� de la imatge traslladada i rotada (manual)');
disp(tform2.T);
fprintf('Theta extreta de la matriu: %.2f\n\n', theta2);
disp('Matriu de transformaci� de la imatge traslladada i rotada (manual donant 2 punts)');
disp(tform3.T);
fprintf('Theta extreta de la matriu: %.2f\n\n', theta3);
% disp('Matriu de transformaci� de la imatge nom�s traslladada');
% disp(tform2.T);
% 
% disp('La traslaci� �s correcte en la segona imatge (100 i 50), per� amb la rotaci� la traslaci� deixa de ser acurada.');
% disp('Qu� caldria fer per tenir la traslaci� despr�s d''invertir la rotaci�?');