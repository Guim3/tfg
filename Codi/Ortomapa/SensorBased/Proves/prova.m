load george
theta = pi/4;
A2 = [cos(theta) sin(theta) 0; -sin(theta) cos(theta) 0; 0 0 1];
T = eye(3); T(3,1:2)= [1 0];
T2 = eye(3); T2(3,1:2)= [-1 0];
tform2 = maketform('affine', T*T2*A2*T);
uv2 = tformfwd(tform2, [x y]);

subplot(1,2,1)
plot(x,y), axis ij, axis equal, axis([-2 2 -2 2]), grid on, title('George')

subplot(1,2,2)
plot(uv2(:,1), uv2(:,2)), axis ij, axis equal, axis([-2 2 -2 2])
grid on
title('Rotated by 45\circ')