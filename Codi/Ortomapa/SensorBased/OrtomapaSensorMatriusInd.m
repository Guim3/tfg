%Link de refer�ncia: http://es.mathworks.com/help/vision/examples/feature-based-panoramic-image-stitching.html
function OrtomapaSensor(baseDir, sensorLogDir, focal)
    format long g;
    global pitch;
    global roll;
    pitch = []; roll = [];
    if ~exist('baseDir','var')
        currentpath = cd('..');
        cd('..');
        cd('..');
        parentpath = pwd();
        cd(currentpath);

        baseDir = [parentpath '\201405161455\'];
    end
    
    if ~exist('sensorLog','var')
        sensorLogDir = '2014-05-16 14-56-51 SynchronizedData.tsv';
    end
    
    if ~exist('focal','var')
                 %X           Y
        focal = [1297.6342180147724 1227.3899167162];
    end
    
    warning('off','images:initSize:adjustingMag');
    margesX = 14; 
    margesY = 3;
    li=dir([baseDir '*.png']);
    sampleImage = imread([baseDir li(1).name]);
    sampleImage = sampleImage(margesY:end-1, margesX:end-margesX, :);
    imageSize = size(sampleImage);
    %Extraiem les matrius de transformaci� que mappejaran totes les imatges
    %relatives a la primera imatge. Les matrius de transformaci�
    %s'extrauran convertint la informaci� del sensor (latitud, longitud,
    %orientaci�) a matriu de transformaci�.
    [tforms, imgNames] = ExtreureTformInfoSensor(sensorLogDir, imageSize, focal);

    %Recalculem les tforms perqu� les imatges siguin relatives a la imatge
    %central, ja que aix� no es propaga tant l'efecte de distorsi�
    %Aix� s'aconsegueix invertint la tform de la imatge central i aplicant
    %aquesta transformaci� a totes les altres
    
    %[tforms, indCentralImage] = RecalcularTformsRespecteImatgeCentral(tforms, imageSize);
    
    %Reescalem les tforms perqu� l'orto mapa es crei m�s r�pid
    thetaInit = -0.6512; scaleInit = 0.163; %Valors emp�rics
    tforms = ReescalarTforms(tforms, imageSize, thetaInit, scaleInit);
    
    %Inicialitzaci� del mosaic
    [mosaic, xLimits, yLimits] = InicialitzarMosaic(tforms, sampleImage, imageSize);
    
    %Creaci� del mosaic
    mosaic = CreacioMosaic(tforms, mosaic, xLimits, yLimits, baseDir, imgNames, margesX, margesY);
    imwrite(mosaic, 'mapa.jpg');
    figure(10);
    imshow(mosaic);
end

function tforms = ReescalarTforms(tforms, imageSize, thetaInit, scaleInit)
    %Reescala el valor de les tforms perqu� l'orto mapa creat sigui m�s
    %petit i per tant es computi m�s r�pid.
    
    T1 = eye(3); T1(3,1:2) = [-imageSize(2)/2 -imageSize(1)/2];
    T2 = eye(3); T2(3,1:2) = [imageSize(2)/2 imageSize(1)/2];
    
    Rinit = eye(3); 
    Rinit(1:2,1:2) = [cosd(thetaInit) sind(thetaInit); ...
                     -sind(thetaInit) cosd(thetaInit)];
    Sinit = eye(3); Sinit(1,1) = scaleInit; Sinit(2,2) = Sinit(1,1);
    tformInit = T1*Sinit*Rinit*T2;
    
    %Propaguem el canvi a la resta de tforms
    for i=1:size(tforms,2)
       tforms(i).T = tforms(i).T*tformInit; 
    end
end

function mosaic = CreacioMosaic(tforms, mosaic, xLimits, yLimits, baseDir, imgNames, margesX, margesY)
    %Use imwarp to map images into the mosaic and 
    %use vision.AlphaBlender to overlay the images together.
    blender = vision.AlphaBlender('Operation', 'Binary mask', ...
    'MaskSource', 'Input port');
    global pitch;
    global roll;
    % Create a 2-D spatial reference object defining the size of the mosaic.
    %An imref2d object encapsulates the relationship between the intrinsic 
    %coordinates anchored to the rows and columns of a 2-D image and the 
    %spatial location of the same row and column locations in a world coordinate system
    mosaicView = imref2d([size(mosaic,1) size(mosaic,2)], xLimits, yLimits);
    idWarning = 'MATLAB:nearlySingularMatrix';
    warning('off',idWarning);
    % Create the mosaic.
    k = 1;
    for i = 1:3:size(imgNames,1)
        I = imread([baseDir imgNames{i}]);
        I = I(margesY:end-1, margesX:end-margesX, :);
        %I = I(1:2:end,:,:);
        
        % Transform I into the mosaic.
        warpedImage = imwarp(I, tforms(i), 'OutputView', mosaicView);

        % Create a mask for the overlay operation.
%         warpedMask = imwarp(ones(size(I(:,:,1))), tforms(i), 'OutputView', mosaicView);

        % Clean up edge artifacts in the mask and convert to a binary image.
%         warpedMask = warpedMask >= 1;
        warpedMask = rgb2gray(warpedImage) > 0.5;
        % Overlay the warpedImage onto the mosaic.
        mosaic = step(blender, mosaic, warpedImage, warpedMask);
        fprintf('%d/%d\tPitch: %.2f\t Roll: %.2f\n', i, size(imgNames,1), pitch(i), roll(i));
        
        if k == 1
            figure(10); imshow(mosaic);
            drawnow;
            %pause;
            k = 0;
        end
        k = k + 1;
    end
    warning('on',idWarning);
end

function [mosaic, xLimits, yLimits] = InicialitzarMosaic(tforms, I, imageSize)
    xlim = zeros(numel(tforms),2);
    ylim = zeros(numel(tforms),2);
    for i = 1:numel(tforms)
        [xlim(i,:), ylim(i,:)] = outputLimits(tforms(i), [1 imageSize(2)], [1 imageSize(1)]);
    end

    % Find the minimum and maximum output limits
    xMin = min(xlim(:));
    xMax = max(xlim(:));
    yMin = min(ylim(:));
    yMax = max(ylim(:));
    
    xLimits = [xMin xMax];
    yLimits = [yMin yMax];

    % Width and height of mosaic.
    width  = round(xMax - xMin);
    height = round(yMax - yMin);
    if width > 6000 || height > 6000
        err = MException('ValueError:ExceededSize', ...
        'La mida del mosaic �s massa gran com perqu� pugui ser processat eficientment.'); 
       throw(err);
    end
    % Initialize the "empty" mosaic.
    mosaic = zeros([height width size(I,3)], 'like', I);
end

function [tforms, indCentralImage] = RecalcularTformsRespecteImatgeCentral(tforms, imageSize)
    %% Compute the output limits  for each transform
    % Com que volem trobar la imatge central, primer mirem la mida de tot
    % el "mapa". Aleshores, com que sabrem el centre del mapa, podem trobar
    % la imatge central.
    xlim = zeros(numel(tforms),2);
    ylim = zeros(numel(tforms),2);
    for i = 1:numel(tforms)
        [xlim(i,:), ylim(i,:)] = tforms(i).outputLimits([1 imageSize(2)], [1 imageSize(1)]);
    end
    
    %% Busquem la imatge central a partir la mitja de X i Y
    %Obtens els centres X i Y de cada imatge
    avgXYLim = [mean(xlim, 2) mean(ylim, 2)];
    %Calculem el centre del mosaic
%     centerMosaic = [round(max(avgXYLim(:,1))+min(avgXYLim(:,1)))/2 ...
%                     round(max(avgXYLim(:,2))+min(avgXYLim(:,2)))/2];
    centerMosaic = [mean(avgXYLim(:,1),1) mean(avgXYLim(:,2),1)];
    
    %Restes a totes les imatges el centre del mapa per veure quina �s la
    %que est� m�s propera al centre
    errorImages = abs(avgXYLim(:,1)-centerMosaic(1))+abs(avgXYLim(:,2)-centerMosaic(2));
    %Ordenes les imatges segons l'error respecte el centre i et quedes amb
    %el que menys error t� (el primer)
    [~, idxy] = sort(errorImages);
    
    %%Aplicar la transformada inversa de la imatge central a la resta
    %Si la inversa �s poc precisa, calculem la seg�ent imatge pr�xima al centre
    idWarning = 'MATLAB:nearlySingularMatrix';
    warning('off',idWarning);
    lastwarn('');
    Tinv = invert(tforms(idxy(1)));
    indCentralImage = idxy(1);
    i = 2;
    [~, msgid] = lastwarn;
    while strcmp(msgid, idWarning)
        lastwarn('');
        Tinv = invert(tforms(idxy(i)));
        [~, msgid] = lastwarn;
        indCentralImage = idxy(i);
        i = i + 1;
    end
    warning('on',idWarning)
    for i = 1:numel(tforms)
        tforms(i).T = tforms(i).T * Tinv.T ;
    end

end

function [tforms, frameNames] = ExtreureTformInfoSensor(sensorLogDir, imageSz, focal)
    format long g;
    global pitch;
    global roll;
    %%Llegir log sensor
    [data,header,frameNames] = tblread(sensorLogDir,'\t');
    frameNames = cellstr(frameNames);

    %https://bitbucket.org/Guim3/tfg/src/22a0f6f36f48/Ortomapa/SensorBased/OrtomapaSensor.m
    %%Inicialitzem les tforms
    T1 = eye(3); T1(3,1:2) = [-imageSz(2)/2 -imageSz(1)/2];
    T2 = eye(3); T2(3,1:2) = [imageSz(2)/2 imageSz(1)/2];
    tforms(size(frameNames,1)) = affine2d(eye(3));
    xInit = data(1,3); yInit = data(1,2); zInit = data(1,7); 
    yawInit = data(1,13);
    pitch2 = data(1,16); roll2 = data(1,10);
    distXOrigen = 0;
    distYOrigen = 0;
    %C�lcul de la primera translaci� del pitch i el roll
    thetaPitch = -pitch2;%pitch1-pitch2;
    pitch(end+1) = thetaPitch;
    cosPitch = cosd(thetaPitch);
    sinPitch = sind(thetaPitch);
    offsetPitch = [0 meters2pixel(zInit*sinPitch,zInit,focal(2))];
    offsetPitchRotated = offsetPitch*[cosd(yawInit) sind(yawInit); -sind(yawInit) cosd(yawInit)];
    thetaRoll = -roll2;
    roll(end+1) = thetaRoll;
    cosRoll = cosd(thetaRoll);
    sinRoll = sind(thetaRoll);
    offsetRoll = [meters2pixel(zInit*sinRoll,zInit,focal(1)) 0];
    offsetRollRotated = offsetRoll*[cosd(yawInit) sind(yawInit); -sind(yawInit) cosd(yawInit)];%Rotar segons el yaw total
   
    %El primer frame ha d'estar orientat segons la br�ixola, ja que la
    %latitud i la longitud s�n coordenades respecte nord-sud i est-oest, no
    %s�n translacions relatives a les imatges.
    tforms(1).T = T1*[cosd(yawInit) sind(yawInit) 0; -sind(yawInit) cosd(yawInit) 0; 0 0 1]*T2 ...
                   *[1 0 0; 0 1 0; offsetPitchRotated(1)+offsetRollRotated(1) offsetPitchRotated(2)+offsetRollRotated(2) 1];

    %%Per cada parell de frames calculem la tform que mappeja el frame N al N-1
    %%respecte el frame 1
    for i=2:size(frameNames,1)
        %%Llegim par�metres
        
        x2 = data(i,3); y2 = data(i,2); z2 = data(i,7); 
        yaw2 = data(i,13); pitch2 = data(i,16); roll2 = data(i,10);
        
      %%Translaci�
        %Convertim punts de latitud i longitud en dist�ncies de metres
        [distXmtr,~] = distance([0 xInit],[0 x2]);
        distXmtr = distXmtr*111319.9; %Degree to meters
        if xInit > x2
            %La funci� d'abans et d�na la dist�ncia sempre positiva, per� s'ha de saber el sentit (positiu o negatiu)
            distXmtr = -distXmtr; 
        end
        
        [distYmtr,~] = distance([yInit 0],[y2 0]);
        distYmtr = distYmtr*111319.9; %Degree to meters
        if yInit < y2
            distYmtr = -distYmtr;
        end
      
        %Per la m�xima precisi� s'ha de separar la focal X i la focal Y
        %Explicaci�:
        %Arribats a aquest punt tenim la dist�ncia en metres RESPECTE la
        %latidud i longitud, la qual cosa est� b� perqu� l'orto mapa el creem
        %a partir de latitud i longitud. Per passar de metres a p�xels, per�,
        %necessitem saber d'aquesta dist�ncia quina part es correspon a
        %moviment horitzontal i vertical en p�xels, ja que la focal de la
        %c�mera pot ser diferent verticalment o horitzontalment.
        
        %Rotem la dist�ncia perqu� sigui segons X Y de la imatge, i no
        %latitud longitud.
        distMtrRotated = [distXmtr distYmtr] * [cosd(-yaw2) sind(-yaw2); -sind(-yaw2) cosd(-yaw2)];
        %Conversi� de metres a p�xels
        distPxlRotated = [meters2pixel(distMtrRotated(1), z2, focal(1)) ...
                          meters2pixel(distMtrRotated(2), z2, focal(2))];
        %Invertim la rotaci� per tornar a deixar la dist�ncia segons
        %latitud i longitud
        distPxl = distPxlRotated * [cosd(yaw2) sind(yaw2); -sind(yaw2) cosd(yaw2)];
        
        %La imatge s'ha de crear respecte la posici� del GPS de l'UAV, i no respecte
        %la projecci� de la imatge. Per aix� restem la translaci� del pitch
        %i roll de la imatge anterior. En altres paraules, anules el pitch
        %i el roll anterior.
        distXpxl = distPxl(1);
        distYpxl = distPxl(2); 
%         distXpxl = meters2pixel(distXmtr, z1, mean(focal));
%         distYpxl = meters2pixel(distYmtr, z1, mean(focal));  

      %%Rotem el frame2 (n) a la mateixa orientaci� que el frame1 (n-1)
        %ROTACI� YAW (Z)
        %S'ha de traslladar la imatge en el centre (0,0) de coordenades m�n
        %per escalar-la i rotar-la, i despr�s fer una altra traslaci� per
        %deixar la imatge on estava.
        thetaYaw = yaw2; %Angle entre f1 i f2. Rotaci� per Z
        cosYaw = cosd(thetaYaw);
        sinYaw = sind(thetaYaw);
        RYaw = eye(3); RYaw(1:2,1:2) = [cosYaw sinYaw; -sinYaw cosYaw];
        
        %ROTACI� PITCH (X)
        %thetaPitch positiu: imatge ha de baixar (Y+offset)
        %thetaPitch negatiu: imatge ha de pujar (Y-offset)
        %Per fer-ho amb pitch1-pitch2 s'hauria de fer:
        %1. Eliminar per X i Y:  distXpxl = distPxl(1)-translacioPitchRollAnterior(1);
        %2. Rotar translacioPitchRollAnterior per yaw2-yaw1 (o potser yaw1-yaw2?)
        %   Com que tens el roll i el pitch respecte la imatge anterior, un
        %   roll = 0 vol dir que tens el mateix roll que la imatge
        %   anterior, per� potser la imatge actual t� un yaw diferent. Per
        %   aix� has de realitzar aquesta rotaci�.
        %3. Sumar la rotaci� del pas 2 a translacioPitchRollAnterior
        thetaPitch = -pitch2;%pitch1-pitch2;
        pitch(end+1) = thetaPitch;
        cosPitch = cosd(thetaPitch);
        sinPitch = sind(thetaPitch);

                                      %Abans z2*sinPitch/cosPitch (catetOposat = sinx*hipotenusa)
        offsetPitch = [0 meters2pixel(z2*sinPitch,z2,focal(2))];
%         offsetPitch = offsetPitch*[cosd(yaw2) sind(yaw2); -sind(yaw2) cosd(yaw2)];
        offsetPitchRotated = offsetPitch*[cosd(yaw2) sind(yaw2); -sind(yaw2) cosd(yaw2)];%Rotar segons el yaw total
        %Apliquem la rotaci� per X directament en la translaci�, en comptes
        %de rotar tota la imatge. 
        distXpxl = distXpxl+offsetPitchRotated(1);
        distYpxl = distYpxl+offsetPitchRotated(2);
        
        %ROTACI� ROLL (Y)
        thetaRoll = -roll2;%-roll1;
        roll(end+1) = thetaRoll;
        cosRoll = cosd(thetaRoll);
        sinRoll = sind(thetaRoll);
        offsetRoll = [meters2pixel(z2*sinRoll,z2,focal(1)) 0];
%         offsetRoll = offsetRoll*[cosd(yaw2) sind(yaw2); -sind(yaw2) cosd(yaw2)];
        offsetRollRotated = offsetRoll*[cosd(yaw2) sind(yaw2); -sind(yaw2) cosd(yaw2)];%Rotar segons el yaw total
        distXpxl = distXpxl+offsetRollRotated(1);
        distYpxl = distYpxl+offsetRollRotated(2);
        T = eye(3); T(3,1:2)= [distXpxl distYpxl];
        
        %MOLT IMPORTANT: has de rotar respecte el centre de massa de la
        %imatge, �s a dir, la imatge ha d'estar centrada en el (0,0) de
        %L'ESPAI M�N. Per aix� mateix s'ha de traslladar fins al (0,0) de
        %L'ESPAI M�N (T1), rotar (R), i invertir la traslaci� (T2)
        T1 = eye(3); T1(3,1:2) = [-imageSz(2)/2 -imageSz(1)/2];
        T2 = eye(3); T2(3,1:2) = [imageSz(2)/2 imageSz(1)/2];

      %%Escalatge
        %Si f2 m�s lluny que f1, escala > 1.00 (cas d'enlairament)
        S = eye(3); S(1,1) = z2/zInit; S(2,2) = S(1,1);
        tform = T1*S*RYaw*T2*T;
        tforms(i).T = tform;

        fprintf('%d/%d\n', i, size(frameNames,1));
    end
end

function frame = LlegirFrame(path, margesX, margesY, frame)
    if ~exist('frame','var')
        frame=im2double(rgb2gray(imread(path)));
    end
    %Els frames contenen marges negres que no interessen.
    frame = frame(margesY:end-1, margesX:end-margesX, :); %En el cas de la Y nom�s t� un marge superior, no inferior
    %Ens quedem nom�s amb les linies senars
    frame = frame(1:2:end,:,:);
 
end
