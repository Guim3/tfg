function varargout = DataLogViewer(varargin)
% DATALOGVIEWER M-file for DataLogViewer.fig
%      DATALOGVIEWER, by itself, creates a new DATALOGVIEWER or raises the existing
%      singleton*.
%
%      H = DATALOGVIEWER returns the handle to a new DATALOGVIEWER or the handle to
%      the existing singleton*.
%
%      DATALOGVIEWER('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DATALOGVIEWER.M with the given input arguments.
%
%      DATALOGVIEWER('Property','Value',...) creates a new DATALOGVIEWER or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before DataLogViewer_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to DataLogViewer_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help DataLogViewer

% Last Modified by GUIDE v2.5 21-Jul-2014 15:28:14

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @DataLogViewer_OpeningFcn, ...
                   'gui_OutputFcn',  @DataLogViewer_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before DataLogViewer is made visible.
function DataLogViewer_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to DataLogViewer (see VARARGIN)

% Choose default command line output for DataLogViewer
handles.output = hObject;

% Initialization of the directories to use
[hObject,handles] = InitSourceFiles(hObject,handles);

% Intialization of the data to annotate
[hObject,handles] = LoadSequenceInfo(hObject,handles);

% This sets up the initial plot - only do when we are invisible
% so window can get raised using DataLogViewer.
if strcmp(get(hObject,'Visible'),'off')
    handles = InitFrameSimulator(handles);  
    handles = InitPlots(handles);
end

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes DataLogViewer wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% ====================================================================
% == INITIALIZATION FUNCTIONS
% ====================================================================

function [hObject,handles] = InitSourceFiles(hObject,handles)
%    set(handles.sequenceNameEditBox,'String',...
%   'E:\DadesFirewatcher\SequenciesPropies\201405161414\');
 
    set(handles.sequenceNameEditBox,'String',...
   'D:\User\Documentos\Guim\Treballs\4t Carrera\Treball Fi de Grau\SequenciesPropies\201405161455\');


%  set(handles.sequenceNameEditBox,'String',...
  % 'E:\DadesFirewatcher\SequenciesPropies\201405161455\');
  
    % joan handles.ortoMapFileName = 'E:\DadesFirewatcher\SequenciesPropies\CampVolSantCugatWGS84.tif';
    handles.ortoMapFileName = 'D:\User\Documentos\Guim\Treballs\4t Carrera\Treball Fi de Grau\SequenciesPropies\Repositori\Sincronitzaciˇ\CampVolSantCugatWGS84.tif';

function handles = InitPlots(handles)  

  % Single Frame Init
  % ---------------------------------------- 

  axes(handles.frameViewAxis);
  axis([1 720 1 576]);
   
%  set(handles.totalFramesText,'String', sprintf('/%d', ...
%					      size(handles.fileList,2))); 
  UpdateFrame(handles);
  
  UpdatePlot(handles.axes1, handles.popupmenu1, handles);
  UpdatePlot(handles.axes2, handles.popupmenu2, handles);
   
  handles.prevMapView = -1;
  handles = UpdateMap(handles);
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %%% Flight simulation related code
  % -------------------------------------------------------------
  
function handles = InitFrameSimulator(handles)

  % Raster data, from where to emulate the flight

  [handles.ortoMap, handles.ortoInfo] = geotiffread(handles.ortoMapFileName);
               
  % Acquisition camera parameters
  % By analising a frame taken at a known height, and measuring the
  % observed data (using QGis for instance), the camera angle of view
  % is computed. Obviously, this could be determined from the
  % knowledge of the camera intrisics (camera calib parameters)

  % 'Estimated flight height...'
  flightHeight = round(max([handles.frameData{8}]));

  % Establishment of the camera related parameters
  % This should be estimated from calibration data

  handles.angleOfViewX = 45*pi/180;
  frameWidth = 720;
  frameHeight = 576;
  handles.frameAspectRatio = frameWidth/frameHeight;

  % Cell size info. Factor to convert between meters and pixels
  % according to the ortophoto resolutions (ICGC : 25cm/pixel)
  % Cells of same extent in X and Y are assumed.

  % This could be recovered from ortoInfo, put depending ot the CRS,
  % the information is expressed in degrees. For simplicity, now we
  % fix this info to the know value in meters, since using the
  % function 'distMeters' (Haversine distance) applied to the 
  % CellExtentInLatitude/Longitude is not as accurate.
  
  handles.cellSize = 0.25;
  
  observedAreaWidth = 2*tan(handles.angleOfViewX/2)*flightHeight;

  handles.effFocal = frameWidth/(2*tan(handles.angleOfViewX/2));

  % The simulator will generate 2 types of information:
  % 1: A crop of the ortophoto 1:1 of fixed dimension, centered at the
  %    frame (lat,long). The size will be fixed to match aprox. the 
  %    content of an acquired frame at the assumed flightHeight
  %
  % 2: A crop of the ortophoto, given the position, orientation and 
  %    elevation assigned to the frame

  handles.FixDimW = observedAreaWidth / handles.cellSize; % Meters to pixel conversion

  % Width multiple of 4 is inforced
  handles.FixDimW = ceil(handles.FixDimW/4)*4;
  handles.FixDimH = round(handles.FixDimW / handles.frameAspectRatio);

  
function [hObject,handles] = LoadSequenceInfo(hObject, handles);
  
% 2014-05-16 14-14-08 - SynchronizedData.tsv

% The tsv fils constains the image frame sequence,
% having each frame its associated UAV log data 


 %tsvDir = dir([get(handles.sequenceNameEditBox,'String') '* - SynchronizedData.tsv']);
%  if size(tsvDir,1)>0
   %handles.tsvFile = [get(handles.sequenceNameEditBox,'String') tsvDir(1).name];
   handles.tsvFile = 'D:\User\Documentos\Guim\Treballs\4t Carrera\Treball Fi de Grau\SequenciesPropies\Repositori\Ortomapa\SensorBased\2014-05-16 14-56-51 SynchronizedData orig.tsv';
   handles = LoadTSVFile(handles);
  
   % Init plot selectors
   dataFields = [handles.fileHeader{3:end}]'; 
   set(handles.popupmenu1,'String',dataFields);
   set(handles.popupmenu2,'String',dataFields);
%  end
 
 % Init map selectors
 mapOptions = ['map              '; ...
               'map crop         '; ...
               'flight simulation'];
 set(handles.mapPopupmenu,'String',mapOptions);
 
% Initialization of the 'single frame' displayed 

handles.currentFrameIndex = 1;
  
 set(handles.currentFrameEditBox,'String',...
		    sprintf('%d', handles.currentFrameIndex));
  
 guidata(hObject,handles);

function handles = LoadTSVFile(handles)
  fid = fopen(handles.tsvFile);
  
  handles.fileHeader = textscan(fid, [repmat('%s', 1, 18)], 1)';  
  handles.frameData = textscan(fid, ['%s' repmat('%f',1,17)]);  
   
  fclose(fid);

% ====================================================================
% == UPDATE DISPLAY FUNCTIONS
% ====================================================================

% --- Executes on button press in updatePlot1Button.
function updatePlot1Button_Callback(hObject, eventdata, handles)
% hObject    handle to updatePlot1Button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

UpdatePlot(handles.axes1, handles.popupmenu1, handles);

% --- Executes on button press in updatePlot2Button.
function updatePlot2Button_Callback(hObject, eventdata, handles)
% hObject    handle to updatePlot1Button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

UpdatePlot(handles.axes2, handles.popupmenu2, handles);


function UpdatePlot(axesHandle, popupHandle, handles)

axes(axesHandle);
cla;

popup_sel_index = get(popupHandle, 'Value');

frameDataIndex = popup_sel_index+2;

plot([handles.frameData{2}], [handles.frameData{frameDataIndex}]);

hold on;

plot(handles.frameData{2}(handles.currentFrameIndex), ...
    handles.frameData{frameDataIndex}(handles.currentFrameIndex),'o',...
    'MarkerEdgeColor','r',...
    'MarkerSize',10);
  
function handles = UpdateViews(handles)

    UpdateFrame(handles);
    UpdatePlot(handles.axes1, handles.popupmenu1, handles);
    UpdatePlot(handles.axes2, handles.popupmenu2, handles);
    handles = UpdateMap(handles);  

    
% ====================================================================
% == SEQUENCE NAVIGATION CONTROLS
% ====================================================================
    
function currentFrameEditBox_Callback(hObject, eventdata, handles)
% hObject    handle to currentFrameEditBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of currentFrameEditBox as text
%        str2double(get(hObject,'String')) returns contents of currentFrameEditBox as a double
  frameIndex = str2double(get(hObject,'String'));
   
   if ~isnan(frameIndex) 
     if (frameIndex>=1) && (frameIndex<=size(handles.frameData{1},1))
        handles.currentFrameIndex =  frameIndex;
	guidata(hObject, handles);
	UpdateFrame(handles);
     else
       set(hObject,'String',sprintf('%d', handles.currentFrameIndex));
     end
   else
     set(hObject,'String',sprintf('%d', handles.currentFrameIndex));
   end

% --- Executes on button press in prevFrameButton.
function prevFrameButton_Callback(hObject, eventdata, handles)
% hObject    handle to prevFrameButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

 stepText = get(handles.frameStepEditBox,'String'); 
 step = str2double(stepText);

 if (handles.currentFrameIndex-step) >= 1
   handles.currentFrameIndex =  handles.currentFrameIndex-step;
   
    handles = UpdateViews(handles);
   guidata(hObject, handles);
 end

  
% --- Executes on button press in nextFrameButton.
function nextFrameButton_Callback(hObject, eventdata, handles)
% hObject    handle to nextFrameButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

  stepText = get(handles.frameStepEditBox,'String'); 
  step = str2double(stepText);
   
  if (handles.currentFrameIndex+step) <= size(handles.frameData{1},1)
    handles.currentFrameIndex =  handles.currentFrameIndex+step;
     handles = UpdateViews(handles);
    guidata(hObject, handles);
  end
  
  
% -- Refreshes the graphical elements of the single frame view
function handles = UpdateFrame(handles)  
  axes(handles.frameViewAxis);
  axisLimits = axis;
  cla reset;
  
  % Plot current frame image
    
  imageNamePath = [get(handles.sequenceNameEditBox,'String') ...
                    handles.frameData{1}{handles.currentFrameIndex}];
        
  frame = imread(imageNamePath);
  
  if(get(handles.oddLinesCheckbox,'Value')==1)
      frame = frame(1:2:end,:,:);
  end
    
  imshow(frame,'XData',[0 size(frame,2)-1],...
	              'YData',[0 size(frame,1)-1]);
  
  set(handles.currentFrameEditBox,'String',...
		    sprintf('%d', handles.currentFrameIndex));
  
  % 10 bit images. To display, the less significant 2 bits are
  % ignored, determining the grayscale value for the top most 8 bits.

  if CheckNoisyImage(frame)
      set(handles.noiseAlertStaticText,'String','NOISY');
  else
      set(handles.noiseAlertStaticText,'String','');
  end

% ====================================================================
% == TEST TO DETECT NOISY FRAMES
% ====================================================================   
  
function noisy = CheckNoisyImage(frame)

  gap = 6;
  thr = 100;
  
  leftRegion = max(mean(mean(frame(:,1:gap,:),3),2));
  rightRegion = max(mean(mean(frame(:,(end-gap+1):end,:),3),2));
  
  noisy = (leftRegion>thr) | (rightRegion>thr);
  
% ====================================================================
% == UPDATE MAP FUNCTIONS
% ====================================================================   

% Convert geographic to pixel coords (matlab indexed. ie, starting at 1)
% geoCoords is a 2xN vector (longitude 1st row, latitude 2nd row)
function pixCoords = Geo2Pix(geoCoords, handles)
    geoMin = repmat([handles.ortoInfo.LongitudeLimits(1); handles.ortoInfo.LatitudeLimits(1)],1,size(geoCoords,2));
    geoRange = repmat([handles.ortoInfo.LongitudeLimits(2)-handles.ortoInfo.LongitudeLimits(1);...
                          handles.ortoInfo.LatitudeLimits(2)-handles.ortoInfo.LatitudeLimits(1)],1,size(geoCoords,2));
    
                     
    rasterSize = handles.ortoInfo.RasterSize';            
                 
    rasterSize = flipud(rasterSize);
    pixRange = repmat(rasterSize - [1;1],1,size(geoCoords,2));
    pixRange(2,:) = -pixRange(2,:);
    % Inverse Y image axis considered
    pixCoords = ((geoCoords - geoMin)./geoRange .* pixRange) + ones(2,size(geoCoords,2))+repmat([0;rasterSize(2)-1],1,size(geoCoords,2));
    
% Convert pixel to geographic coords (matlab indexed. ie, starting at 1)
% pixCoords is a 2xN vector (longitude 1st row, latitude 2nd row)
    
function geoCoords = Pix2Geo(pixCoords, handles)
    
    % Since the Y image axis has opposite direction than the latitud axis,
    % formulas change a little bit from the rule of 3.
    geoMin = repmat([handles.ortoInfo.LongitudeLimits(1); handles.ortoInfo.LatitudeLimits(1)],1,size(pixCoords,2));
    pixMin = repmat([1;1],1,size(pixCoords,2));
    
    rasterSize = handles.ortoInfo.RasterSize';
                 
    rasterSize = flipud(rasterSize);
    pixRange = repmat(rasterSize- [1;1],1,size(pixCoords,2));
    pixRange(2,:) = -pixRange(2,:);
   
    geoRange = repmat([handles.ortoInfo.LongitudeLimits(2)-handles.ortoInfo.LongitudeLimits(1);...
                         ( handles.ortoInfo.LatitudeLimits(2)-handles.ortoInfo.LatitudeLimits(1))],1,size(pixCoords,2));
    
    geoCoords = (pixCoords - pixMin -repmat([0;rasterSize(2)-1],1,size(pixCoords,2)))./pixRange .* geoRange + geoMin;        
            
function handles = UpdateMap(handles)
  
  % Compute the extent of the map area to be cropped, according to the UAV
  % log information;
  lat = handles.frameData{3}(handles.currentFrameIndex);
  lon = handles.frameData{4}(handles.currentFrameIndex);
  relAlt = handles.frameData{8}(handles.currentFrameIndex);
  cosYaw = handles.frameData{13}(handles.currentFrameIndex);
  sinYaw = handles.frameData{14}(handles.currentFrameIndex);
  cosPitch = handles.frameData{15}(handles.currentFrameIndex);
  sinPitch = handles.frameData{16}(handles.currentFrameIndex);
  
  % 1) [pixMap/geoMap]-[Rectangle/RotatedRectangle] ------------
  % map Crop (maintaining N-S orientation)
  
  pixCenter = Geo2Pix([lon;lat],handles);
  
  % Offset due to pitch
  % Probably a better formalization of the angles is needed.
  % Now I estimate the y ofsset due to camera pitch, which is then
  % rotated considering the yaw angle. But maybe this is not the correct
  % order...
  
  pixCenterOffset = [0; -(relAlt*sinPitch/cosPitch)/handles.cellSize];
   
  % Rectangle in pixel coordinates, as centered at the origin
  % The dimensions are fixed, keeping the orthomap resolution, considering
  % that the UAV is flying at its maximum height.
  %Calcules els 4 extrems del rectangle
  pixRectangleOffset = [-handles.FixDimW/2 -handles.FixDimW/2 handles.FixDimW/2 handles.FixDimW/2;...
                        handles.FixDimH/2 -handles.FixDimH/2 -handles.FixDimH/2 handles.FixDimH/2];
  
  matRot = [cosYaw -sinYaw; sinYaw cosYaw];
  %Rotes el rectangle (els 4 punts)
  pixRotatedRectangleOffset = matRot * pixRectangleOffset;
  %Rotes l'offset degut al pitch
  pixRotatedCenterOffset = matRot * pixCenterOffset;
  
  %Situes el rectangle sense rotar (abans centrat al 0,0) al localitzaciˇ pertinent del mapa
  pixMapRectangle = pixRectangleOffset + repmat(pixCenter,1,4);
  %Situes el rectangle rotat (abans centrat al 0,0) a la localitzaciˇ del
  %mapa tenint en compte el desplašament del pitch
  pixMapRotatedRectangle = pixRotatedRectangleOffset + repmat(pixCenter+pixRotatedCenterOffset,1,4);
  
  geoMapRectangle = Pix2Geo(pixMapRectangle,handles);
  geoMapRotatedRectangle = Pix2Geo(pixMapRotatedRectangle,handles); 
  
  % 2) [pixMap/geoMap]-[RealRectangle/RealRotatedRectangle] ------------
  % Pixel coordinates of the observed orthomap area, considering the
  % current altitude of the UAV (RelAlt)
  
  [pixMapRealRectangle, pixMapRealRotatedRectangle, ...
          geoMapRealRectangle, geoMapRealRotatedRectangle] = ComputeExpectedFrameParams(handles, handles.currentFrameIndex);

  % 3) simImage : Flight simulation image - 
  % provides an image of fixed size, keeping the orthomap resolution

  [cols,rows] = meshgrid((-handles.FixDimW/2):(handles.FixDimW/2-1),(-handles.FixDimH/2):(handles.FixDimH/2-1));
    
  xy = [reshape(cols, prod(size(cols)),1)'; reshape(rows, prod(size(rows)),1)'];
  
  xyRot = matRot * xy + repmat(pixCenter+pixRotatedCenterOffset,1,size(xy,2));
    
  rowsRot = reshape(xyRot(2,:), handles.FixDimH, handles.FixDimW);
  colsRot = reshape(xyRot(1,:), handles.FixDimH, handles.FixDimW);
  
  samplingImage = im2double(handles.ortoMap);

  simImage(:,:,1) = (interp2(samplingImage(:,:,1),colsRot, rowsRot,'linear'));
  simImage(:,:,2) = (interp2(samplingImage(:,:,2),colsRot, rowsRot,'linear'));
  simImage(:,:,3) = (interp2(samplingImage(:,:,3),colsRot, rowsRot,'linear'));
  
  % Display ----------------------------------------
  
  popupSelIndex = get(handles.mapPopupmenu, 'Value');
  axes(handles.map);
  
  switch popupSelIndex
      case 1  % Map 

          % Depending on the coordinate system (wether it is a geographic
          % latitude-longitude system, or a planar (projected) map coordinate
          % system, a diferetn function has to be used to display the ortofoto.

          if handles.prevMapView ~= 1
            cla
            switch handles.ortoInfo.CoordinateSystemType
                  case 'geographic'  
                     geoshow(handles.ortoMap, handles.ortoInfo);

                case 'planar'
                    mapshow(handles.ortoMap, handles.ortoInfo);
            end
          end

          hold on;
          axis square;

          plot([handles.frameData{4}], [handles.frameData{3}]);
          
          plot(geoMapRectangle(1,:), geoMapRectangle(2,:),'g-');
          plot(geoMapRotatedRectangle(1,:), geoMapRotatedRectangle(2,:),'r-');
          plot(geoMapRealRotatedRectangle(1,:), geoMapRealRotatedRectangle(2,:),'b-');
          
      case 2 % Map Crop
          
        imshow(handles.ortoMap)
        axis on
        hold on
          
        plot(pixCenter(1),pixCenter(2),'r*');        
        plot(pixMapRectangle(1,:), pixMapRectangle(2,:),'g-');
        plot(pixMapRotatedRectangle(1,:), pixMapRotatedRectangle(2,:),'r-');
        plot(pixMapRealRotatedRectangle(1,:), pixMapRealRotatedRectangle(2,:),'b-');
       
      case 3 % Flight simulation
          cla;
          axis ij
          imshow(simImage);
  end
  
  handles.prevMapView = popupSelIndex;
  
   
% ====================================================================
% == GENERATION OF EXPECTED FRAMES MAP LIMITS FROM UAV DATALOG
% ====================================================================   
   
% --- Executes on button press in saveExpectedFramesButton.
function saveExpectedFramesButton_Callback(hObject, eventdata, handles)
% hObject    handle to saveExpectedFramesButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

oldPointer = get(handles.figure1,'pointer');
set(handles.figure1,'pointer','watch');
drawnow;
SaveExpectedFramesParams(handles)
set(handles.figure1,'pointer',oldPointer);

function SaveExpectedFramesParams(handles)

 fileName = handles.tsvFile;
 fileName = strrep(fileName,'SynchronizedData','ExpectedFrames');
  
 fid = fopen(fileName,'w');
  
 fileHeader = 'FrameName x1 x2 x3 x4 y1 y2 y3 y4';
 
 fprintf(fid,'%s\n',fileHeader);
 
 for index=1:size(handles.frameData{1},1)
        
        [pixMapRealRectangle, pixMapRealRotatedRectangle, ...
          geoMapRealRectangle, geoMapRealRotatedRectangle] = ComputeExpectedFrameParams(handles, index);
      
      fprintf(fid,'%s ',cell2mat(handles.frameData{1}(index)));
      fprintf(fid,'%1.3f ',pixMapRealRotatedRectangle(1,:));
      fprintf(fid,'%1.3f ',pixMapRealRotatedRectangle(2,:));
      fprintf(fid,'\n');
  
  end
    
  fclose(fid);
   
  function [pixMapRealRectangle, pixMapRealRotatedRectangle, ...
          geoMapRealRectangle, geoMapRealRotatedRectangle] = ComputeExpectedFrameParams(handles, index)  
      
      
    % Compute the extent of the map area to be cropped, according to the UAV
  % log information;
  lat = handles.frameData{3}(index);
  lon = handles.frameData{4}(index);
  relAlt = handles.frameData{8}(index);
  cosYaw = handles.frameData{13}(index);
  sinYaw = handles.frameData{14}(index);
  cosPitch = handles.frameData{15}(index);
  sinPitch = handles.frameData{16}(index);
  
  % map Crop (maintaining N-S orientation)
  
  pixCenter = Geo2Pix([lon;lat],handles);
  
  % Offset due to pitch
  % Probably a better formalization of the angles is needed.
  % Now I estimate the y ofsset due to camera pitch, which is then
  % rotated considering the yaw angle. But maybe this is not the correct
  % order...
  
  pixCenterOffset = [0; -(relAlt*sinPitch/cosPitch)/handles.cellSize];
    
  geoObservedWidth = 2*tan(handles.angleOfViewX/2)*relAlt; % In Meters (it does not need to be in degrees since cellSize consideres meters)
  pixObservedWidth = geoObservedWidth/handles.cellSize;
  pixObservedHeight = pixObservedWidth / handles.frameAspectRatio;
  
  pixRectangleOffset = [-pixObservedWidth/2 -pixObservedWidth/2 pixObservedWidth/2 pixObservedWidth/2; ...
                         pixObservedHeight/2 -pixObservedHeight/2 -pixObservedHeight/2 pixObservedHeight/2 ];
 
  matRot = [cosYaw -sinYaw; sinYaw cosYaw];
 
                     
  pixRotatedRectangleOffset = matRot * pixRectangleOffset;
  
  pixRotatedCenterOffset = matRot * pixCenterOffset;
  
  pixMapRealRectangle = pixRectangleOffset + repmat(pixCenter,1,4);
  pixMapRealRotatedRectangle = pixRotatedRectangleOffset + repmat(pixCenter+pixRotatedCenterOffset,1,4);
  
  geoMapRealRectangle = Pix2Geo(pixMapRealRectangle,handles);
  geoMapRealRotatedRectangle = Pix2Geo(pixMapRealRotatedRectangle,handles); 
    
% ====================================================================
% == AUTOMATICALLY GENERATED CALLBACKS INITIALIZATION FUNCTIONS
% ====================================================================

% --- Outputs from this function are returned to the command line.
function varargout = DataLogViewer_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;
    
% --- Executes on button press in mapSelectorPushbutton.
function mapSelectorPushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to mapSelectorPushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on selection change in mapPopupmenu.
function mapPopupmenu_Callback(hObject, eventdata, handles)
% hObject    handle to mapPopupmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns mapPopupmenu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from mapPopupmenu


% --- Executes during object creation, after setting all properties.
function mapPopupmenu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mapPopupmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in oddLinesCheckbox.
function oddLinesCheckbox_Callback(hObject, eventdata, handles)
% hObject    handle to oddLinesCheckbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of oddLinesCheckbox

% --------------------------------------------------------------------
function FileMenu_Callback(hObject, eventdata, handles)
% hObject    handle to FileMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function OpenMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to OpenMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
file = uigetfile('*.fig');
if ~isequal(file, 0)
    open(file);
end

% --------------------------------------------------------------------
function PrintMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to PrintMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
printdlg(handles.figure1)

% --------------------------------------------------------------------
function CloseMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to CloseMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
selection = questdlg(['Close ' get(handles.figure1,'Name') '?'],...
                     ['Close ' get(handles.figure1,'Name') '...'],...
                     'Yes','No','Yes');
if strcmp(selection,'No')
    return;
end

delete(handles.figure1)


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
     set(hObject,'BackgroundColor','white');
end

set(hObject, 'String', {'plot(rand(5))', 'plot(sin(1:0.01:25))', 'bar(1:.5:10)', 'plot(membrane)', 'surf(peaks)'});

% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2

% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function sequenceNameEditBox_Callback(hObject, eventdata, handles)
% hObject    handle to sequenceNameEditBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of sequenceNameEditBox as text
%        str2double(get(hObject,'String')) returns contents of sequenceNameEditBox as a double


% --- Executes during object creation, after setting all properties.
function sequenceNameEditBox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sequenceNameEditBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function frameStepEditBox_Callback(hObject, eventdata, handles)
% hObject    handle to frameStepEditBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of frameStepEditBox as text
%        str2double(get(hObject,'String')) returns contents of frameStepEditBox as a double


% --- Executes during object creation, after setting all properties.
function frameStepEditBox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to frameStepEditBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function currentFrameEditBox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to currentFrameEditBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
