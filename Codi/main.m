function main()
    addpath('Sincronitzaci�');
    %%SINCRONITZACI�
    %Directori on es troben tots els frames de la seq��ncia de v�deo.
    framesPath = 'D:\User\Documentos\Guim\Treballs\4t Carrera\Treball Fi de Grau\SequenciesPropies\201405161455\';  %201405161414    201405161455
    %Ruta del fitxer amb les dades del sensor (no sincronitzades)
    logSensorPath = 'UAVLog\Raw\2014-05-16 14-56-51.log'; %50m
    %Tipus de detector utilitzat en el feature matching
    tipusDeteccio = 'FAST';
    %Despla�ament entre frames al fer l'estimaci�
    despFrames = 1;
    rutaFitxerSincronitzat = mainSincronitzacio(framesPath, logSensorPath, tipusDeteccio, despFrames);
    %rutaFitxerSincrontizat = 'Fitxers generats\2014-05-16 14-56-51 SynchronizedData.tsv';
    
    %%GENERACI� ORTO MAPA
    addpath('Ortomapa');
    focal = [1297.6342180147724 1227.3899167162];
    tipusOrtomapa ='ImageBased'; %'SensorBased'; 'LucasKanadeBasedFAST'; 'LucasKanadeBasedImage+Sensor'
    
    switch tipusOrtomapa
        case 'ImageBased'
            addpath('Ortomapa\ImageBased');
            OrtomapaFeatureMatching(framesPath, tipusDeteccio);
            
        case 'SensorBased'
            addpath('Ortomapa\SensorBased');
            OrtomapaSensor(framesPath, rutaFitxerSincrontizat, focal);
            
        case 'LucasKanadeBasedFAST'
            addpath('Ortomapa\ImageAndSensorBased');
            addpath('Ortomapa\ImageAndSensorBased\LucasKanade');
            OrtomapaLK_FAST(framesPath, tipusDeteccio, rutaFitxerSincrontizat);
           
        case 'LucasKanadeBasedImage+Sensor'
            addpath('Ortomapa\ImageAndSensorBased');
            addpath('Ortomapa\ImageAndSensorBased\LucasKanade');
            OrtomapaLK_ImageAndSensor(framesPath, tipusDeteccio, rutaFitxerSincrontizat, focal);
    end
    
end