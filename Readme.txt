TAULA DE CONTINGUTS
	
	1. Codi

		Nota: s'ha utilitzat MATLAB R2014a pel desenvolupament d'aquest treball. Algunes funcions podrien no estar disponibles en versions anteriors.

		main.m és el codi principal que crida a cada una de les funcions per realitzar el procés de sincronitzar les dades dels sensors amb les imatges de l'UAV per finalment generar l'ortomapa. En aquest codi principal no s'inclou la validació dels descriptors/detectors de característiques ni dels ortomapes, ja que és un procés addicional de cara a analitzar els resultats. Els codis per realitzar aquesta avaluació es troben en els seus respectius apartats (1.1.4 per detectors/descriptors, 1.2.4 per ortomapes)

		1.1. Sincronització: tot el codi pertanyent a sincronitzar les dades dels sensors amb les imatges de la missió de vol de l'UAV.
			mainSincronitzacio.m és el programa principal que realitza tota la sincronització, és a dir, donat un conjunt d'imatges i un fitxer amb dades de sensors, et crea un fitxer amb les dades dels sensors per cada una de les imatges de la seqüència.

			1.1.1 Calaix de proves: codi enfocat a experiments puntuals.
			1.1.2 Fitxers generats: arxius temporals emmagatzemats per no repetir experiments costosos en temps.
			1.1.3 Imatges: gràfics generats.
			1.1.4 Validació i testos: codi centrat en avaluar cada un dels descriptors mitjançant anotació de ground truth.
			

		1.2. Ortomapa
			En aquest directori es troba, per una part, els tres tipus d'aproximacions seguides per generar un ortomapa (basat en imatges, basat en sensors, i basat en ambdós), i per l'altra la validació dels ortomapes. Cada una de les tres aproximacions té el seu propi codi per generar l'ortomapa.
			1.2.1 ImageBased: generació de l'ortomapa basat només en imatges, utilitzant el descriptor FAST + RANSAC per generar l'estimació.
			1.2.2 SensorBased: generació de l'ortomaapa basat només en la informació del sensor, no requereix analitzar les imatges.
			1.2.3 ImageAndSensorBased: generació de l'ortomapa basat en l'aplicació de l'algorisme Lucas-Kanade.
			1.2.4 Validació: avaluació d'ortomapes mitjançant un ground truth i el mapa real de la zona on s'han enregistrat les imatges.


		1.3. BundleAdjustment (*INACABAT!*)
			Aproximació per aplicar un bundle adjustment en un ortomapa. El codi està inacabat, però s'inclouen les llibreries necessaries per executar-lo. Aquestes llibreries es troben a la carpeta vincentToolbox_3.1.1

	2. Informes
		Aquí s'inclou tota la documentació generada durant tot el treball: informe inicial, informe de progrés I, informe de progrés II i l'article final. Adicionalment s'inclou documentació centrada en la rotació de la NCC i el SURF, i altres documents de planificació com diagrames de GANTT o arbres d'objectius.

	3. Documentació
		Apartat on es troben els articles llegits i altra documentació utilitzada per a la realització del treball.
